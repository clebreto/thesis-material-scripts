import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def yoyf(signal, events_reader_output, event="") :
    events = []
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if events_reader_output[e_i].uid == event :
            events.append(signal[:, int(events_reader_output[e_i].timestamp / 1000. * sampling_rate):int(events_reader_output[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return events

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events = events_reader.output()

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

## Wavelet Transform
fmin = 9.
fmax = 11
fstep = 0.5

wavelet_width = 12.

subject_id = "0001"

files = np.array(list(recordings.glob("./*/*" + subject_id + "_alpha.edf")))
weights = np.zeros(shape=(len(files) * 5, 31))
for f_i, f in enumerate(tqdm(files)) :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal = reader_edf_file.output()[:32, :]
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32])

    # all but reference :
    channels_not_reference = [c for c in channels if c not in ["F3"]]
    indices_not_reference = [np.where(channels == c)[0][0] for c in channels_not_reference]
    signal = signal[indices_not_reference] - signal[[np.where(channels == c)[0][0] for c in ["F3"]]]

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(3.)
    bp.setFrequency(10.)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()
    signal_filtered = bp.output()[:, 3*512: -3*512]

    alpha_signals = yoyf(signal_filtered, events, "alpha")

    for s_i, s in enumerate(alpha_signals):
        # Solution 1
        M = s
        n = M.shape[0]
        H = M.dot(M.T)

        K = H[0:-1,0:-1]
        h = H[-1,0:-1]
        a = H[-1,-1]
        o = np.ones((n-1))

        Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
        B = a*o-h

        try:
            x = np.linalg.solve(Q,B)
            w = np.append(x,1-np.dot(x,o))
        except np.linalg.LinAlgError as error:
            w = np.zeros(shape=(len(channels) - 1))
        weights[f_i * s_i + s_i] = w

plt.clf()
fg, ax = plt.subplots(2, 1)
positions = np.arange(0, len(files) * 5, 1)
weights_f = [wf for wf in weights]
weights_c = [wc for wc in weights.T]
ax[0].boxplot(weights_f, showfliers=False)
ax[1].boxplot(weights_c, showfliers=False, labels=channels[:-1])

name = "minimization_all_files_" + subject_id + "alpha_reference_F3"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 10), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
