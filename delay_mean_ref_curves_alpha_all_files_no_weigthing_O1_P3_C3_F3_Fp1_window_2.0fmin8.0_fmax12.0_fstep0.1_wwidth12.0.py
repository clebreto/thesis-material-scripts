import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")


## Channel selection
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])

## Wavelet Transform
fmin = 8.
fmax = 12.
fstep = 0.1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

nb_files = 0
files = sorted(recordings.glob("./2020_11_*/*01_alpha.edf"))
min_nb_values = float('inf')
for f in files :
    nb_files += 1
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32] # 33 is GSR
    signal = signal - np.mean(signal, axis=0)
    nb_values = signal.shape[1]
    if nb_values < min_nb_values :
        min_nb_values = nb_values

## Delay window analysis
min_window_length = 2. # seconds
min_sampling_rate = 512.
min_window_size = int(min_window_length * min_sampling_rate)
min_nb_windows = int(min_nb_values / min_window_size)
delay = np.zeros(shape=(channels_selection.size - 1, min_nb_windows))

files = sorted(recordings.glob("./2020_11_*/*01_alpha.edf"))
for f in files :
    print(str(f))
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32,: ] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Reads the event as they were presented during the recording
    events_reader = ztrcore.ztrProcessEventsReader()
    events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/calculus.evt")
    events_reader.run()
    events_reader_output = events_reader.output()

    ## Select only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    signal = signal[indices_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    ## To compute on pairs with O1
    Y_mask = np.eye(nb_channels, nb_channels, 1)
    pairs_indices = np.nonzero(Y_mask)
    nb_pairs = len(Y_mask[pairs_indices])

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output())

    ## Exttract the amplitude and the phase
    amplitude = np.abs(wt)
    phase = np.angle(wt)

    window_length = 2. # seconds
    window_size = int(window_length * sampling_rate)
    nb_windows = int(nb_values / window_size)
    ## Then analyze window by window (it's not possible to allocate enough memory to compute all the diff)
    diff = np.empty(shape=(nb_pairs, nb_windows))
    for w_i in range(nb_windows) :
        w_start = int(w_i * window_size)
        w_end = int((w_i + 1) * window_size)
        w_phase = phase[:, :,  w_start : w_end]
        w_amplitude = amplitude[:, :, w_start : w_end]
        w_phase_diff = np.empty(shape=(nb_pairs, wt.shape[1], window_size))
        w_amplitude_prod = np.empty(shape=(nb_pairs, wt.shape[1], window_size))
        for p_i in range(nb_pairs) :
            w_phase_diff[p_i] = w_phase[pairs_indices[1][p_i]] - w_phase[pairs_indices[0][p_i]]
            w_amplitude_prod[p_i] = w_amplitude[pairs_indices[1][p_i]] * w_amplitude[pairs_indices[0][p_i]]
        ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
        w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
        w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)

        for p_i in range(nb_pairs) :
            diff[p_i][w_i] = np.mean(w_phase_diff[p_i], axis=(0, 1))

    delay += diff[:, :min_nb_windows]

delay /= nb_files

plt.clf()
for p_i in range(nb_pairs) :
    plt.plot(delay[p_i], label=channels[pairs_indices[1][p_i]] + "-" + channels[pairs_indices[0][p_i]])
plt.ylim([-np.pi, np.pi])
plt.legend()

name = "delay_mean_ref_curves_alpha_all_files_no_weigthing_O1_P3_C3_F3_Fp1" + "_window_" + str(window_length) + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
