import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.tri as tri

import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

# Compute the a Delaunay triangulation to get close neighbors
headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")
positions = headset.positions()
labels    = np.array(headset.labels())
electrodes_positions = dict(zip(labels, positions))

x = [positions[x, 0] for x in range(len(positions))]
y = [positions[y, 1] for y in range(len(positions))]

triangulation = tri.Triangulation(x, y)
short_range_neighbors = np.zeros(shape=(len(labels), len(labels)))
for e in triangulation.edges :
    short_range_neighbors[e[1], e[0]] = 1

## Wavelet Transform
fmin = 9.
fmax = 11.
fstep = 0.1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

nb_files = 0
files = recordings.glob("./2020_11_*/*01_alpha.edf")
min_nb_values = float('inf')
for f in files :
    nb_files += 1
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32] # 33 is GSR
    nb_values = signal.shape[1]
    if nb_values < min_nb_values :
        min_nb_values = nb_values

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events = events_reader.output()

## Delay window analysis
window_length = 2. # seconds
sampling_rate = 512.
window_size = int(window_length * sampling_rate)
nb_windows = int(min_nb_values / window_size)
nb_pairs = 83
phase_diff_alpha  = np.zeros(shape=(nb_pairs))
phase_diff_baseline  = np.zeros(shape=(nb_pairs))
nb_alpha_events = 0

files = recordings.glob("./2020_11_*/*01_alpha.edf")
skip = False
for f in files :
    print(str(f))
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32, :min_nb_values] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output())

    ## Exttract the amplitude and the phase
    amplitude = np.abs(wt)
    phase = np.angle(wt)

    pairs_indices = np.nonzero(short_range_neighbors)
    nb_pairs = len(short_range_neighbors[pairs_indices])

    for e_i in range(len(events)) :
        if events[e_i].uid == 'alpha' :
            if skip :
                skip = False
                continue
            phase_event = np.copy(phase[:, :, int(events[e_i].timestamp / 1000. * sampling_rate) : int(events[e_i + 1].timestamp / 1000. * sampling_rate)])
            phase_diff_event = np.empty(shape=(nb_pairs, phase_event.shape[1], phase_event.shape[2]))
            for p_i in range(nb_pairs) :
                phase_diff_event[p_i] = phase_event[pairs_indices[1][p_i]] - phase_event[pairs_indices[0][p_i]]

            ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
            phase_diff_event = np.where(phase_diff_event > np.pi, phase_diff_event - 2 * np.pi, phase_diff_event)
            phase_diff_event = np.where(phase_diff_event < -np.pi, phase_diff_event + 2 * np.pi, phase_diff_event)

            nb_alpha_events += 1

            phase_diff_alpha += np.mean(phase_diff_event, axis=(1, 2))

            skip = True

delay_alpha = phase_diff_alpha / nb_alpha_events
delay_alpha_abs = np.abs(delay_alpha)
pairs_x = []
pairs_y = []
pairs_labels = []
for p_i in range(nb_pairs):
    pairs_labels.append(labels[pairs_indices[1][p_i]] + "-" + labels[pairs_indices[0][p_i]])
    pairs_x.append((positions[pairs_indices[1][p_i]][0] + positions[pairs_indices[0][p_i]][0]) / 2.)
    pairs_y.append((positions[pairs_indices[1][p_i]][1] + positions[pairs_indices[0][p_i]][1]) / 2.)

x = [positions[x, 0] for x in range(len(positions_selection))]
y = [positions[y, 1] for y in range(len(positions_selection))]

pairs_triangulation = tri.Triangulation(pairs_x, pairs_y)
plt.clf()
plt.triplot(triangulation)
plt.triplot(pairs_triangulation)
plt.tricontourf(pairs_triangulation, delay_alpha_abs)
for ep in electrodes_positions.items() :
    plt.annotate(ep[0], xy=(ep[1][0], ep[1][1]))

cax = plt.axes([0.85, 0.1, 0.035, 0.8])
plt.colorbar(cax=cax)

name = "delay_alpha_alpha_all_files_no_weigthing_all_electrodes" + "_window_" + str(window_length) + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
