import numpy as np
import matplotlib.pyplot as plt
import sys, os, shutil
import ztrcore
import ztrplotting

duration = 20.
sampling_rate = 512
nb_samples = int(duration * sampling_rate)

t_signal = np.linspace(0., duration, nb_samples)
f_signal = 10.
t_0 = 7
omega_s = 2
signal = np.cos(2. * np.pi * f_signal * t_signal + np.pi / 2.) * np.exp(-(2 * (t_signal - t_0)**2 * np.pi**2 * f_signal**2) / (omega_s**2))


fmin = 7.
fmax = 17.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 10.

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(np.expand_dims(signal, axis=(0)))
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(np.abs(wavelet_fourier.output()))


check_0 = np.zeros(shape=wt.shape)
check_1 = np.zeros(shape=wt.shape)
for f_i, f in enumerate(frequencies) :
    nu_s = f_signal
    nu_w = f_i
    omega_s = omega_s
    omega_w = wavelet_width
    phi_s = np.pi/2.
    # check[0, f_i, :] = np.arctan(np.tan(2. * np.pi * t_signal * ((f * f_signal * f_signal * wavelet_width * wavelet_width) + (f_signal * f * f * omega_s * omega_s)) / (f_signal * f_signal * wavelet_width * wavelet_width + f * f * omega_s * omega_s) + 2. * np.pi * t_0 * (f_signal - f)))

    check_0[0, f_i, :] = np.arctan((np.exp((-omega_w**2 * (nu_s+nu_w)**2 * omega_s**2 - 4 * np.pi**2 * nu_s**2 * nu_w**2 * (t_signal- t_0)**2) / (2 * omega_w**2 * nu_s**2 + 2 * nu_w**2 * omega_s**2)) * np.sin((-2 * np.pi * nu_s**3 * t_0 * omega_w**2 + 2 * omega_w**2 * (np.pi * (t_signal- t_0) * nu_w - phi_s / 2) * nu_s**2 - 2 * np.pi * t_signal * nu_s * nu_w**2 * omega_s**2 - nu_w**2 * phi_s * omega_s**2) / (omega_w**2 * nu_s**2 + nu_w**2 * omega_s**2)) + np.exp((-omega_w**2 * (nu_s - nu_w)**2 * omega_s**2 - 4 * np.pi**2 * nu_s**2 * nu_w**2 * (t_signal- t_0)**2) / (2 * omega_w**2 * nu_s**2 + 2 * nu_w**2 * omega_s**2)) * np.sin((2 * np.pi * nu_s**3 * t_0 * omega_w**2 + 2 * (np.pi * (t_signal- t_0) * nu_w + phi_s / 2) * omega_w**2 * nu_s**2 + 2 * np.pi * t_signal* nu_s* nu_w**2 * omega_s**2 + nu_w**2 * phi_s * omega_s**2) / (omega_w**2 * nu_s**2 + nu_w**2 * omega_s**2))) / (np.exp((-omega_w**2 * (nu_s+ nu_w)**2 * omega_s**2 - 4 * np.pi**2 * nu_s**2 * nu_w**2 * (t_signal- t_0)**2) / (2 * omega_w**2 * nu_s**2 + 2 * nu_w**2 * omega_s**2)) * np.cos((-2 * np.pi * nu_s**3 * t_0 * omega_w**2 + 2 * omega_w**2 * (np.pi * (t_signal- t_0) * nu_w - phi_s / 2) * nu_s**2 - 2 * np.pi * t_signal* nu_s* nu_w**2 * omega_s**2 - nu_w**2 * phi_s * omega_s**2) / (omega_w**2 * nu_s**2 + nu_w**2 * omega_s**2)) + np.exp((-omega_w**2 * (nu_s- nu_w)**2 * omega_s**2 - 4 * np.pi**2 * nu_s**2 * nu_w**2 * (t_signal- t_0)**2) / (2 * omega_w**2 * nu_s**2 + 2 * nu_w**2 * omega_s**2)) * np.cos((2 * np.pi * nu_s**3 * t_0 * omega_w**2 + 2 * (np.pi * (t_signal- t_0) * nu_w + phi_s / 2) * omega_w**2 * nu_s**2 + 2 * np.pi * t_signal* nu_s* nu_w**2 * omega_s**2 + nu_w**2 * phi_s * omega_s**2) / (omega_w**2 * nu_s**2 + nu_w**2 * omega_s**2))))

    check_1[0, f_i, :] = np.arctan((np.sin((2 * np.pi * nu_s**3 * t_0 * omega_w**2 + 2 * (np.pi * (t_signal- t_0) * nu_w + phi_s / 2) * omega_w**2 * nu_s**2 + 2 * np.pi * t_signal* nu_s* nu_w**2 * omega_s**2 + nu_w**2 * phi_s * omega_s**2) / (omega_w**2 * nu_s**2 + nu_w**2 * omega_s**2))) / (np.cos((2 * np.pi * nu_s**3 * t_0 * omega_w**2 + 2 * (np.pi * (t_signal- t_0) * nu_w + phi_s / 2) * omega_w**2 * nu_s**2 + 2 * np.pi * t_signal* nu_s* nu_w**2 * omega_s**2 + nu_w**2 * phi_s * omega_s**2) / (omega_w**2 * nu_s**2 + nu_w**2 * omega_s**2))))




print(check[0][30][7*512])
print(wt[0][30][7*512])

plt.clf()
fg, ax = plt.subplots(3, 1)
ztrplotting.plotTFPhases(fg, ax[0], np.linspace(0, duration, nb_samples), frequencies, wt[0], xlabel=r'$Time, \delta_k (s.)$', ylabel=r'$\nu$')
ztrplotting.plotTFPhases(fg, ax[1], np.linspace(0, duration, nb_samples), frequencies, check_0[0], xlabel=r'$Time, \delta_k (s.)$', ylabel=r'$\nu$')
ztrplotting.plotTFPhases(fg, ax[2], np.linspace(0, duration, nb_samples), frequencies, check_1[0], xlabel=r'$Time, \delta_k (s.)$', ylabel=r'$\nu$')

name = "analytic_wavelet_complete_sinus_phase"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 4.), forward=False)
fg.savefig(name + ".png", dpi=300, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
