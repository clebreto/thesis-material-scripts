import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 20,
    'axes.labelsize': 20,
    'axes.titlesize': 20,
    'xtick.labelsize': 20,
    'ytick.labelsize': 20
})

colors = np.array(["black", "red", "blue", "cyan"])
labels = np.array(['O1-P3', 'O1-C3', 'O1-F3', 'O1-Fp1'])
x = np.array([100+6.125, 200+12.5, 300+25, 400+30])
y = np.array([5.5, 11.5, 17., 25.])

m, b = np.polyfit(x, y, 1)

plt.clf()
plt.plot(x, m*x+b, '--k') #'--k'=black dashed line, 'yo' = yellow circle marker
for (x, y, l, c) in zip(x, y, labels, colors):
    plt.scatter(x, y, color=c, label=l)
plt.legend()
plt.xlabel("Delay (ms)")
plt.ylabel("Distance (cm)")
plt.tight_layout()

name = "speed_waves"
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
