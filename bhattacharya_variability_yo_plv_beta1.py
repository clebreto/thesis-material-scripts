import ztrcore

import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba
import matplotlib

import scipy as sc
import scipy.ndimage.filters as filters
import scipy.signal
from scipy.spatial import distance_matrix

import copy

from tqdm import tqdm

ztrcore.initialize()

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")

reader_edf_file.setFilePath("/user/clebreto/home/Data/Neurofeedback/2022_01_25_17_38_38_0001_ztrMarkerOneChunkImstantaneousCorrelation.edf")
reader_edf_file.run()
marker        = reader_edf_file.output()
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels())
nb_values = marker.shape[1]
duration = nb_values / sampling_rate

print("marker duration", duration)

reader_edf_file.setFilePath("/user/clebreto/home/Data/Neurofeedback/2022_01_25/2022_01_25_17_38_36_0001_neurofeedback_synchrony.edf")
reader_edf_file.run()
eeg        = reader_edf_file.output()
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels())
nb_values = marker.shape[1]
duration = nb_values / sampling_rate

print("eeg duration", duration)


nb_channels = len(channels)

plt.clf()
fg, ax = plt.subplots(1, 1)
ax.plot(np.linspace(0, duration, marker.shape[1]), marker[0], color='black')
ax.margins(x=0)

name = "neurofeedback_synchrony"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((60, 80), forward=False)
fg.savefig(name + ".png", dpi=50, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
