import numpy as np
import matplotlib.pyplot as plt
import sys, os, shutil

nb_samples = 1024

t_signal = np.linspace(0., 10., nb_samples)
f_signal_1 = 10.
f_signal_2 = 15.
f_signal_3 = 12.
signal = np.empty(shape=t_signal.shape)
signal[:int(nb_samples / 3) - 1] = np.sin(2. * np.pi * f_signal_1 * t_signal[:int(nb_samples / 3) - 1])
signal[int(nb_samples / 3) : int(2. * nb_samples / 3.) - 1] = np.sin(2. * np.pi * f_signal_2 * t_signal[int(nb_samples / 3) : int(2. * nb_samples / 3. - 1)])
signal[int(2. * nb_samples / 3.) - 1 : ] = np.sin(2. * np.pi * f_signal_3 * t_signal[int(2. * nb_samples / 3 - 1):])

duration = (t_signal[t_signal.shape[0] - 1] - t_signal[0])

signal = np.concatenate((signal, np.flip(signal, axis=0)))

f_min = 5.
f_max = 20.
f_step = 0.5
frequencies = np.arange(f_min, f_max, f_step)

width = 10.
t_morlet = np.linspace(- duration,  duration, 2 * nb_samples)
tf = np.empty(shape=(frequencies.shape[0], nb_samples), dtype=complex)
for f_i in range(frequencies.shape[0]) :
    omega = 2. * np.pi * frequencies[f_i]
    sigma = width / omega;
    morlet = 1. / np.sqrt(sigma * np.sqrt(np.pi)) * np.exp(1j * omega * t_morlet) * np.exp(- 0.5 * t_morlet * t_morlet / (sigma * sigma))
    tf[f_i] = np.fft.ifft(np.fft.fft(morlet) * np.fft.fft(signal))[nb_samples:]

plt.clf()
fig, ax = plt.subplots(1, 1)
ax.matshow(np.abs(tf))
fig.show()

name = "analytic_wavelet_convolve_sinus"
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((15.5, 12.5), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
