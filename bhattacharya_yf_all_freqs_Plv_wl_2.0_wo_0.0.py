import numpy as np
import matplotlib.pyplot as plt
from scipy import stats, optimize, interpolate
import pathlib as pl
import ztrcore
import shutil

## To load dynamic zither plugins
ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def nb_epochs_per_subject(files) :
    epochs = {}
    for f in files :
        subject = list(f.parts)[-2]
        if subject in epochs :
            epochs[subject] += 1
        else :
            epochs[subject] = 1
    return epochs

def measure(files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection) :
    short_range_pairs_indices = np.nonzero(np.triu(short_range_neighbors))
    short_range_nb_pairs = len(short_range_neighbors[short_range_pairs_indices])
    long_range_pairs_indices = np.nonzero(np.triu(long_range_neighbors))
    long_range_nb_pairs = len(long_range_neighbors[long_range_pairs_indices])

    short_range_output = np.empty(shape=(short_range_nb_pairs, frequencies.shape[0], 0))
    long_range_output = np.empty(shape=(long_range_nb_pairs, frequencies.shape[0], 0))

    for f in files :
        reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
        reader_edf_file.setFilePath(str(f))
        reader_edf_file.run()
        data          = reader_edf_file.output()
        sampling_rate = reader_edf_file.samplingRate()
        channels      = reader_edf_file.channels() # C x T (real)

        channels_selector = ztrcore.ztrProcessChannelsSelector()
        channels_selector.setChannels(channels)
        channels_selector.setSignal(data)
        channels_selector.setSelection(selection)
        channels_selector.run()
        selected_data   = channels_selector.output() # C x T (real)
        selected_channels = channels_selector.outputChannels()

        nb_channels = selected_data.shape[0]
        nb_values   = selected_data.shape[1]
        duration  = nb_values / sampling_rate

        filter_band_stop = ztrcore.ztrProcessFilterBandStopOffline()
        filter_band_stop.setBandwidth(7);
        filter_band_stop.setFrequency(50);
        filter_band_stop.setSamplingRate(sampling_rate);
        filter_band_stop.setSignal(selected_data);
        filter_band_stop.setOrder(6);
        filter_band_stop.run();
        filter_band_stop_output = filter_band_stop.output() # C x T (real)

        # Filter the signal on several frequency bands (center_freq + bandwidth)
        filter_bank = ztrcore.ztrProcessFilterBankOffline()
        filter_bank.setFrequencies(frequencies);
        filter_bank.setBandwidths(bandwidths);
        filter_bank.setSamplingRate(sampling_rate);
        filter_bank.setSignal(filter_band_stop_output);
        filter_bank.setOrder(6);
        filter_bank.run();
        filter_bank_output = filter_bank.output() # C x F x T (real)

        # Compute for each frequency band the analytic signal
        hilbert_bank = ztrcore.ztrProcessHilbertBank()
        hilbert_bank.setSamplingRate(sampling_rate);
        hilbert_bank.setSignal(filter_bank_output);
        hilbert_bank.run();
        hilbert_bank_output = hilbert_bank.output(); # C x F x T (complex)

        # Compute the synchrony measure on windows
        tfwt = ztrcore.ztrProcessTFWindowT(measure_name, hilbert_bank_output, sampling_rate, window_length, window_overlapping)
        tfwt.run()
        tfwt_output = tfwt.output() # C x C x F x T (real)

        nb_channels    = tfwt_output.shape[0];
        nb_frequencies = tfwt_output.shape[2];
        nb_windows     = tfwt_output.shape[3];

        temp_short_range_output = np.empty(shape=(short_range_nb_pairs, nb_frequencies, nb_windows))
        temp_long_range_output  = np.empty(shape=(long_range_nb_pairs, nb_frequencies, nb_windows))

        s_k = 0
        l_k = 0
        for i in range(nb_channels) :
            for j in range(i + 1, nb_channels) :
                if short_range_neighbors[i, j] == 1 :
                    temp_short_range_output[s_k, :, :] = tfwt_output[i, j, :, :]
                    s_k += 1
                if long_range_neighbors[i, j] == 1 :
                    temp_long_range_output[l_k, :, :] = tfwt_output[i, j, :, :]
                    l_k += 1

        short_range_output = np.concatenate((short_range_output, temp_short_range_output), axis=2)
        long_range_output = np.concatenate((long_range_output, temp_long_range_output), axis=2)
    return (short_range_output, long_range_output)

#epileptiques A, temoins B
def pltNormalTest(ax, A, B, title=None, A_name="", B_name="") :
    nb_pairs = A.shape[0]
    nb_frequencies = A.shape[1]

    normal_A = np.zeros((nb_pairs, nb_frequencies))
    normal_B = np.zeros((nb_pairs, nb_frequencies))
    for f in range(nb_frequencies) :
        for c in range(nb_pairs) :
            if(stats.normaltest(A[c, f, :]).pvalue > 0.05) :
                normal_A[c, f] = 1
            if(stats.normaltest(B[c, f, :]).pvalue > 0.05) :
                normal_B[c, f] = 1

    x = np.arange(nb_frequencies)
    width = 0.35

    ax.bar(x - width / 2, np.mean(normal_A, axis=0), width, label=B_name, color="white", edgecolor="black")
    ax.bar(x + width / 2, np.mean(normal_B, axis=0), width, label=A_name, color="black", edgecolor="black")
    ax.set_xticks(x)
    ax.set_xticklabels(["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"])
    ax.legend()
    ax.set_ylim([0,1])

    if title == None :
        title = "Null hypothesis : Normal distribution"
    ax.set_title(title)

def pltLeveneTest(ax, A, B, title=None, A_name="", B_name="") :
    nb_pairs = A.shape[0]
    nb_frequencies = A.shape[1]

    levene = np.zeros((nb_pairs, nb_frequencies))
    for f in range(nb_frequencies) :
        for c in range(nb_pairs) :
            if(stats.levene(A[c, f, :], B[c, f, :]).pvalue > 0.05) :
                levene[c, f] = 1

    x = np.arange(nb_frequencies)
    width = 0.35

    ax.bar(x - width / 2, np.mean(levene, axis=0), width, label=A_name + " vs " + B_name, color="grey", edgecolor="grey")
    ax.set_xticks(x)
    ax.set_xticklabels(["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"])
    ax.legend()
    ax.set_ylim([0,1])
    if title != None :
        ax.set_title(title)
    if title == None :
        title = "Null hypothesis : Same variance"
    ax.set_title(title)

def pltBartlettTest(ax, A, B, title=None, A_name="", B_name="") :
    nb_pairs = A.shape[0]
    nb_frequencies = A.shape[1]

    bartlett = np.zeros((nb_pairs, nb_frequencies))
    for f in range(nb_frequencies) :
        for c in range(nb_pairs) :
            if(stats.bartlett(A[c, f, :], B[c, f, :]).pvalue > 0.05) :
                bartlett[c, f] = 1

    x = np.arange(nb_frequencies)
    width = 0.35

    ax.bar(x - width / 2, np.mean(bartlett , axis=0), width, label=A_name + " vs " + B_name, color="grey", edgecolor="grey")
    ax.set_xticks(x)
    ax.set_xticklabels(["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"])
    ax.legend()
    ax.set_ylim([0,1])
    if title != None :
        ax.set_title(title)
    if title == None :
        title = "Null hypothesis : Same variance"
    ax.set_title(title)

def pltCompareManwhitneyU(ax, A, B, title=None, A_name="", B_name="") :
    nb_pairs = A.shape[0]
    nb_frequencies = A.shape[1]

    manwhitneyu_A = np.zeros((nb_frequencies))
    manwhitneyu_B = np.zeros((nb_frequencies))
    for f in range(nb_frequencies) :
        for c in range(nb_pairs) :
            if(stats.mannwhitneyu(A[c, f, :], B[c, f, :], use_continuity=False, alternative='greater').pvalue < 0.05) :
                manwhitneyu_A[f] += 1
            if(stats.mannwhitneyu(B[c, f, :], A[c, f, :], use_continuity=False, alternative='greater').pvalue < 0.05) :
                manwhitneyu_B[f] += 1

    manwhitneyu_A /= nb_pairs
    manwhitneyu_B /= nb_pairs

    x = np.arange(nb_frequencies)
    width = 0.35


    ax.bar(x - width / 2, manwhitneyu_A, width, label=A_name, color="black", edgecolor="black")
    ax.bar(x + width / 2, manwhitneyu_B, width, label=B_name, color="white", edgecolor="black")
    ax.set_xticks(x)
    ax.set_xticklabels(["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"])
    ax.legend()
    if title != None :
        ax.set_title(title)

def pltCompareHistograms(ax, A, B, title=None, A_name="", B_name="") :
    ax.hist(B[:,:,:].flatten(), alpha = 1., label = B_name, density = False, color='white', edgecolor='black', bins=100)
    ax.hist(A[:,:,:].flatten(), alpha = .5, label = A_name, density = False, color='black', edgecolor='black', bins=100)
    if title != None :
        ax.set_title(title)
    ax.legend()

def pltCompareHistogramsFreq(ax, A, B, f, title=None, A_name="", B_name="") :
    ax.hist(B[:,f,:].flatten(), alpha = 1., label = B_name, density = False, color='white', edgecolor='black', bins=100)
    ax.hist(A[:,f,:].flatten(), alpha = .5, label = A_name, density = False, color='black', edgecolor='black', bins=100)
    if title != None :
        ax.set_title(title)
    ax.legend()

def pltTableFiles(ax, nb_epochs_per_subject) :
    table = ax.table(cellText=[[v] for v in nb_epochs_per_subject.values()],
                 rowLabels=[k for k in nb_epochs_per_subject.keys()],
                 loc='bottom')
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    cell_dict = table.get_celld()
    for c in cell_dict.values():
        c.set_width(0.3)
        c.set_height(0.2)

# Parameters
frequencies = np.array([2.5, 5.5, 8.5, 11.5, 15.5, 24., 50.])
bandwidths = np.array([3., 3., 3., 3., 5., 12., 40.])

measure_name       = "Plv"
window_length      = 2.
window_overlapping = 0.

short_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/short_range.npy")
long_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/long_range.npy")

selection = ztrcore.readSelection("/home/clebreto/programming/come/zither/resources/selection/16battacharya.sel")

recordings = pl.Path("/home/clebreto/Data/Nice/")

condition = "yf"

condition_name = ""
if condition =="yo" :
    condition_name = "Eyes Open"
else :
    condition_name = "Eyes Closed"

A_files = np.array(list(recordings.glob("./Temoins/temoin_*/*" + condition + "*.edf")))
B_files = np.array(list(recordings.glob("./Epi_ZE_connue_SP/*/*" + condition + "*.edf")))

(A_short_range_output, A_long_range_output) = measure(A_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection)
(B_short_range_output, B_long_range_output) = measure(B_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection)

nb_frequencies = len(frequencies)
freq_names = ["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"]

####################### Plotting
plt.clf()
fg, ax = plt.subplots(nb_frequencies + 5 + 1, 2)

pltNormalTest(ax[0, 0], A_short_range_output, B_short_range_output, title="Normal Test for Short range", A_name="Control", B_name="Epileptic")
pltNormalTest(ax[0, 1], A_long_range_output, B_long_range_output, title="Normal Test for Long range", A_name="Control", B_name="Epileptic")
pltLeveneTest(ax[1, 0], A_short_range_output, B_short_range_output, title="Levene Test for Short range", A_name="Control", B_name="Epileptic")
pltLeveneTest(ax[1, 1], A_long_range_output, B_long_range_output, title="Levene Test for Long range", A_name="Control", B_name="Epileptic")
pltBartlettTest(ax[2, 0], A_short_range_output, B_short_range_output, title="Bartlett Test for Short range", A_name="Control", B_name="Epileptic")
pltBartlettTest(ax[2, 1], A_long_range_output, B_long_range_output, title="Bartlett Test for Long range", A_name="Control", B_name="Epileptic")

pltCompareManwhitneyU(ax[3, 0], A_short_range_output, B_short_range_output, title="Short range", A_name="Control", B_name="Epileptic")
pltCompareManwhitneyU(ax[3, 1], A_long_range_output, B_long_range_output, title="Long range", A_name="Control", B_name="Epileptic")

pltCompareHistograms(ax[4, 0], A_short_range_output, B_short_range_output, A_name="Control", B_name="Epileptic")
pltCompareHistograms(ax[4, 1], A_long_range_output, B_long_range_output, A_name="Control", B_name="Epileptic")
ax[4, 0].set_title("All", rotation='vertical',x=-0.1,y=0.5)

for f in range(nb_frequencies) :
    pltCompareHistogramsFreq(ax[5 + f, 0], A_short_range_output, B_short_range_output, f, A_name="Control", B_name="Epileptic")
    pltCompareHistogramsFreq(ax[5 + f, 1], A_long_range_output, B_long_range_output, f, A_name="Control", B_name="Epileptic")
    ax[5 + f, 0].set_title(freq_names[f], rotation='vertical',x=-0.1,y=0.)

pltTableFiles(ax[5 + nb_frequencies, 0], nb_epochs_per_subject(A_files))
pltTableFiles(ax[5 + nb_frequencies, 1], nb_epochs_per_subject(B_files))

fg.suptitle(condition_name + ", " + measure_name + ", Control nb windows : " + str(A_short_range_output.shape[2]) + " Epileptic nb windows :" + str(B_short_range_output.shape[2]))
plt.subplots_adjust(hspace = .5)

plt.draw()

name = "bhattacharya_" + condition + "_all_freqs_" + measure_name + "_wl_" + str(window_length) + "_wo_" + str(window_overlapping)
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 20.5), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
