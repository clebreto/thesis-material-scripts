import numpy as np
import matplotlib.pyplot as plt
import sys, os, shutil

width = 10.
f = 10.
omega = 2. * np.pi * f
duration = width / f

nb_samples = 1024
t = np.linspace(0., duration / 2.)
freq = np.fft.fftshift(np.fft.fftfreq(len(t), t[1] - t[0]))

sigma = width / omega;

morlet = np.exp(1j * omega * t) * np.exp(- t * t / (2 *sigma * sigma))

fft = np.fft.fftshift(np.fft.fft(morlet))

plt.clf()
fig, ax = plt.subplots(2, 1)
ax[0].plot(morlet.imag)
ax[0].plot(morlet.real)
#ax[1].plot(freq[int(nb_samples / 2 - 50) : int(nb_samples / 2 + 50)], fft.imag[int(nb_samples / 2 - 50) : int(nb_samples / 2 + 50)])
#ax[1].plot(freq[int(nb_samples / 2 - 50) : int(nb_samples / 2 + 50)], fft.real[int(nb_samples / 2 - 50) : int(nb_samples / 2 + 50)])
ax[1].plot(freq, fft.imag)
ax[1].plot(freq, fft.real)

name = "analytic_wavelet_second_half"
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((15.5, 12.5), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
