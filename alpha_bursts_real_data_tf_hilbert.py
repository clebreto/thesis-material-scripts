import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba
import matplotlib

import scipy as sc
from scipy import ndimage
from scipy import signal

from astroML.sum_of_norms import sum_of_norms, norm

from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def plotTFAmplitudes(fg, ax, time, frequencies, atf, events=None, event='', color='red', title='', xlabel='', ylabel='', norm=False) :
        time = np.asarray(time);
        if time.ndim != 1 :
            raise ValueError("time : must be a 1d array")
        frequencies = np.asarray(frequencies);
        if frequencies.ndim != 1 :
            raise ValueError("frequencies : must be a 1d array")

        sampling_rate = 1. / (time[1] - time[0])
        amplitudes = atf

        if norm is True :
            if events is not None:
                not_event_cc = np.ndarray(shape=(amplitudes.shape[0], 0))
                skip = False
                for e_i in range(len(events)) :
                    if events[e_i].uid != event :
                        if skip :
                            skip = False
                            continue
                        not_event_cc = np.concatenate((not_event_cc, amplitudes[:, int((events[e_i].timestamp / 1000 - time[0]) * sampling_rate) : int((events[e_i + 1].timestamp / 1000 - time[0]) * sampling_rate)]), axis=(1))
                        skip = True
                skip = False

                mean = np.expand_dims(np.mean(not_event_cc, axis=(1)), axis=(1))
                std = np.expand_dims(np.std(not_event_cc, axis=(1)), axis=(1))
                amplitudes = (amplitudes - mean) / std

        ratio = 1. / 10.

        x_decimation = 1
        y_decimation = 1

        if amplitudes.shape[0] < amplitudes.shape[1] :
          x_decimation = int(ratio * amplitudes.shape[1] / amplitudes.shape[0])

        amplitudes_to_show = amplitudes[::y_decimation, ::x_decimation]
        frequencies_to_show =  amplitudes_to_show.shape[0]
        time_to_show = amplitudes_to_show.shape[1]

        duration = time[-1] - time[0]

        ms = ax.imshow(amplitudes_to_show, origin='lower', cmap='binary')
        ax.set_xticks(np.linspace(0, time_to_show, 6))
        ax.set_xticklabels(["${0:.0f}$".format(x) for x in np.linspace(time[0], time[-1], 6)])
        ax.set_xlabel(xlabel, fontsize=40)
        ax.set_yticks(np.linspace(0, frequencies_to_show, 4))
        ax.set_yticklabels(["${0:.0f}$".format(x) for x in np.linspace(frequencies[0], frequencies[-1], 4)])
        ax.set_ylabel(ylabel, fontsize=40)
        ax.set_title(title)

        dvd = make_axes_locatable(ax)
        cax = dvd.append_axes('right', size='1%', pad=0.05)
        fg.colorbar(ms, cax=cax, orientation='vertical')

        handles = []
        if events is not None:
            skip = False
            for e_i in range(len(events)) :
                if events[e_i].uid in event :
                    if skip :
                        skip = False
                        continue
                    handles.append(ptc.Patch(edgecolor=color, facecolor=(0,0,0,0.0), fill=True, label=events[e_i].uid))
                    ax.add_patch(ptc.Rectangle( ((events[e_i].timestamp / 1000. - time[0]) * sampling_rate / x_decimation, 0), (events[e_i + 1].timestamp - events[e_i].timestamp) / 1000. * sampling_rate / x_decimation, amplitudes.shape[0], edgecolor = color, facecolor = (0,0,0,0.0), fill=True))
                    skip = True
            skip = False
        ax.legend(handles=handles)

def autocorr(s, max_lag_size) :
    result = np.zeros(shape=(s.shape[0], max_lag_size))
    s -= np.expand_dims(np.mean(s, axis=(1)), axis=(1))
    result[:, 0] = np.ones(shape=(s.shape[0]))
    for k in range(1, max_lag_size) :
        result[:, k] = np.sum(s[:, k:]*np.conj(s[:, :-k]), axis=(1))/(np.sqrt(np.sum(s[:, :-k]*s[:, :-k], axis=(1))*np.sum(s[:, k:]*s[:, k:], axis=(1))))
    return result

def yoyf(signal, events_reader_output, event="") :
    events = []
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if events_reader_output[e_i].uid == event :
            events.append(signal[:, int(events_reader_output[e_i].timestamp / 1000. * sampling_rate):int(events_reader_output[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return events

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

## Event selection
paradigm = "alpha"
event = "alpha"
subject_id = "0001"

## Channel selection
channels_selection = np.array(['O1'])

sampling_rate = 512
max_lag_duration = 10.  #seconds
max_lag_size = int(max_lag_duration * sampling_rate)  #samples

files = sorted(recordings.glob("./*11*/*" + str(subject_id) + "_" + paradigm + ".edf"))
files = [f for f in files]

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(files[0]))
reader_edf_file.run()

signal        = reader_edf_file.output()[:32, :] # 33 is GSR
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/" + paradigm + ".evt")
events_reader.run()
events_reader_output = events_reader.output()

## Selects only certain electrodes, and filter the signal
indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
signal = signal[indices_selection]
channels = channels[indices_selection]
nb_channels = len(channels)

highpass = ztrcore.ztrProcessFilterHighPassOffline()
highpass.setSignal(signal);
highpass.setFrequency(8.);
highpass.setSamplingRate(sampling_rate);
highpass.setOrder(4);
highpass.run();
highpass_output = np.copy(highpass.output());

lowpass = ztrcore.ztrProcessFilterLowPassOffline()
lowpass.setSignal(highpass_output);
lowpass.setFrequency(12.);
lowpass.setSamplingRate(sampling_rate);
lowpass.setOrder(4);
lowpass.run();
lowpass_output = np.copy(lowpass.output());

## Recovers the event as a list of matrices
signals = yoyf(lowpass_output, events_reader_output, event)

hilberts = []
for s in signals :
    hb = ztrcore.ztrProcessHilbertBank()
    hb.setSignal(np.expand_dims(s, axis=(1)))
    hb.setSamplingRate(512)
    hb.run()
    hilberts.append(hb.output())

amplitude = np.sqrt(np.imag(hilberts[0][0][0])*np.imag(hilberts[0][0][0]) + np.real(hilberts[0][0][0])*np.real(hilberts[0][0][0]))

autocorrelations = []
for s in signals :
    autocorrelation = autocorr(s, max_lag_size)
    autocorrelations.append(autocorrelation)

## Computes the wavelet coeficients with the wavelet trasnform first
fmin = 7.
fmax = 17.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 10.

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signals[0])
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(np.abs(wavelet_fourier.output()))

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(autocorrelations[0])
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
autocorr_wt = np.copy(np.abs(wavelet_fourier.output()))

scales = np.arange(1, 300, 1.)
scalespace = np.zeros(shape=(scales.shape[0], signals[0].shape[1]))

duration = amplitude.shape[0] / sampling_rate
for s_i, s in enumerate(scales) :
    win = sc.signal.ricker(amplitude.shape[0], s)
    scalespace[s_i] = sc.signal.convolve(amplitude, win, mode='same')

peaks = sc.signal.find_peaks(amplitude, prominence=1)[0]

##### Sum of Gaussians (non linear on amplitude) #####
time = np.linspace(0, amplitude.shape[0] / sampling_rate, signals[0].shape[1])
def sum_of_gaussian(params, time, peaks) :
    sog = np.zeros(time.shape[0])
    nb_params=peaks.shape[0]
    p1 = params[: nb_params]
    p2 = params[nb_params: 2 * nb_params]
    for p_i, p in enumerate(peaks) :
        peak_time = time - p / sampling_rate
        sog = sog + p1[p_i] * np.exp(-(peak_time * peak_time) / (2. * (p2[p_i] * p2[p_i])))
    return amplitude - sog

params_0 = np.ones(shape=(peaks.shape[0] * 2))
params_0[: peaks.shape[0]] = 10. #amplitude
params_0[peaks.shape[0] :] = .05 #width
sog_bounds_min = np.concatenate((1.0 * np.ones(shape=peaks.shape), .01 * np.ones(shape=peaks.shape)))
sog_bounds_max = np.concatenate((100 * np.ones(shape=peaks.shape), .2 * np.ones(shape=peaks.shape)))
sog_params = sc.optimize.least_squares(sum_of_gaussian, params_0, args=(time, peaks), method='trf', bounds=(sog_bounds_min, sog_bounds_max)).x

og = np.zeros(shape=(time.shape[0], peaks.shape[0]))
sog = np.zeros(shape=(time.shape[0]))
for p_i, p in enumerate(peaks) :
    peak_time = time - p / sampling_rate
    og[:, p_i] = sog_params[p_i] * np.exp(-(peak_time * peak_time) / (2. * (sog_params[peaks.shape[0] + p_i] * sog_params[peaks.shape[0] + p_i])))
    sog = sog + og[:, p_i]
##########

##### Sum of Bursts (non linear on signal) #####
def sum_of_bursts(params, time, peaks) :
    sob = np.zeros(time.shape[0])
    nb_params = peaks.shape[0]
    p1 = params[: nb_params]
    p2 = params[nb_params: 2 * nb_params]
    p3 = params[2 * nb_params : 3 * nb_params]
    p4 = params[3 * nb_params : 4 * nb_params]
    for p_i, p in enumerate(peaks) :
        peak_time = time - p / sampling_rate
        sob = sob + p1[p_i] * np.exp(-(peak_time * peak_time) / (2. * (p2[p_i] * p2[p_i]))) * np.cos(2. * np.pi * p4[p_i] * peak_time + p3[p_i])
    return signals[0][0] - sob

params_0 = np.ones(shape=(peaks.shape[0] * 4))
params_0[: peaks.shape[0]] = 10 # amplitude
params_0[peaks.shape[0] : 2 * peaks.shape[0]] = 0.05 # width
params_0[2 * peaks.shape[0] : 3 * peaks.shape[0]] = np.pi # phase shift
params_0[3 * peaks.shape[0] :] = 10. # frequency of cos
sob_bounds_min = np.concatenate((1. * np.ones(shape=peaks.shape), .01 * np.ones(shape=peaks.shape), 0. * np.ones(shape=peaks.shape), 9. * np.ones(shape=peaks.shape)))
sob_bounds_max = np.concatenate((30 * np.ones(shape=peaks.shape), .2 * np.ones(shape=peaks.shape), 2. * np.pi * np.ones(shape=peaks.shape), 11. * np.ones(shape=peaks.shape)))
sob_params = sc.optimize.least_squares(sum_of_bursts, params_0, args=(time, peaks), method='dogbox', bounds=(sob_bounds_min, sob_bounds_max)).x
ob = np.zeros(shape=(time.shape[0], peaks.shape[0]))
sob = np.zeros(shape=(time.shape[0]))
for p_i, p in enumerate(peaks) :
    peak_time = time - p / sampling_rate
    ob[:, p_i] = sob_params[p_i] * np.exp(-(peak_time * peak_time) / (2. * (sob_params[peaks.shape[0] + p_i] * sob_params[peaks.shape[0] + p_i]))) * np.cos(2. * np.pi * 10 * time)
    sob = sob + ob[:, p_i]
##########

##### Sum of norms (linear on amplitude) #####
w_best, rms, locs, widths = sum_of_norms(np.arange(0, amplitude.shape[0], 1),
                                         amplitude,
                                         locs=peaks,
                                         widths=60*np.ones(shape=peaks.shape),
                                         spacing='linear',
                                         full_output=True)
norms = w_best * norm(np.arange(0, amplitude.shape[0], 1)[:, None], locs, widths)
##########

plt.clf()
fg, ax = plt.subplots(5, 1)
ax[0].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), signals[0][0], color='black')
ax[0].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), amplitude, color='red')
ax[0].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), norms, color='blue')
ax[0].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), np.sum(norms, axis=(1)), color='purple')
ax[0].scatter(peaks / sampling_rate, amplitude[peaks])
ax[0].set_xlabel("$Time (s.)$")
ax[0].set_ylabel(r'$Voltage (\mu V)$')
ax[0].text(-0.075, 1.1, r'$\mathbf{a}$', transform=ax[0].transAxes, size=40)
ax[0].margins(x=0)
dvd = make_axes_locatable(ax[0])
cax = dvd.append_axes('right', size='1%', pad=0.05)
cax.set_visible(False)

ax[1].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), signals[0][0], color='black')
ax[1].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), amplitude, color='red')
ax[1].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), sog, color='orange')
ax[1].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), og, color='grey')
ax[1].scatter(peaks / sampling_rate, amplitude[peaks])
ax[1].set_xlabel("$Time (s.)$")
ax[1].set_ylabel(r'$Voltage (\mu V)$')
ax[1].text(-0.075, 1.1, r'$\mathbf{a}$', transform=ax[1].transAxes, size=40)
ax[1].margins(x=0)
dvd = make_axes_locatable(ax[1])
cax = dvd.append_axes('right', size='1%', pad=0.05)
cax.set_visible(False)

ax[2].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), signals[0][0], color='black')
ax[2].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), sob, color='orange')
ax[2].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), ob, color='grey')
ax[2].scatter(peaks / sampling_rate, amplitude[peaks])
ax[2].set_xlabel("$Time (s.)$")
ax[2].set_ylabel(r'$Voltage (\mu V)$')
ax[2].text(-0.075, 1.1, r'$\mathbf{a}$', transform=ax[2].transAxes, size=40)
ax[2].margins(x=0)
dvd = make_axes_locatable(ax[2])
cax = dvd.append_axes('right', size='1%', pad=0.05)
cax.set_visible(False)

name = paradigm + "_bursts_real_data_tf_hilbert"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((30, 25), forward=False)
fg.savefig(name + ".png", dpi=100, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
