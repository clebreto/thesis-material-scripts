import ztrcore
import ztrplotting
import numpy as np
from scipy.linalg import eigh
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits import mplot3d
import scipy as sc
from tqdm import tqdm

import matplotlib
matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 30,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 20

def multiple_formatter(denominator=2, number=np.pi, latex='\pi'):
    def gcd(a, b):
        while b:
            a, b = b, a%b
        return a
    def _multiple_formatter(x, pos):
        den = denominator
        num = int(np.rint(den*x/number))
        com = gcd(num,den)
        (num,den) = (int(num/com),int(den/com))
        if den==1:
            if num==0:
                return r'$0$'
            if num==1:
                return r'$%s$'%latex
            elif num==-1:
                return r'$-%s$'%latex
            else:
                return r'$%s%s$'%(num,latex)
        else:
            if num==1:
                return r'$\frac{%s}{%s}$'%(latex,den)
            elif num==-1:
                return r'$\frac{-%s}{%s}$'%(latex,den)
            else:
                return r'$\frac{%s%s}{%s}$'%(num,latex,den)
    return _multiple_formatter
​
class Multiple:
    def __init__(self, denominator=2, number=np.pi, latex='\pi'):
        self.denominator = denominator
        self.number = number
        self.latex = latex
​
    def locator(self):
        return plt.MultipleLocator(self.number / self.denominator)
​
    def formatter(self):
        return plt.FuncFormatter(multiple_formatter(self.denominator, self.number, self.latex))


ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

colors = np.array(["black", "red", "blue", "cyan", "orange"])

channels = ['O1', 'P3', 'C3', 'F3', 'Fp1']
pairs_indices = np.array([[1, 2, 3, 4], [0, 0, 0, 0]])

sampling_rate = 512.
duration = 10.

# Sources
nb_sources = 2
sources_positions = [[-1., 0., 0.], [1., 0., 0.]]
sources_positions = np.array(sources_positions)
sources_orientations = sources_positions / 10 # because centered on 0
sources_activities = 1.5*np.random.rand(nb_sources, int(duration * sampling_rate))

f = 10.
phi = 0.93 * np.pi
time = np.arange(0, duration, 1./sampling_rate)
sources_activities[0] += np.cos(2. * np.pi * f * time)
sources_activities[1] += np.cos(2. * np.pi * f * time + phi)

# Sensors
nb_sensors = 5
phi = np.linspace(-np.pi/4, np.pi/4, nb_sensors) + np.pi / 100. * np.random.rand(int(nb_sensors))
x = []
y = []
z = []
for p in phi:
    x.append(np.sin(p))
    y.append(0)
    z.append(np.cos(p))

x = np.array(x)
y = np.array(y)
z = np.array(z)
sensors_positions = 1.1 * np.vstack((x, y, z))
sensors_positions = np.transpose(sensors_positions)

s2s_proj = ztrcore.ztrProcessSourcesToSensorsProjection()
s2s_proj.setSourcesActivities(sources_activities)
s2s_proj.setSourcesPositions(sources_positions)
s2s_proj.setSourcesOrientations(sources_orientations)
s2s_proj.setSensorsPositions(sensors_positions)
s2s_proj.run()
sensors_activities = s2s_proj.sensorsActivities()


wavelet_width = 12
frequencies = np.linspace(8, 12, 4)

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(sensors_activities)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(wavelet_fourier.output())

amplitude = np.abs(wt)
phase = np.angle(wt)

w_phase_diff = np.empty(shape=(4, phase.shape[1], phase.shape[2]))
for p_i in range(1, 5) :
    w_phase_diff[p_i-1] = phase[p_i] - phase[0]

w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)

histograms = []
for p_i in range(1, 5) :
    histograms.append(np.histogram(w_phase_diff[p_i-1], bins=100, range=[-np.pi, np.pi])[0])
bins = np.histogram([], bins=100, range=[-np.pi, np.pi], weights=[])[1]
center = (bins[:-1] + bins[1:]) / 2

fg, ax = plt.subplots(1, 1)

for p_i in range(1, 5) :
    ax.plot(center, histograms[p_i-1] / np.sum(histograms[p_i-1]), color=colors[p_i-1], label=channels[pairs_indices[1][p_i-1]] + "-" + channels[pairs_indices[0][p_i-1]])
ax.xaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
ax.xaxis.set_minor_locator(plt.MultipleLocator(np.pi / 12))
ax.xaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))
ax.axvline(x=0.)
ax.legend()

fg.show()
name = "simulation_traveling_histos"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((20.5, 20.5), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
