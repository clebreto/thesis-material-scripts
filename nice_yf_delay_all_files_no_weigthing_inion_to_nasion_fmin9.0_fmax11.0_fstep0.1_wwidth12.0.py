import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from mpl_toolkits.axes_grid1 import make_axes_locatable

import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

recordings = pl.Path("/user/clebreto/home/Data/Nice/Temoins/")

headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")
positions = headset.positions()
labels    = np.array(headset.labels())
electrodes_positions = dict(zip(labels, positions))

nice_labels = np.array(['Fp1', 'Fpz', 'Fp2', 'F7', 'F3', 'Fz', 'F4', 'F8', 'T7', 'C3', 'Cz', 'C4', 'T8', 'P7', 'P3', 'Pz', 'P4', 'P8', 'O1', 'Oz', 'O2'])
nice_labels_indices = [np.where(nice_labels == l)[0][0] if l in nice_labels else np.nan for l in labels]

x = np.array([positions[x, 0] for x in range(len(positions))])
y = np.array([positions[y, 1] for y in range(len(positions))])

triangulation = tri.Triangulation(x, y)

pairs_labels = [["O1", "P3"], ["P3", "C3"], ["C3", "F3"], ["F3", "Fp1"], ["Oz", "Pz"], ["Pz", "Cz"], ["Cz", "Fz"], ["Fz", "Fpz"], ["O2", "P4"], ["P4", "C4"], ["C4", "F4"], ["F4", "Fp2"], ["P7", "T7"], ["T7", "F7"], ["P8", "T8"], ["T8", "F8"]]
pairs_indices = []
for p in pairs_labels :
    pairs_indices.append([np.where(labels == c)[0][0] for c in p])

short_range_neighbors = np.zeros(shape=(len(labels), len(labels)))
for p_i in pairs_indices :
    short_range_neighbors[p_i[1], p_i[0]] = 1

pairs_indices = np.nonzero(short_range_neighbors)
nb_pairs = len(short_range_neighbors[pairs_indices])

pairs_x = []
pairs_y = []
pairs_labels = []
for p_i in range(nb_pairs):
    pairs_labels.append(labels[pairs_indices[1][p_i]] + "-" + labels[pairs_indices[0][p_i]])
    pairs_x.append((positions[pairs_indices[1][p_i]][0] + positions[pairs_indices[0][p_i]][0]) / 2.)
    pairs_y.append((positions[pairs_indices[1][p_i]][1] + positions[pairs_indices[0][p_i]][1]) / 2.)

pairs_triangulation = tri.Triangulation(pairs_x, pairs_y)

## Wavelet Transform
fmin = 9.
fmax = 11.
fstep = 0.1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

nb_files = 0
files = recordings.glob("./temoin_*/*yf*.edf")
min_nb_values = float('inf')
for f in files :
    nb_files += 1
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output() # 33 is GSR
    nb_values = signal.shape[1]
    if nb_values < min_nb_values :
        min_nb_values = nb_values

sampling_rate = 256.

window_length = 2. # seconds
window_size = int(window_length * sampling_rate)
nb_windows = int(min_nb_values / window_size)

l = int(np.ceil(np.sqrt(nb_files)))
c = int(np.ceil(nb_files / l))
plt.clf()
fig, ax = plt.subplots(l, c)
files = recordings.glob("./temoin_*/*yf*.edf")
i = 0
skip = False
for f in files :
    print(str(f))
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:21, :min_nb_values]
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:21])
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output())

    ## Exttract the amplitude and the phase
    amplitude = np.abs(wt)
    phase = np.angle(wt)
    phase_diff = np.zeros(shape=(nb_pairs, phase.shape[1], phase.shape[2]))

    for p_i in range(nb_pairs) :
        phase_diff[p_i] = phase[nice_labels_indices[pairs_indices[1][p_i]]] - phase[nice_labels_indices[pairs_indices[0][p_i]]]

    phase_diff = np.where(phase_diff > np.pi, phase_diff - 2 * np.pi, phase_diff)
    phase_diff = np.where(phase_diff < -np.pi, phase_diff + 2 * np.pi, phase_diff)

    diff = np.mean(phase_diff, axis=(1, 2))

    colo = ax[int(np.floor(i / l)), i % c].tricontourf(pairs_triangulation, diff)
    for ep in electrodes_positions.items() :
        ax[int(np.floor(i / l)), i % c].annotate(ep[0], xy=(ep[1][0], ep[1][1]))
    ax[int(np.floor(i / l)), i % c].scatter(pairs_x, pairs_y, color='red', s=1)
    ax[int(np.floor(i / l)), i % c].scatter(x, y, color='black', s=1)
    ax[int(np.floor(i / l)), i % c].set_title(f.stem)
    divider = make_axes_locatable(ax[int(np.floor(i / l)), i % c])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(colo, cax=cax, orientation='vertical')

    i += 1

name = "nice_yf_delay_all_files_no_weigthing_inion_to_nasion_" + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((62, 50), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
