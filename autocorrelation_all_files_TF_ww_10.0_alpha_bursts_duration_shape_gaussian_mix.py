import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba
import matplotlib

import scipy as sc
from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def plotTFAmplitudes(fg, ax, time, frequencies, atf, events=None, event='', color='red', title='', norm=False) :

        time = np.asarray(time);
        if time.ndim != 1 :
            raise ValueError("time : must be a 1d array")
        frequencies = np.asarray(frequencies);
        if frequencies.ndim != 1 :
            raise ValueError("frequencies : must be a 1d array")

        sampling_rate = 1. / (time[1] - time[0])
        amplitudes = atf

        if norm is True :
            if events is not None:
                not_event_cc = np.ndarray(shape=(amplitudes.shape[0], 0))
                skip = False
                for e_i in range(len(events)) :
                    if events[e_i].uid != event :
                        if skip :
                            skip = False
                            continue
                        not_event_cc = np.concatenate((not_event_cc, amplitudes[:, int((events[e_i].timestamp / 1000 - time[0]) * sampling_rate) : int((events[e_i + 1].timestamp / 1000 - time[0]) * sampling_rate)]), axis=(1))
                        skip = True
                skip = False

                mean = np.expand_dims(np.mean(not_event_cc, axis=(1)), axis=(1))
                std = np.expand_dims(np.std(not_event_cc, axis=(1)), axis=(1))
                amplitudes = (amplitudes - mean) / std

        ratio = 1. / 10.

        x_decimation = 1
        y_decimation = 1

        if amplitudes.shape[0] < amplitudes.shape[1] :
          x_decimation = int(ratio * amplitudes.shape[1] / amplitudes.shape[0])

        amplitudes_to_show = amplitudes[::y_decimation, ::x_decimation]
        frequencies_to_show =  amplitudes_to_show.shape[0]
        time_to_show = amplitudes_to_show.shape[1]

        duration = time[-1] - time[0]

        ms = ax.imshow(amplitudes_to_show, origin='lower', cmap='binary')
        ax.set_xticks(np.linspace(0, time_to_show, 6))
        ax.set_xticklabels(["${0:.0f}$".format(x) for x in np.linspace(time[0], time[-1], 6)])
        ax.set_xlabel(r'$Lag, \delta_k (s.)$', fontsize=40)
        ax.set_yticks(np.linspace(0, frequencies_to_show, 4))
        ax.set_yticklabels(["${0:.0f}$".format(x) for x in np.linspace(frequencies[0], frequencies[-1], 4)])
        ax.set_ylabel(r'$\nu$', fontsize=40)
        ax.set_title(title)

        dvd = make_axes_locatable(ax)
        cax = dvd.append_axes('right', size='1%', pad=0.05)
        fg.colorbar(ms, cax=cax, orientation='vertical')

        handles = []
        if events is not None:
            skip = False
            for e_i in range(len(events)) :
                if events[e_i].uid in event :
                    if skip :
                        skip = False
                        continue
                    handles.append(ptc.Patch(edgecolor=color, facecolor=(0,0,0,0.0), fill=True, label=events[e_i].uid))
                    ax.add_patch(ptc.Rectangle( ((events[e_i].timestamp / 1000. - time[0]) * sampling_rate / x_decimation, 0), (events[e_i + 1].timestamp - events[e_i].timestamp) / 1000. * sampling_rate / x_decimation, amplitudes.shape[0], edgecolor = color, facecolor = (0,0,0,0.0), fill=True))
                    skip = True
            skip = False
        ax.legend(handles=handles)

def autocorr(s, max_lag_size) :
    result = np.zeros(shape=(s.shape[0], max_lag_size))
    s -= np.expand_dims(np.mean(s, axis=(1)), axis=(1))
    result[:, 0] = np.ones(shape=(s.shape[0]))
    for k in range(1, max_lag_size) :
        result[:, k] = np.sum(s[:, k:]*np.conj(s[:, :-k]), axis=(1))/(np.sqrt(np.sum(s[:, :-k]*s[:, :-k], axis=(1))*np.sum(s[:, k:]*s[:, k:], axis=(1))))
    return result

sampling_rate = 512
nb_channels = 4
max_lag_duration = 10.
max_lag_size = int(max_lag_duration * sampling_rate)

## Computes the wavelet coeficients with the wavelet trasnform first
fmin = 7.
fmax = 17.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 10.

wt = np.zeros(shape=(nb_channels, frequencies.shape[0], max_lag_size))

bursts_frequencies       = [10., 13., 2.]
bursts_durations         = [0.5,  2., 0.4]
bursts_durations_spread  = 0.1 * np.array([1.,  1., 1.])
bursts_spacings          = [0.3, 3., 10.]
bursts_spacings_spread   = np.array([0.05,  0.05, 3.])
bursts_amplitudes        = [15., 15., 60.]
bursts_amplitudes_spread = [4., 4., 4.]

duration = 20.
nb_channels = len(bursts_frequencies)

signal = np.zeros(shape=(1, int(sampling_rate * duration)))
for c_i in range(nb_channels):
    total = 0
    time = np.arange(0, duration, 1./sampling_rate)
    while total < 20 :
        burst_duration = np.random.normal(bursts_durations[c_i], bursts_durations_spread[c_i])
        burst_spacing = np.random.normal(bursts_spacings[c_i], bursts_spacings_spread[c_i])
        burst_amplitude = np.random.normal(bursts_amplitudes[c_i], bursts_amplitudes_spread[c_i])
        burst_frequency = bursts_frequencies[c_i]
        burst_duration_plus_spacings = (burst_duration + burst_spacing) * 1
        start = total + burst_duration_plus_spacings
        s = burst_duration / (2. * np.sqrt(2*np.log(2)))
        a = - ((time - start) * (time - start)) / (2. * s * s)
        A = burst_amplitude
        dephasage = -2. * np.pi * burst_frequency * start
        signal[0] += A * np.exp(a) * np.cos(2 * np.pi * burst_frequency * time + dephasage)
        total += burst_duration_plus_spacings

time = np.arange(0, duration, 1./sampling_rate)

signal_autocorr = autocorr(signal, max_lag_size)

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signal_autocorr)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(np.abs(wavelet_fourier.output()))

plt.clf()
fg, ax = plt.subplots(3, 1)
for c_i in range(nb_channels) :
    time = np.arange(0, duration, 1./sampling_rate)
    ax[0].plot(np.arange(0, duration, 1./sampling_rate), signal[0], color='black')
    delay = np.linspace(0., max_lag_size / sampling_rate, max_lag_size)
    alpha = np.linspace(1., 0.5, max_lag_size)
    points = np.array([delay, signal_autocorr[0]]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    colors = [to_rgba('black', alpha[v_i]) for v_i in range(signal_autocorr[0].shape[0])]
    line = LineCollection(segments, colors=colors)
    lc = ax[1].add_collection(line)
    ax[1].set_xlim(0, max_lag_duration)
    ax[1].set_ylim(-1, 1)
    ax[1].set_xlabel(r'$Lag, \delta_k (s.)$', fontsize=40)
    ax[1].set_ylabel(r'$\rho$', fontsize=40)
    ax[1].margins(x=0)
    dvd = make_axes_locatable(ax[1])
    cax = dvd.append_axes('right', size='1%', pad=0.05)
    cax.axis('off')
    plotTFAmplitudes(fg, ax[2], np.linspace(0, max_lag_size / sampling_rate, max_lag_size), frequencies, wt[0])

fg.suptitle("Morlet Wavelet Transform over generated alpha bursts duration ")
# Mean on channels of
name = "autocorrelation_all_files_TF_ww_" + str(wavelet_width) + "_alpha_bursts_duration_shape_gaussian_mix"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((30, 40), forward=False)
fg.savefig(name + ".png", dpi=300, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
