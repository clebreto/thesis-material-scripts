import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/home/clebreto/Data/Neurofeedback/2021_07_09/2021_07_09_17_55_39_00004_alpha.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()[:32]
sampling_rate = reader_edf_file.samplingRate()
channels      = reader_edf_file.channels()[:32]
nb_values = signal.shape[1]
duration = nb_values / sampling_rate
print(channels)
## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events = events_reader.output()

plt.clf()
fig, ax = plt.subplots(2, 1)

time = np.linspace(0, duration, nb_values)
frequencies = np.arange(9, 12, 0.1)

ztrplotting.plotTFAmplitudes(ax[0], time, frequencies, signal[2], w_width=13, events=events, event="alpha")
ztrplotting.plotTFPhases(ax[1], time, frequencies, signal[2], w_width=13, events=events, event="alpha")

name = "TF_" + "channel3" + file_path.name
shutil.copy("impact.py", name + ".py")
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
