import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

recordings = pl.Path("/user/clebreto/home/Data/Nice/Temoins/")


## Channel selection
channels_selection = np.array(['Fp1', 'Fpz', 'Fp2', 'F7', 'F3', 'Fz', 'F4', 'F8', 'T3', 'C3', 'Cz', 'C4', 'T4', 'T5', 'P3', 'Pz', 'P4', 'Pz', 'T6', 'O1', 'O2'])

## Wavelet Transform
fmin = 9.
fmax = 11
fstep = 0.5

wavelet_width = 12.

subject_id = "009"

files = np.array(list(recordings.glob("./temoin_" + subject_id + "/*.edf")))
weights = np.zeros(shape=(len(files), 21))
for f_i, f in enumerate(tqdm(files)) :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal        = reader_edf_file.output()
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels())

    ## Selection
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    signal = signal[indices_selection]
    channels = channels[indices_selection]
    print(channels)
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(3.)
    bp.setFrequency(10.)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()
    signal_filtered = bp.output()[:, 3*512: -3*512]

    # Solution 1
    M = signal_filtered
    n = M.shape[0]
    H = M.dot(M.T)

    K = H[0:-1,0:-1]
    h = H[-1,0:-1]
    a = H[-1,-1]
    o = np.ones((n-1))

    Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
    B = a*o-h

    try:
        x = np.linalg.solve(Q,B)
        w = np.append(x,1-np.dot(x,o))
    except np.linalg.LinAlgError as error:
        w =np.zeros(shape=(len(channels)))
    weights[f_i] = w

plt.clf()
fg, ax = plt.subplots(2, 1)
positions = np.arange(0, len(files), 1)
weights_f = [wf for wf in weights]
weights_c = [wc for wc in weights.T]
ax[0].boxplot(weights_f, showfliers=False)
ax[1].boxplot(weights_c, showfliers=False, labels=channels)

name = "NICE_minimization_all_files_" + subject_id
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 10), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
