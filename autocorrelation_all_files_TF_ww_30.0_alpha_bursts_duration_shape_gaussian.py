import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def plotTFAmplitudes(fg, ax, time, frequencies, atf, events=None, event='', color='red', title='', norm=False) :

        time = np.asarray(time);
        if time.ndim != 1 :
            raise ValueError("time : must be a 1d array")
        frequencies = np.asarray(frequencies);
        if frequencies.ndim != 1 :
            raise ValueError("frequencies : must be a 1d array")

        sampling_rate = 1. / (time[1] - time[0])
        amplitudes = atf

        if norm is True :
            if events is not None:
                not_event_cc = np.ndarray(shape=(amplitudes.shape[0], 0))
                skip = False
                for e_i in range(len(events)) :
                    if events[e_i].uid != event :
                        if skip :
                            skip = False
                            continue
                        not_event_cc = np.concatenate((not_event_cc, amplitudes[:, int((events[e_i].timestamp / 1000 - time[0]) * sampling_rate) : int((events[e_i + 1].timestamp / 1000 - time[0]) * sampling_rate)]), axis=(1))
                        skip = True
                skip = False

                mean = np.expand_dims(np.mean(not_event_cc, axis=(1)), axis=(1))
                std = np.expand_dims(np.std(not_event_cc, axis=(1)), axis=(1))
                amplitudes = (amplitudes - mean) / std

        ratio = 1. / 10.

        x_decimation = 1
        y_decimation = 1

        if amplitudes.shape[0] < amplitudes.shape[1] :
          x_decimation = int(ratio * amplitudes.shape[1] / amplitudes.shape[0])

        amplitudes_to_show = amplitudes[::y_decimation, ::x_decimation]
        frequencies_to_show =  amplitudes_to_show.shape[0]
        time_to_show = amplitudes_to_show.shape[1]

        duration = time[-1] - time[0]

        ms = ax.matshow(amplitudes_to_show, origin='lower')
        ax.set_xticks(np.linspace(0, time_to_show, 20))
        ax.set_xticklabels(["{0:.1f}".format(x) for x in np.linspace(time[0], time[-1], 20)])
        ax.set_yticks(np.linspace(0, frequencies_to_show, 10))
        ax.set_yticklabels(["{0:.1f}".format(x) for x in np.linspace(frequencies[0], frequencies[-1], 10)])
        ax.set_title(title)

        dvd = make_axes_locatable(ax)
        cax = dvd.append_axes('right', size='5%', pad=0.05)
        fg.colorbar(ms, cax=cax)

        handles = []
        if events is not None:
            skip = False
            for e_i in range(len(events)) :
                if events[e_i].uid in event :
                    if skip :
                        skip = False
                        continue
                    handles.append(ptc.Patch(edgecolor=color, facecolor=(0,0,0,0.0), fill=True, label=events[e_i].uid))
                    ax.add_patch(ptc.Rectangle( ((events[e_i].timestamp / 1000. - time[0]) * sampling_rate / x_decimation, 0), (events[e_i + 1].timestamp - events[e_i].timestamp) / 1000. * sampling_rate / x_decimation, amplitudes.shape[0], edgecolor = color, facecolor = (0,0,0,0.0), fill=True))
                    skip = True
            skip = False
        ax.legend(handles=handles)

def autocorr(s, max_lag_size) :
    result = np.zeros(shape=(s.shape[0], max_lag_size))
    s -= np.expand_dims(np.mean(s, axis=(1)), axis=(1))
    result[:, 0] = np.ones(shape=(s.shape[0]))
    for k in range(1, max_lag_size) :
        result[:, k] = np.sum(s[:, k:]*np.conj(s[:, :-k]), axis=(1))/(np.sqrt(np.sum(s[:, :-k]*s[:, :-k], axis=(1))*np.sum(s[:, k:]*s[:, k:], axis=(1))))
    return result

sampling_rate = 512
nb_channels = 4
max_lag_duration = 10.
max_lag_size = int(max_lag_duration * sampling_rate)

## Computes the wavelet coeficients with the wavelet trasnform first
fmin = 7.
fmax = 17.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 30.

wt = np.zeros(shape=(nb_channels, frequencies.shape[0], max_lag_size))

bursts_frequencies = [10., 11., 12., 15.]
bursts_durations =    [0.5,  1., 0.7, 0.3]
bursts_spacings =     [0.3, 0.4, 0.4, 0.3]
duration = 20.
nb_channels = len(bursts_frequencies)

time = np.arange(0, duration, 1./sampling_rate)

signal = np.zeros(shape=(nb_channels, int(sampling_rate * duration)))
for c_i in range(nb_channels):
    burst_duration_plus_spacings = (bursts_durations[c_i] + bursts_spacings[c_i]) *3
    nb_bursts = int(duration / burst_duration_plus_spacings)
    s = bursts_durations[c_i] / (2. * np.sqrt(2*np.log(2)))
    for b_i in range(nb_bursts + 1) :
            start = int(b_i * burst_duration_plus_spacings * sampling_rate)
            end = int((b_i + 1) * burst_duration_plus_spacings * sampling_rate)
            t = time[start:end]
            hd = (t[-1]-t[0])/2.
            a = - ((t - t[0] - hd) * (t - t[0] - hd)) / (2. * s * s)
            A = 1./s*np.sqrt(2.*np.pi)
            signal[c_i, start : end] = A * np.exp(a) * np.cos(2*np.pi*bursts_frequencies[c_i]*t)

signal_autocorr = autocorr(signal, max_lag_size)
wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signal_autocorr)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(np.abs(wavelet_fourier.output()))

plt.clf()
fg, ax = plt.subplots(nb_channels * 3, 1)
for c_i in range(nb_channels) :
        ax[3 * c_i].plot(np.arange(0, duration, 1./sampling_rate), signal[c_i])
        ax[3 * c_i].set_title("Frequency : " + str(bursts_frequencies[c_i]) + " Duration : " + str(bursts_durations[c_i]) + " Spacing " + str(bursts_spacings[c_i]))
        ax[3 * c_i + 1].plot(np.linspace(0, max_lag_size / sampling_rate, max_lag_size), signal_autocorr[c_i])
        plotTFAmplitudes(fg, ax[3 * c_i + 2], np.linspace(0, max_lag_size / sampling_rate, max_lag_size), frequencies, wt[c_i])

fg.suptitle("Morlet Wavelet Transform over generated alpha bursts duration ")
# Mean on channels of
name = "autocorrelation_all_files_TF_ww_" + str(wavelet_width) + "_alpha_bursts_duration_shape_gaussian"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((30, 55), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
