import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2020_11_24/2020_11_24_17_39_14_0001_calculus.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()[:32]
sampling_rate = reader_edf_file.samplingRate()
channels      = reader_edf_file.channels()[:32]
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/calculus.evt")
events_reader.run()
events_reader_output = events_reader.output()

fmin = 7.
fmax = 17.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)

width = 7.
## Computes the wavelet coeficients with the wavelet trasnform
wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signal)
wavelet_fourier.setWidth(width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wavelet_fourier_output = np.copy(wavelet_fourier.output())

time = np.linspace(0., duration, wavelet_fourier_output.shape[2])

plt.clf()
plt.imshow(np.abs(wavelet_fourier_output[0]), aspect="auto", origin='lower')
plt.text(5, 5, channels[0], bbox={'facecolor': 'white', 'pad': 10})
y_positions = np.arange(0, frequencies.size, 10)
y_labels = frequencies[::10]
plt.yticks(y_positions, np.around(y_labels, decimals=1))

name = "TF_calculus_baseline_nowindow_fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(width)
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
