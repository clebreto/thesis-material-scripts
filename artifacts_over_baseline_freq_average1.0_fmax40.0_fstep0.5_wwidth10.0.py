import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq

import matplotlib
matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 30,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 20

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2022_02_04/2022_02_04_16_00_49_0001_artefacts_cligne_machoires_droitegauche_hautbas_deglutir.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()
sampling_rate = reader_edf_file.samplingRate()
channels      = reader_edf_file.channels()
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

hp = ztrcore.ztrProcessFilterHighPassOffline()
hp.setFrequency(1.)
hp.setOrder(4)
hp.setSamplingRate(sampling_rate)
hp.setSignal(signal)
hp.run()
signal = np.copy(hp.output())

plt.clf()
fg, ax = plt.subplots(6, 1)
freq = np.fft.fftfreq(int(20.*sampling_rate), d=1./sampling_rate)
freq = freq[:freq.shape[0]//12]

b_ft = []
for i in range(6) :
    start = int((i * 50. + 5.) * sampling_rate) + int(sampling_rate)
    end   = int(start + 20. * sampling_rate) - int(sampling_rate)
    b_ft.append(np.fft.fft(signal[1, start:end])[:freq.shape[0]])
b_ft = np.array(b_ft)
b_ft = np.mean(b_ft, axis=0)
ax[0].plot(freq, np.abs(b_ft), label="baseline (eyes open)/n Average over 6 periods", lw=0.5, c="black")
ax[0].set(yticklabels=[])
ax[0].tick_params(left=False)
ax[0].legend()

artifacts = ["eye blinks", "jaw clench", "head right left", "head up down", "swallow"]
for a_i, a in enumerate(artifacts):
    start = int((a_i * 50 + 30.) * sampling_rate)
    end   = int(start + 20. * sampling_rate)
    print("start", start/sampling_rate, "end", end/sampling_rate)
    ft = np.fft.fft(signal[1, start:end])[:freq.shape[0]]
    # ax[a_i + 1].set(yticklabels=[])
    # ax[a_i + 1].tick_params(left=False)
    ax[a_i + 1].plot(freq, np.abs(ft) / np.abs(b_ft), label=a+" over baseline", lw=0.5, c="black")
    ax[a_i + 1].legend()


name = "artifacts_over_baseline_freq_average" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(width)
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15, 20), forward=False)
plt.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
