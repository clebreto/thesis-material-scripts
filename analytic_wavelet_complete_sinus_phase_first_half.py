import numpy as np
import matplotlib.pyplot as plt
import sys, os, shutil

nb_samples = 1024

t_signal = np.linspace(0., 10., nb_samples)
f_signal = 10.
signal = np.sin(2. * np.pi * f_signal * t_signal)

duration = (t_signal[t_signal.shape[0] - 1] - t_signal[0])

f_min = 8.
f_max = 12.
f_step = 0.1
frequencies = np.arange(f_min, f_max, f_step)

signal = np.concatenate((signal, np.flip(signal)))

t_morlet = np.linspace(-2. * duration / 2., 2. * duration / 2., nb_samples * 2)
tf = np.empty(shape=(frequencies.shape[0], nb_samples), dtype=complex)
for f_i in range(frequencies.shape[0]) :
    width = 10.
    omega = 2. * np.pi * frequencies[f_i]
    sigma = width / omega;
    morlet = np.exp(1j * omega * t_morlet) * np.exp(- t_morlet * t_morlet / (2 * sigma * sigma))
    tf[f_i] = np.fft.ifft(np.fft.fft(morlet) * np.fft.fft(signal))[nb_samples:]

t_morlet_first_half = np.linspace(-4. * duration / 2., 0, nb_samples * 2)
tf = np.empty(shape=(frequencies.shape[0], nb_samples), dtype=complex)
for f_i in range(frequencies.shape[0]) :
    width = 10.
    omega = 2. * np.pi * frequencies[f_i]
    sigma = width / omega;
    morlet = np.exp(1j * omega * t_morlet_first_half) * np.exp(- t_morlet_first_half * t_morlet_first_half / (2 * sigma * sigma))
    tf[f_i] = np.fft.ifft(np.fft.fft(morlet) * np.fft.fft(signal))[nb_samples:]

plt.clf()
fig, ax = plt.subplots(1, 1)
ax.matshow(np.angle(tf))
fig.show()

name = "analytic_wavelet_complete_sinus_phase_first_half"
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((15.5, 12.5), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
