import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events_reader_output = events_reader.output()

selection = ztrcore.readSelection("/home/clebreto/programming/come/zither/resources/selection/16battacharya.sel")

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

# Wavelet
wavelet_width = 12
fmin = 9.
fmax = 11.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)

# Synchrony measure
measure = "Plv"
window_length = .5
window_overlapping = 0.4

# Plot dimensions
nb_files = len(list(recordings.glob("./2020_11_*/*01_alpha.edf")))
l = int(np.ceil(np.sqrt(nb_files)))
c = int(np.ceil(nb_files / l))
plt.clf()
fig, ax = plt.subplots(l, c)

i = 0
files = sorted(recordings.glob("./2020_11_*/*01_alpha.edf"))
for f in files:
    file_path = pl.Path(str(f))
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(file_path))
    reader_edf_file.run()
    reader_edf_file_output        = reader_edf_file.output()
    reader_edf_file_sampling_rate = reader_edf_file.samplingRate()
    reader_edf_file_channels      = reader_edf_file.channels()

    nb_values = reader_edf_file_output.shape[1]
    duration = nb_values / reader_edf_file_sampling_rate

    channels_selector = ztrcore.ztrProcessChannelsSelector()
    channels_selector.setChannels(reader_edf_file_channels)
    channels_selector.setSignal(reader_edf_file_output)
    channels_selector.setSelection(selection)
    channels_selector.run()
    channels_selector_channels = channels_selector.outputChannels()
    channels_selector_output = channels_selector.output()
    channels_selector_output = channels_selector_output - np.mean(channels_selector_output, axis=0)

    ## Computes the wavelet coeficients with the wavelet trasnform
    ## It is required to compute the following synchrony measures
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(channels_selector_output)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(reader_edf_file_sampling_rate)
    wavelet_fourier.run()
    wavelet_fourier_output = wavelet_fourier.output()

    ## Computes the synchrony measure over all the montage channels
    ## with previously computed wavelet coefficients
    tfwt = ztrcore.ztrProcessTFWindowT(measure, wavelet_fourier_output, reader_edf_file_sampling_rate, window_length, window_overlapping)
    tfwt.run()
    window_t_output = np.copy(np.real(tfwt.output()))

    headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")
    positions = headset.positions()
    labels    = headset.labels()
    electrodes_positions = dict(zip(labels, positions))

    step_length = window_length - window_overlapping
    nb_steps = (duration - window_length) / step_length
    time = np.linspace(window_length / 2., duration - window_length / 2., window_t_output.shape[3])
    ztrplotting.plotNetworkZScore(ax[int(np.floor(i / l)), i % c], time, window_t_output, electrodes_positions, channels_selector_channels, events = events_reader_output, baseline='baseline', event='alpha')
    ax[int(np.floor(i / l)), i % c].set_title(f.stem)

    i += 1

name = "connectivity_mean_ref_alpha_zscore_network_measure_" + measure + "_window_length_" + str(window_length) + "window_overlapping_" + str(window_overlapping) + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((15.5, 12.5), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
