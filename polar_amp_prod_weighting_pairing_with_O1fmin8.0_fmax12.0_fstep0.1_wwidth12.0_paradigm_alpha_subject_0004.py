import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import FuncFormatter, MultipleLocator, FormatStrFormatter
import scipy as sc
from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def multiple_formatter(denominator=2, number=np.pi, latex='\pi'):
    def gcd(a, b):
        while b:
            a, b = b, a%b
        return a
    def _multiple_formatter(x, pos):
        den = denominator
        num = int(np.rint(den*x/number))
        com = gcd(num,den)
        (num,den) = (int(num/com),int(den/com))
        if den==1:
            if num==0:
                return r'$0$'
            if num==1:
                return r'$%s$'%latex
            elif num==-1:
                return r'$-%s$'%latex
            else:
                return r'$%s%s$'%(num,latex)
        else:
            if num==1:
                return r'$\frac{%s}{%s}$'%(latex,den)
            elif num==-1:
                return r'$\frac{-%s}{%s}$'%(latex,den)
            else:
                return r'$\frac{%s%s}{%s}$'%(num,latex,den)
    return _multiple_formatter
​
class Multiple:
    def __init__(self, denominator=2, number=np.pi, latex='\pi'):
        self.denominator = denominator
        self.number = number
        self.latex = latex
​
    def locator(self):
        return plt.MultipleLocator(self.number / self.denominator)
​
    def formatter(self):
        return plt.FuncFormatter(multiple_formatter(self.denominator, self.number, self.latex))

def yoyf(tf, events) :
    event_tfs = {}
    skip = False
    for e_i in range(len(events)) :
        if skip :
            skip = False
            continue
        if events[e_i].uid not in event_tfs.keys() :
            event_tfs[events[e_i].uid] = []
        event_tfs[events[e_i].uid].append(tf[:, :, int(events[e_i].timestamp / 1000. * sampling_rate):int(events[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return event_tfs

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

colors = np.array(["black", "red", "blue", "cyan", "orange"])
markers = np.array(["None", "+", "o", "x"])

## Channel selection
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])

## Wavelet Transform
fmin = 8.
fmax = 12.
fstep = 0.1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

paradigm = 'alpha' # possible values : 'alpha', 'calculus'
subject_id = "0004"
pairing = 'with_O1' # possible values : 'with_O1', 'chain'
weighting = 'amp_prod_weighting' # possible values : 'amp_prod_weighting', 'no_weighting'
reference_electrode = "CPz"

nb_files = 0
files = sorted(recordings.glob("./*/*" + str(subject_id) + "_" + paradigm + ".edf"))
files = [f for f in files]
min_nb_values = float('inf')
nb_channels = None
sampling_rate = 0
for f in tqdm(files) :
    nb_files += 1
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32] # 33 is GSR
    nb_values = signal.shape[1]
    nb_channels = signal.shape[0]
    if nb_values < min_nb_values :
        min_nb_values = nb_values

    sampling_rate = reader_edf_file.samplingRate()

    ## Select only certain electrodes, and filter the signal
    channels = np.array(reader_edf_file.channels())
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

Y_mask = []
## To compute on pairs
if pairing == 'chain' :
    Y_mask = np.eye(nb_channels, nb_channels, 1)
elif pairing == 'with_O1' :
    Y_mask = np.zeros(shape=(nb_channels, nb_channels))
    Y_mask[0, 1:] = np.ones(nb_channels - 1)
else :
    print("pairing not recognized")
    exit

pairs_indices = np.nonzero(Y_mask)
nb_pairs = len(Y_mask[pairs_indices])

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/" + paradigm + ".evt")
events_reader.run()
events = events_reader.output()

bars_diff = {}
for f in tqdm(files) :
    print(str(f))

    if str(f) not in bars_diff.keys() :
        bars_diff[str(f)] = {}

    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32, :min_nb_values] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Select only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]

    if reference_electrode in channels :
        reference = signal[[np.where(channels == c)[0][0] for c in [reference_electrode]]]
        signal = signal - reference

    signal = signal[indices_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output())

    ## Extract the amplitude and the phase
    amplitude = np.abs(wt)
    phase = np.angle(wt)

    ## Split between conditions
    phase_events     = yoyf(phase, events)
    amplitude_events = yoyf(amplitude, events)
    for ev in phase_events.keys() :
        print("ev", ev)
        if ev not in bars_diff[str(f)].keys() :
            bars_diff[str(f)][ev] = []

        for ph_i, am_i in zip(phase_events[ev], amplitude_events[ev]) :
            shape = ph_i.shape
            w_phase_diff = np.empty(shape=(nb_pairs, shape[1], shape[2]))
            w_amplitude_prod = np.empty(shape=(nb_pairs, shape[1], shape[2]))
            for p_i in range(nb_pairs) :
                w_phase_diff[p_i]     = ph_i[pairs_indices[1][p_i]] - ph_i[pairs_indices[0][p_i]]
                w_amplitude_prod[p_i] = am_i[pairs_indices[1][p_i]] * am_i[pairs_indices[0][p_i]]
            ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
            w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
            w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)

            bars_diff_block = []
            for p_i in range(nb_pairs) :
                if weighting == 'amp_prod_weighting' :
                    bars_diff_block.append(np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi], weights=w_amplitude_prod[p_i])[0])
                elif weighting == 'no_weighting' :
                    bars_diff_block.append(np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi])[0])
                # elif weighting == 'amp_mod_weighting' :
                #     # Since the phase differences in a window are not normaly distributed, we take the most representative value as the bin value of the highest bar
                #     bars, bins = np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi], weights=w_amplitude_prod[p_i])
                    # diff[p_i][w_i] = bins[np.argmax(bars)]
                else :
                    print("weighting not recognized")
            bars_diff[str(f)][ev].append(bars_diff_block)

## Creates the histograms to fill with data :
##  - as many histograms as pairs of channels x 2 (for the two conditions)
##  - with as many bins as 100 (100 bins between all the possible phase lags (-pi -> pi)
bins = np.histogram([], bins=100, range=[-np.pi, np.pi], weights=[])[1]

width = 0.7 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2

locations = {}
concentrations = {}
for f in files :
    for ev in phase_events.keys() :

        if ev not in locations.keys() :
            locations[ev] = {}
            concentrations[ev] = {}

        for i in range(len(bars_diff[str(f)][ev])) :
            for p_i in range(nb_pairs) :
                if p_i not in locations[ev].keys() :
                    locations[ev][p_i] = []
                    concentrations[ev][p_i] = []

                #One histogram for each pair and each block
                data_from_histo = sc.stats.rv_histogram((bars_diff[str(f)][ev][i][p_i], bins)).rvs(size=1000)
                fitted_bars_diff = np.histogram(data_from_histo, bins=100, range=[-np.pi, np.pi])[0]
                (concentration, location, scale) = vonmises.fit(data_from_histo, fscale=1.)
                locations[ev][p_i].append(location)
                concentrations[ev][p_i].append(concentration)

plt.clf()
fig, ax = plt.subplots(1, 1, subplot_kw={'projection': 'polar'})
for ev_i, ev in enumerate(phase_events.keys()) :
    for p_i, p in enumerate(locations[ev]) :
        ax.plot(center, np.histogram(np.array(locations[ev][p]), bins=100, range=[-np.pi, np.pi])[0], color=colors[p_i], marker=markers[ev_i], label=ev + channels[pairs_indices[1][p_i]] + "-" + channels[pairs_indices[0][p_i]])
ax.legend()

name = "polar_" + weighting + "_pairing_" + pairing + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width) + "_paradigm_" + paradigm + "_subject_" + subject_id
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((20.5, 20.5), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
