import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import FuncFormatter, MultipleLocator, FormatStrFormatter
from scipy import stats
from scipy.stats import vonmises
import scipy as sc
import scipy.ndimage.filters as filters
import scipy.signal
from scipy.spatial import distance_matrix

from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 40

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()
​
def yoyf(tf, events) :
    event_tfs = {}
    skip = False
    for e_i in range(len(events)) :
        if skip :
            skip = False
            continue
        if events[e_i].uid not in event_tfs.keys() :
            event_tfs[events[e_i].uid] = []
        event_tfs[events[e_i].uid].append(tf[:, :, int(events[e_i].timestamp / 1000. * sampling_rate):int(events[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return event_tfs

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

colors = np.array(["black", "red", "blue", "cyan", "orange"])
markers = np.array(["None", "+", "o", "x"])

## Channel selection
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])

## Wavelet Transform
fmin = 8.
fmax = 12.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 7.

paradigm = 'alpha' # possible values : 'alpha', 'calculus'
subject_id = "0001"
pairing = 'with_O1' # possible values : 'with_O1', 'chain'
weighting = 'amp_prod_weighting' # possible values : 'amp_prod_weighting', 'no_weighting'
reference_option = 'electrode' # possible values : 'optimized', 'electrode'
reference_electrode = 'M2' # if reference_option == 'electrode'
reference_strategy = 'sum1' # if reference_option = 'optimized', possible values : 'sum1', 'ratio'
crop = 3.

nb_files = 0
files = sorted(recordings.glob("./*/*" + str(subject_id) + "_" + paradigm + ".edf"))
files = [f for f in files][:2]
min_nb_values = float('inf')
nb_channels = None
sampling_rate = 0

for f in tqdm(files) :
    nb_files += 1
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32] # 33 is GSR
    nb_values = signal.shape[1]
    nb_channels = signal.shape[0]
    if nb_values < min_nb_values :
        min_nb_values = nb_values

    sampling_rate = reader_edf_file.samplingRate()

    ## Select only certain electrodes, and filter the signal
    channels = np.array(reader_edf_file.channels())
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

Y_mask = []
## To compute on pairs
if pairing == 'chain' :
    Y_mask = np.eye(nb_channels, nb_channels, 1)
elif pairing == 'with_O1' :
    Y_mask = np.zeros(shape=(nb_channels, nb_channels))
    Y_mask[0, 1:] = np.ones(nb_channels - 1)
else :
    print("pairing not recognized")
    exit

pairs_indices = np.nonzero(Y_mask)
nb_pairs = len(Y_mask[pairs_indices])

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/" + paradigm + ".evt")
events_reader.run()
events = events_reader.output()

burst_parameters = {}
for f in tqdm(files) :
    print(str(f))

    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32, :min_nb_values] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Select only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    if reference_option == 'electrode' :
        if reference_electrode in channels :
            reference = signal[[np.where(channels == c)[0][0] for c in [reference_electrode]]]
            signal = signal - reference
    else :
        channels_not_selection = [c for c in channels if c not in channels_selection]
        indices_not_selection = [np.where(channels == c)[0][0] for c in channels_not_selection]
        reference_signal = signal[indices_not_selection]
        center_frequency = (fmax + fmin) / 2.
        bandwidth = (fmax - fmin)
        bp = ztrcore.ztrProcessFilterBandPassOffline()
        bp.setFrequency(center_frequency)
        bp.setBandwidth(bandwidth)
        bp.setOrder(4)
        bp.setSamplingRate(sampling_rate)
        bp.setSignal(reference_signal)
        bp.run()
        M = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
        H = M.dot(M.T)
        if np.linalg.matrix_rank(H) != reference_signal.shape[0]:
            print("MATRIX NOT FULL RANK : ", np.linalg.matrix_rank(H))
            continue
        if reference_strategy == 'ratio' :
            S = reference_signal[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
            G = S.dot(S.T)
            (ratio_e_val, ratio_e_vec) = np.linalg.eig(np.dot(np.linalg.inv(G), H))
            weights = np.real(ratio_e_vec[:, np.argmin(ratio_e_val)]) # Select the vector associated to the smallest eigen value
        elif reference_strategy == 'sum1' :
            K = H[0:-1,0:-1]
            h = H[-1,0:-1]
            a = H[-1,-1]
            o = np.ones((M.shape[0]-1))
            Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
            B = a*o-h

            print("Q Rank : ", np.linalg.matrix_rank(Q))
            x = np.linalg.solve(Q,B)
            weights = np.append(x, 1 - np.dot(x,o))
        else :
            print("Reference strategy not recognized")
            exit
        reference = np.dot(weights, reference_signal)
        signal = signal - reference

    signal = signal[indices_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output())

    amplitude = np.abs(wt)
    phase= np.angle(wt)

    ## Split between conditions
    phase_events     = yoyf(phase, events)
    amplitude_events = yoyf(amplitude, events)
    for ev in amplitude_events.keys() :
        if ev not in burst_parameters.keys() :
            burst_parameters[ev] = {'t':[], 'f':[], 'a':[], 'p':[]}
        for (ph_i, am_i) in zip(phase_events[ev], amplitude_events[ev]) :
            for s_i in range(am_i.shape[0]) :
                local_maxima = filters.maximum_filter(am_i[s_i], (3,3), mode="nearest")==am_i[s_i]
                locations = np.array(np.where(local_maxima==True))
                burst_parameters[ev]['t'].append(locations[1]/sampling_rate)
                burst_parameters[ev]['f'].append(fmin + locations[0] * fstep)
                burst_parameters[ev]['a'].append(am_i[s_i][locations[0], locations[1]])
                burst_parameters[ev]['p'].append(ph_i[s_i][locations[0], locations[1]])

plt.clf()
fig, ax = plt.subplots(1, 1)
ax.scatter(burst_parameters['alpha']['t'][0], burst_parameters['alpha']['f'][0], marker='+', s=100)
ax.scatter(burst_parameters['alpha']['t'][1], burst_parameters['alpha']['f'][1], marker='+', s=100)
ax.scatter(burst_parameters['alpha']['t'][2], burst_parameters['alpha']['f'][2], marker='+', s=100)
ax.scatter(burst_parameters['alpha']['t'][3], burst_parameters['alpha']['f'][3], marker='+', s=100)

if reference_option == 'optimized' :
    name = "bursts_properties_" + weighting + "_pairing_" + pairing + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width) + "_paradigm_" + paradigm + "_reference_option_" + reference_option + "_reference_strategy_" + reference_strategy + "_subject_" + subject_id
elif reference_option == 'electrode' :
    name = "bursts_properties_" + weighting + "_pairing_" + pairing + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width) + "_paradigm_" + paradigm + "_reference_option_" + reference_option + "_reference_electrode_" + reference_electrode + "_subject_" + subject_id

shutil.copy("impact.py", name + ".py")
fig.set_size_inches((20.5, 20.5), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
