import numpy as np
import matplotlib.pyplot as plt
import sys, os, shutil

sampling_rate = 512.
duration = 3.
nb_samples = int(duration * sampling_rate)

t_signal = np.linspace(0., duration, int(nb_samples))
f = 10.
signal = np.sin(2. * np.pi * f * t_signal)

f_min = 1.
f_max = 19.
f_step = 0.1
frequencies = np.arange(f_min, f_max, f_step)

signal = np.concatenate((signal, np.flip(signal, axis=0)))

width = 7.
t_morlet = np.linspace(- duration,  duration, 2 * nb_samples)
tf = np.empty(shape=(frequencies.shape[0], nb_samples), dtype=complex)
for f_i in range(frequencies.shape[0]) :
    omega = 2. * np.pi * frequencies[f_i]
    sigma = width / omega;
    morlet = 1. / np.sqrt(sigma * np.sqrt(np.pi)) * np.exp(1j * omega * t_morlet) * np.exp(- 0.5 * t_morlet * t_morlet / (sigma * sigma))
    tf[f_i] = np.fft.ifft(np.fft.fft(morlet) * np.fft.fft(signal))[nb_samples:]


plt.clf()
fig, ax = plt.subplots(1, 1)
ax.matshow(np.angle(tf))
fig.show()

name = "analytic_wavelet_convolve_sinus_phase_shorter"
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((15.5, 12.5), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
