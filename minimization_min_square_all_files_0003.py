import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

subject_id = "0003"

fmin = 5.
fmax = 15.
fstep = 0.5
frequencies = np.arange(fmin, fmax, fstep)

wavelet_width = 12.

files = np.array(list(recordings.glob("./*/*" + subject_id + "_alpha.edf")))

nb_values = []
sampling_rates = []
nb_channels = []
for f in files :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal = reader_edf_file.output()[:32] # 33 is GSR
    nb_channels.append(signal.shape[0])
    sampling_rates.append(reader_edf_file.samplingRate())
    nb_values.append(signal.shape[1])

sampling_rate = sampling_rates[0]
nb_channels = nb_channels[0]
min_nb_values = np.min(np.array(nb_values))

# Weights
ev = np.zeros(shape=(0, nb_channels))
w = np.zeros(shape=(0, nb_channels))
# Residuals
residual_ev = np.zeros(shape=(0))
residual_w = np.zeros(shape=(0))
residual_mean = np.zeros(shape=(0))
# Time Frequency analysis
mean_tf_ev = np.zeros(shape=(frequencies.shape[0], min_nb_values), dtype=complex)
mean_tf_w = np.zeros(shape=(frequencies.shape[0], min_nb_values), dtype=complex)
# Distribution analysis
bars_old_reference = []
bars_ev = []
bars_w = []

for c_i in range(nb_channels) :
    bars_old_reference.append(np.histogram([], bins=100)[0])
    bars_ev.append(np.histogram([], bins=100)[0])
    bars_w.append(np.histogram([], bins=100)[0])

for f in tqdm(files) :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal        = reader_edf_file.output()[:32, :min_nb_values]
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32])

    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(3.)
    bp.setFrequency(10.)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()
    signal_filtered = bp.output()[:, 3*512: -3*512]

    M = signal_filtered
    n = M.shape[0]
    H = M.dot(M.T)

    if np.linalg.matrix_rank(H) == signal.shape[0]:
        # sum{w_i^2}=1
        (e_val, e_vec) = np.linalg.eig(H)
        s_e_vec = np.real(e_vec[:, np.argmin(e_val)]) # Select the vector associated to the smaller eigen value
        # Display eigen values
        print(e_val)
        s_e_vec_norm = s_e_vec / np.sum(s_e_vec)
        ev = np.vstack((ev, np.expand_dims(s_e_vec_norm, axis=0)))
        ref_ev = np.dot(s_e_vec_norm, signal_filtered)
        residual_ev = np.append(residual_ev, np.dot(ref_ev, ref_ev) / signal_filtered.shape[1])

        # wf_ev = ztrcore.ztrProcessWaveletFourier()
        # wf_ev.setFrequencies(frequencies)
        # wf_ev.setSignal(np.expand_dims(prod, axis=0))
        # wf_ev.setWidth(wavelet_width)
        # wf_ev.setSamplingRate(sampling_rate)
        # wf_ev.run()
        # wf_ev = wf_ev.output()
        # mean_tf_ev += np.mean(wf_ev, axis=0) # Mean over the channels

        # sum{w_i}=1
        K = H[0:-1,0:-1]
        h = H[-1,0:-1]
        a = H[-1,-1]
        o = np.ones((n-1))

        Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
        B = a*o-h

        x = np.linalg.solve(Q,B)
        x = np.append(x, 1 - np.dot(x,o))
        w = np.vstack((w, np.expand_dims(x, axis=0)))
        ref_w = np.dot(x, signal_filtered)
        residual_w = np.append(residual_w, np.dot(ref_w, ref_w) / signal_filtered.shape[1])

        mean = np.ones(signal_filtered.shape[0]) / signal_filtered.shape[0]
        ref_mean = np.dot(mean, signal_filtered)
        residual_mean = np.append(residual_mean, np.dot(ref_mean, ref_mean) / signal_filtered.shape[1])

        # wf_w = ztrcore.ztrProcessWaveletFourier()
        # wf_w.setFrequencies(frequencies)
        # wf_w.setSignal(prod)
        # wf_w.setWidth(wavelet_width)
        # wf_w.setSamplingRate(sampling_rate)
        # wf_w.run()
        # wf_w = wf_w.output()
        # mean_tf_w += np.mean(wf_w, axis=0) # Mean over the channels
        for c_i in range(signal_filtered.shape[0]):
            bars_old_reference[c_i] += np.histogram(signal_filtered[c_i], bins=100)[0]
            bars_ev[c_i] += np.histogram(signal_filtered[c_i] - ref_ev, bins=100)[0]
            bars_w[c_i] += np.histogram(signal_filtered[c_i] - ref_w, bins=100)[0]

    else:
        print("Matrix is not full rank")

# Mean over recordings
mean_tf_ev /= residual_mean.shape[0]
mean_tf_w /= residual_mean.shape[0]

plt.clf()
fg, ax = plt.subplots(5, 2)
positions = np.arange(0, ev.shape[0], 1)
ev_f = [ef for ef in ev]
ev_c = [ec for ec in ev.T]
w_f = [wf for wf in w]
w_c = [wc for wc in w.T]
ax[0, 0].boxplot(w_f, showfliers=False, showmeans=True)
ax[0, 0].set_title("sum{w_i}=1")
ax[1, 0].boxplot(w_c, showfliers=False, showmeans=True, labels=channels)
ax[1, 0].set_title("sum{w_i}=1")
ax[0, 1].boxplot(ev_f, showfliers=False, showmeans=True)
ax[0, 1].set_title("sum{w_i^2}=1")
ax[1, 1].boxplot(ev_c, labels=channels)
ax[1, 1].set_title("sum{w_i^2}=1")
for b_i, b in enumerate(bars_w) :
    ax[2, 0].plot(b, label=channels[b_i])
ax[2, 0].legend(ncol=3)
ax[2, 0].set_title("sum{w_i}=1")
for b_i, b in enumerate(bars_ev) :
    ax[2, 1].plot(b, label=channels[b_i])
ax[2, 1].legend(ncol=3)
ax[2, 1].set_title("sum{w_i^2}=1")
# ztrplotting.plotTFAmplitudes(ax[2, 1], np.linspace(0, duration, min_nb_values), mean_tf_ev)
ztrplotting.plotTopography(ax[3, 0], np.mean(w, axis=0), headset)
ztrplotting.plotTopography(ax[3, 1], np.mean(ev, axis=0), headset)
ax[4, 0].scatter(positions, residual_ev, label="sum{w_i^2}=1")
ax[4, 0].scatter(positions, residual_w, label="sum{w_i}=1")
ax[4, 0].scatter(positions, residual_mean, label="sum{w_i}=1, w_i = 1/n")
ax[4, 0].legend()

name = "minimization_min_square_all_files_" + subject_id
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((25.5, 10), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
o
