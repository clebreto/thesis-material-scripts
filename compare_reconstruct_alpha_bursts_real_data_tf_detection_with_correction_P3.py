import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba
import matplotlib

import scipy as sc
import scipy.ndimage.filters as filters
import scipy.signal
from scipy.spatial import distance_matrix

import copy

from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def yoyf(signal, events_reader_output, event="") :
    events = []
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if events_reader_output[e_i].uid == event :
            events.append(signal[:, int(events_reader_output[e_i].timestamp / 1000. * sampling_rate):int(events_reader_output[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return events

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

## Event selection
paradigm = "alpha"
event = "alpha"
subject_id = "0001"

## Channel selection
channels_selection = np.array(['P3'])

sampling_rate = 512
max_lag_duration = 10.  #seconds
max_lag_size = int(max_lag_duration * sampling_rate)  #samples

files = sorted(recordings.glob("./*11*/*" + str(subject_id) + "_" + paradigm + ".edf"))
files = [f for f in files]

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(files[0]))
reader_edf_file.run()

signal        = reader_edf_file.output()[:32, :] # 33 is GSR
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/" + paradigm + ".evt")
events_reader.run()
events_reader_output = events_reader.output()

## Selects only certain electrodes, and filter the signal
indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
signal = signal[indices_selection] - signal[18]
channels = channels[indices_selection]
nb_channels = len(channels)

## Recovers the event as a list of matrices
signals = yoyf(signal, events_reader_output, event)
duration = 20.

## Computes the wavelet coeficients with the wavelet transform first
fmin = 8
fmax = 12
fstep = .01
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 7.

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signals[0])
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt_ampli = np.copy(np.abs(wavelet_fourier.output()))[0]
wt_phase = np.copy(np.angle(wavelet_fourier.output()))[0]

local_maxima = filters.maximum_filter(wt_ampli, (3,3), mode="nearest")==wt_ampli
ma_wt_ampli = np.ma.masked_where(local_maxima, wt_ampli)
ma_wt_phase = np.ma.masked_where(local_maxima, wt_phase)

locations = np.array(np.where(local_maxima==True))

## Reconstruct the signal
burst_duration = 0.5
bursts_frequencies = locations[0] * fstep + fmin
bursts_durations   = np.ones(shape=locations[0].shape[0]) * burst_duration
bursts_locations   = locations[1] / sampling_rate
bursts_amplitudes  = wt_ampli[locations[0], locations[1]]
bursts_phases      = wt_phase[locations[0], locations[1]]

print("Number of local maxima detected : ", locations.shape[1])

regenerated_signal = np.zeros(shape=(1, int(sampling_rate * duration)))
time = np.arange(0, duration, 1./sampling_rate)
for l, a, f, d, p  in zip(bursts_locations, bursts_amplitudes, bursts_frequencies, bursts_durations, bursts_phases) :
    s = d / (2. * np.sqrt(2*np.log(2)))
    G = np.exp(- ((time - l)**2) / (2. * s**2))
    regenerated_signal += a * G * np.cos(2 * np.pi * f * (time - l) + p)

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(regenerated_signal)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
regenerated_wt_ampli = np.copy(np.abs(wavelet_fourier.output()))[0]
regenerated_wt_phase = np.copy(np.angle(wavelet_fourier.output()))[0]

highpass = ztrcore.ztrProcessFilterHighPassOffline()
highpass.setSignal(signals[0]);
highpass.setFrequency(fmin);
highpass.setSamplingRate(sampling_rate);
highpass.setOrder(4);
highpass.run();
highpass_output = np.copy(highpass.output());

lowpass = ztrcore.ztrProcessFilterLowPassOffline()
lowpass.setSignal(highpass_output);
lowpass.setFrequency(fmax);
lowpass.setSamplingRate(sampling_rate);
lowpass.setOrder(4);
lowpass.run();
lowpass_output = np.copy(lowpass.output());

# Fix the amplitudes
distances = np.zeros(shape=(len(locations[1]), len(locations[1])))
for i, (bl_i, bf_i, bp_i) in enumerate(zip(bursts_locations, bursts_frequencies, bursts_phases)) :
    bl_i = bl_i - bp_i/(2.*np.pi*bf_i)
    for j, (bl_j, bf_j, bp_j)  in enumerate(zip(bursts_locations, bursts_frequencies, bursts_phases)) :
        bl_j = bl_j - bp_j/(2.*np.pi*bf_j)
        distances[i, j] = abs((bl_j - bl_i))

S = burst_duration / (2. * np.sqrt(2.*np.log(2.))) * np.ones(shape=distances.shape)
M = np.exp(-(distances*distances)/(2. * S * S))

bursts_amplitudes_corrected = np.linalg.solve(M, bursts_amplitudes)
print(np.sum(bursts_amplitudes_corrected - bursts_amplitudes))
# for i in range(distances.shape[0]) :
#     for j in range(i + 1, distances.shape[1]) :
#         if distances[i, j] !=0 :
#             s = duration / (2. * np.sqrt(2*np.log(2)))
#             dt2 = distances[i,j] ** 2
#             e0 = np.exp(- dt2 / (2*s*s))
#             e1 = np.exp(- dt2 / (2*s*s))
#             a0 = (bursts_amplitudes[i] - bursts_amplitudes[j] * e1) / (1 - e0 * e1)
#             a1 = bursts_amplitudes[j] - a0 * e0
#             bursts_amplitudes_corrected[i] = a0
#             bursts_amplitudes_corrected[j] = a1
#             print("e0", e0, "e1", e1, "bursts_amplitudes[i]", bursts_amplitudes[i], "a0", a0, "bursts_amplitudes[j]", bursts_amplitudes[j], "a1", a1)

regenerated_signal_corrected = np.zeros(shape=(1, int(sampling_rate * duration)))
time = np.arange(0, duration, 1./sampling_rate)
for l, a, f, d, p  in zip(bursts_locations, bursts_amplitudes_corrected, bursts_frequencies, bursts_durations, bursts_phases) :
    s = d / (2. * np.sqrt(2*np.log(2)))
    G = np.exp(- ((time - l)**2) / (2. * s**2))
    regenerated_signal_corrected += a * G * np.cos(2 * np.pi * f * (time - l) + p)

lowpass_output[0][:int(1*sampling_rate)]=0
lowpass_output[0][-int(1*sampling_rate):]=0
regenerated_signal[0][:int(1*sampling_rate)]=0
regenerated_signal[0][-int(1*sampling_rate):]=0
regenerated_signal_corrected[0][:int(2*sampling_rate)]=0
regenerated_signal_corrected[0][-int(2*sampling_rate):]=0

lp_normalized = (lowpass_output[0] - np.min(lowpass_output[0]))/(np.max(lowpass_output[0]) - np.min(lowpass_output[0]))
rg_normalized = (regenerated_signal[0] - np.min(regenerated_signal[0]))/(np.max(regenerated_signal[0]) - np.min(regenerated_signal[0]))
rg_normalized_corrected = (regenerated_signal_corrected[0] - np.min(regenerated_signal_corrected[0]))/(np.max(regenerated_signal_corrected[0]) - np.min(regenerated_signal_corrected[0]))
# rg_normalized_corrected = regenerated_signal_corrected[0]

plt.clf()
fg, ax = plt.subplots(1, 1)

ax.plot(np.linspace(0, duration, signals[0].shape[1]), lp_normalized, color='black', alpha=0.5, label="Original")
ax.plot(np.linspace(0, duration, signals[0].shape[1]), rg_normalized, color='red', alpha=0.5, label="Reconstructed")
for b_i in range(bursts_locations.shape[0]) :
    ax.axvline(x=bursts_locations[b_i]+bursts_phases[b_i]/(2.*np.pi*bursts_frequencies[b_i]), color="black", linestyle="--")
ax.margins(x=0)
ax.set_ylabel("Normalized Amplitude")
ax.set_xlabel("Time ($s$)")
ax.legend()

name = "compare_reconstruct_" + paradigm + "_bursts_real_data_tf_detection_with_correction_P3"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((30, 10), forward=False)
fg.savefig(name + ".png", dpi=100, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
