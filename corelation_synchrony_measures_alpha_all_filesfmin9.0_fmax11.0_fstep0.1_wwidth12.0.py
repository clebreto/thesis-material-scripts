# This script aims at comparing the mean and variance properties of synchrony measures for varying window sizes
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import pathlib as pl

import ztrplotting
import ztrcore

## To compute and save
ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events = events_reader.output()

baseline_measures = []
alpha_measures = []

fmin  = 9.
fmax  = 11.
fstep = .1
width = 7.

baseline_nb_events=0
alpha_nb_events=0
recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")
files = recordings.glob("./2020_11_*/*01_alpha.edf")
for f in files :
    print(f)
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    reader_edf_file_output = reader_edf_file.output()[:-1,:]
    sampling_rate          = reader_edf_file.samplingRate()
    channels               = reader_edf_file.channels()[:-1]

    nb_values = reader_edf_file_output.shape[1]
    duration = nb_values / sampling_rate


    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(np.arange(fmin, fmax, fstep))
    wavelet_fourier.setSignal(reader_edf_file_output)
    wavelet_fourier.setWidth(7.)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wavelet_fourier_output = np.copy(wavelet_fourier.output())

    band_pass_filter = ztrcore.ztrProcessFilterBandPassOffline()
    band_pass_filter.setSignal(reader_edf_file_output)
    band_pass_filter.setFrequency((fmin + fmax) / 2.)
    band_pass_filter.setBandwidth((fmax - fmin) / 1.5)
    band_pass_filter.setSamplingRate(sampling_rate)
    band_pass_filter.setOrder(4)
    band_pass_filter.run()
    band_pass_filter_output = np.copy(band_pass_filter.output())

    tf_measures_labels = ["ImCoh", "Coh", "AmpCorr", "Pli", "Plv"]
    ct_measures_labels = ["Esd"]

    time = np.linspace(0, wavelet_fourier_output.shape[2] / sampling_rate, wavelet_fourier_output.shape[2])

    max_delay          = 1. # seconds

    window_length      = 2. # seconds
    window_overlapping = 1. # seconds

    embedding_length = 1. # seconds
    shift_length     = .1 # seconds

    nb_measures = len(tf_measures_labels) + len(ct_measures_labels)
    skip = False
    for e_i in range(len(events)) :
        if events[e_i].uid == 'baseline' :
            if skip :
                skip = False
                continue
            skip = True
            tf_temp = np.copy(wavelet_fourier_output[:, :, int(events[e_i].timestamp / 1000. * sampling_rate) : int(events[e_i + 1].timestamp / 1000. * sampling_rate)])
            print(tf_temp.flags['C_CONTIGUOUS'])
            measures_event = []
            for m in tf_measures_labels :
                tfwt = ztrcore.ztrProcessTFWindowT(m, tf_temp, sampling_rate, window_length, window_overlapping)
                tfwt.run()
                measures_event.append(np.copy(np.mean(tfwt.output(), axis=(0, 1, 2))))
                print(m, " measure")
            ct_temp = np.copy(reader_edf_file_output[:, int(events[e_i].timestamp / 1000. * sampling_rate) : int(events[e_i + 1].timestamp / 1000. * sampling_rate)])
            for m in ct_measures_labels :
                ctwt = ztrcore.ztrProcessCTWEsd(ct_temp, sampling_rate, window_length, window_overlapping, embedding_length, shift_length)
                ctwt.run()
`                measures_event.append(ctwt.output())
                print(m, " measure")
            print(e_i, " event")
            baseline_measures.append(measures_event)
            baseline_nb_events += 1

    skip = False
    for e_i in range(len(events)) :
        if events[e_i].uid == 'alpha' :
            if skip :
                skip = False
                continue
            skip = True
            tf_temp = np.copy(wavelet_fourier_output[:, :, int(events[e_i].timestamp / 1000. * sampling_rate) : int(events[e_i + 1].timestamp / 1000. * sampling_rate)])
            print(tf_temp.flags['C_CONTIGUOUS'])
            measures_event = []
            for m in tf_measures_labels :
                tfwt = ztrcore.ztrProcessTFWindowT(m, tf_temp, sampling_rate, window_length, window_overlapping)
                tfwt.run()
                measures_event.append(np.copy(np.mean(tfwt.output(), axis=(0, 1, 2))))
                print(m, " measure")
            ct_temp = np.copy(reader_edf_file_output[:, int(events[e_i].timestamp / 1000. * sampling_rate) : int(events[e_i + 1].timestamp / 1000. * sampling_rate)])
            for m in ct_measures_labels :
                ctwt = ztrcore.ztrProcessCTWEsd(ct_temp, sampling_rate, window_length, window_overlapping, embedding_length, shift_length)
                ctwt.run()
                measures_event.append(ctwt.output())
                print(m, " measure")
            print(e_i, " event")
            alpha_measures.append(measures_event)
            alpha_nb_events += 1

baseline_corr_coeff = np.zeros(shape=(nb_measures, nb_measures, baseline_nb_events))
for m_i in range(nb_measures) :
    for m_j in range(m_i + 1, nb_measures) :
        for e_i in range(baseline_nb_events) :
            baseline_corr_coeff[m_i, m_j, e_i] = np.corrcoef(baseline_measures[e_i][m_i], baseline_measures[e_i][m_j])[0, 1]

alpha_corr_coeff = np.zeros(shape=(nb_measures, nb_measures, alpha_nb_events))
for m_i in range(nb_measures) :
    for m_j in range(m_i + 1, nb_measures) :
        for e_i in range(alpha_nb_events) :
            alpha_corr_coeff[m_i, m_j, e_i] = np.corrcoef(alpha_measures[e_i][m_i], alpha_measures[e_i][m_j])[0, 1]

print(np.mean(alpha_corr_coeff, axis=(2)))
print(np.mean(baseline_corr_coeff, axis=(2)))

plt.clf()
fg, ax = plt.subplots(1, 2)
print(np.mean(baseline_corr_coeff, axis=(2)))
print(np.mean(alpha_corr_coeff, axis=(2)).T)
ax0 = ax[0].matshow(np.mean(baseline_corr_coeff, axis=(2)) + np.mean(alpha_corr_coeff, axis=(2)).T, vmin=-1., vmax=1., cmap="bwr")
ax[0].set_xticks(np.arange(0, nb_measures))
ax[0].set_xticklabels(tf_measures_labels + ct_measures_labels)
ax[0].set_yticks(np.arange(0, nb_measures))
ax[0].set_yticklabels(tf_measures_labels + ct_measures_labels)
divider = make_axes_locatable(ax[0])
cax = divider.append_axes('right', size='5%', pad=0.05)
fg.colorbar(ax0, cax=cax, orientation='vertical')

ax1 = ax[1].matshow(np.std(baseline_corr_coeff, axis=(2)) + np.std(alpha_corr_coeff, axis=(2)).T, vmin=-1., vmax=1., cmap="bwr")
ax[1].set_xticks(np.arange(0, nb_measures))
ax[1].set_xticklabels(tf_measures_labels + ct_measures_labels)
ax[1].set_yticks(np.arange(0, nb_measures))
ax[1].set_yticklabels(tf_measures_labels + ct_measures_labels)
divider = make_axes_locatable(ax[1])
cax = divider.append_axes('right', size='5%', pad=0.05)
fg.colorbar(ax1, cax=cax, orientation='vertical')

name = "corelation_synchrony_measures_alpha_all_files" + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 12.5), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
