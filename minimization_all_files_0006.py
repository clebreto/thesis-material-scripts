import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

## Wavelet Transform
fmin = 9.
fmax = 11
fstep = 0.5

wavelet_width = 12.

subject_id = "0006"

files = np.array(list(recordings.glob("./*/*" + subject_id + "_alpha.edf")))
weights = np.zeros(shape=(len(files), 32))
bins = np.histogram([], bins=100)[1]
bars = []
for c_i in range(32) :
    bars.append(np.histogram([], bins=100)[0])

for f_i, f in enumerate(tqdm(files)) :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal        = reader_edf_file.output()[:32, :]
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:])

    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(3.)
    bp.setFrequency(10.)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()
    signal_filtered = bp.output()[:, 3*512: -3*512]

    for s_i, s in enumerate(signal_filtered):
        bars[s_i] += np.histogram(s, bins=100)[0]
    plt.plot(signal_filtered[0])
    plt.show()
    M = signal_filtered
    n = M.shape[0]
    H = M.dot(M.T)

    K = H[0:-1,0:-1]
    h = H[-1,0:-1]
    a = H[-1,-1]
    o = np.ones((n-1))

    Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
    B = a*o-h

    try:
        x = np.linalg.solve(Q,B)
        w = np.append(x,1-np.dot(x,o))
    except np.linalg.LinAlgError as error:
        w =np.zeros(shape=(len(channels) - 1))
    weights[f_i] = w

plt.clf()
fg, ax = plt.subplots(2, 1)
positions = np.arange(0, len(files), 1)
weights_f = [wf for wf in weights]
weights_c = [wc for wc in weights.T]
ax[0].boxplot(weights_f, showfliers=False, showmeans=True)
ax[1].boxplot(weights_c, showfliers=False, showmeans=True, labels=channels[:-1])

name = "minimization_all_files_" + subject_id
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 10), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
