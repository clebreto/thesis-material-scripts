import numpy as np
import matplotlib.pyplot as plt
import sys, os, shutil

sampling_rate = 512.
duration = 10.
nb_samples = int(duration * sampling_rate)

t_signal = np.linspace(0., duration, int(nb_samples))
f_signal_1 = 10.
f_signal_2 = 15.
f_signal_3 = 12.
signal = np.empty(shape=t_signal.shape)
signal[:int(nb_samples / 3) - 1] = np.sin(2. * np.pi * f_signal_1 * t_signal[:int(nb_samples / 3) - 1])
signal[int(nb_samples / 3) : int(2. * nb_samples / 3.) - 1] = np.sin(2. * np.pi * f_signal_2 * t_signal[int(nb_samples / 3) : int(2. * nb_samples / 3. - 1)])
signal[int(2. * nb_samples / 3.) - 1 : ] = np.sin(2. * np.pi * f_signal_3 * t_signal[int(2. * nb_samples / 3 - 1):])

f_min = 5.
f_max = 20.
f_step = 0.5
frequencies = np.arange(f_min, f_max, f_step)

width = 10.

w_duration = .5
w_nb_samples = int(w_duration * sampling_rate)
nb_windows = int(duration / w_duration)
w_t_morlet =  np.linspace(- w_duration,  w_duration, 2 * int(w_nb_samples))
windowed_tf = np.empty(shape=(frequencies.shape[0], nb_samples), dtype=complex)
for f_i in range(frequencies.shape[0]) :
    omega = 2. * np.pi * frequencies[f_i]
    sigma = width / omega;
    morlet = 1. / np.sqrt(sigma * np.sqrt(np.pi)) * np.exp(1j * omega * w_t_morlet) * np.exp(- 0.5 * w_t_morlet * w_t_morlet / (sigma * sigma))
    morlet_fft = np.fft.fft(morlet)
    for w_i in range(nb_windows) :
        w_signal = np.concatenate((signal[w_i * w_nb_samples : (w_i + 1) * w_nb_samples], np.flip(signal[w_i * w_nb_samples : (w_i + 1) * w_nb_samples], axis=0)))
        windowed_tf[f_i, w_i * w_nb_samples : (w_i + 1) * w_nb_samples] = np.fft.ifft(morlet_fft * np.fft.fft(w_signal))[w_nb_samples:]


signal = np.concatenate((signal, np.flip(signal, axis=0)))

t_morlet = np.linspace(- duration,  duration, 2 * nb_samples)
tf = np.empty(shape=(frequencies.shape[0], nb_samples), dtype=complex)
for f_i in range(frequencies.shape[0]) :
    omega = 2. * np.pi * frequencies[f_i]
    sigma = width / omega;
    morlet = 1. / np.sqrt(sigma * np.sqrt(np.pi)) * np.exp(1j * omega * t_morlet) * np.exp(- 0.5 * t_morlet * t_morlet / (sigma * sigma))
    tf[f_i] = np.fft.ifft(np.fft.fft(morlet) * np.fft.fft(signal))[nb_samples:]


plt.clf()
fig, ax = plt.subplots(1, 1)
ax.matshow(np.angle(tf) - np.angle(windowed_tf))
fig.show()

name = "analytic_wavelet_convolve_sinus_phase_windowed_diff_0.5second"
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((15.5, 12.5), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
