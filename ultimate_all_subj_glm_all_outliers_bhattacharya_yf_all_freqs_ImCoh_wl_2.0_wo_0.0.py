import numpy as np
import matplotlib.pyplot as plt
from scipy import stats, optimize, interpolate
import pathlib as pl
import ztrcore
import shutil
import pandas as pd

epi_locs = {"0c0993e0c6": "gauche", "6bafce6a74":"droite", "7f4499ef3e":"droite", "53fdbcb694":"droite", "74f0194d74":"droite", "514ed32074":"gauche", "7152c6d01c":"gauche", "b5045fd830":"gauche", "e224033c80":"gauche", "f8766225fd":"droite", "1de930ae4f":"droite", "4c2f676eb9":"droite", "8766fe2ebd":"droite", "9517667082":"droite", "c11b592b31":"droite", "dd46a3d321":"droite", "82a084bee6":"droite", "8450e19b55":"gauche", "221150004d":"gauche", "a5c7600a52":"gauche", "dc46f910ec":"gauche", "f560d1b20e":"gauche", "fe070958b9":"gauche"}
elec_locs = {"P3" : "gauche","C3" : "gauche","F3" : "gauche","O1" : "gauche","T5" : "gauche","T3" : "gauche","F7" : "gauche","Fp1" : "gauche","P4" : "droite","C4" : "droite","F4" : "droite","O2" : "droite","T6" : "droite","T4" : "droite","F8" : "droite","Fp2" : "droite"}

## To load dynamic zither plugins
ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def nb_epochs_per_subject(files) :
    epochs = {}
    for f in files :
        subject = list(f.parts)[-2]
        if subject in epochs :
            epochs[subject] += 1
        else :
            epochs[subject] = 1
    return epochs

def measure(columns, group, files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection) :

    df = pd.DataFrame(columns=columns)

    short_range_pairs_indices = np.nonzero(np.triu(short_range_neighbors))
    short_range_nb_pairs = len(short_range_neighbors[short_range_pairs_indices])
    long_range_pairs_indices = np.nonzero(np.triu(long_range_neighbors))
    long_range_nb_pairs = len(long_range_neighbors[long_range_pairs_indices])

    for f in files :
        epi_loc = None
        if group == "Epileptic" :
            if os.path.basename(os.path.dirname(f)) not in epi_locs :
                print("not in epi_locs")
                continue

            epi_loc = epi_locs[os.path.basename(os.path.dirname(f))]

            if epi_loc != 'gauche' and epi_loc != 'droite' :
                print("epi_loc not gauche or droite ", epi_loc)
                continue

        reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
        reader_edf_file.setFilePath(str(f))
        reader_edf_file.run()
        data          = reader_edf_file.output()
        sampling_rate = reader_edf_file.samplingRate()
        channels      = reader_edf_file.channels() # C x T (real)

        channels_selector = ztrcore.ztrProcessChannelsSelector()
        channels_selector.setChannels(channels)
        channels_selector.setSignal(data)
        channels_selector.setSelection(selection)
        channels_selector.run()
        selected_data   = channels_selector.output() # C x T (real)
        selected_channels = channels_selector.outputChannels()

        nb_channels = selected_data.shape[0]
        nb_values   = selected_data.shape[1]
        duration  = nb_values / sampling_rate

        # Filter the signal on several frequency bands (center_freq + bandwidth)
        filter_bank = ztrcore.ztrProcessFilterBankOffline()
        filter_bank.setFrequencies(frequencies);
        filter_bank.setBandwidths(bandwidths);
        filter_bank.setSamplingRate(sampling_rate);
        filter_bank.setSignal(selected_data);
        filter_bank.setOrder(6);
        filter_bank.run();
        filter_bank_output = filter_bank.output() # C x F x T (real)

        # Compute for each frequency band the analytic signal
        hilbert_bank = ztrcore.ztrProcessHilbertBank()
        hilbert_bank.setSamplingRate(sampling_rate);
        hilbert_bank.setSignal(filter_bank_output);
        hilbert_bank.run();
        hilbert_bank_output = hilbert_bank.output(); # C x F x T (complex)

        # Compute the synchrony measure on windows
        tfwt = ztrcore.ztrProcessTFWindowT(measure_name, hilbert_bank_output, sampling_rate, window_length, window_overlapping)
        tfwt.run()
        tfwt_output = tfwt.output() # C x C x F x T (real)

        nb_channels    = tfwt_output.shape[0];
        nb_frequencies = tfwt_output.shape[2];
        nb_windows     = tfwt_output.shape[3];

        #df = pd.DataFrame(columns=['Measure Name', 'Subject', 'Recording', 'Frequency', 'Pair', 'Pair Range', 'Side', 'Laterality', 'Value'])
        temp_df = pd.DataFrame(columns=columns)
        for i in range(nb_channels) :
            for j in range(i + 1, nb_channels) :
                for f_i in range(nb_frequencies) :
                    for w_i in range(nb_windows) :
                        if (elec_locs[selected_channels[i]] == "gauche" and elec_locs[selected_channels[j]] == "droite") or (elec_locs[selected_channels[i]] == "droite" and elec_locs[selected_channels[j]] == "gauche") :
                            if group == "Control" :
                                if short_range_neighbors[i, j] == 1 :
                                    temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Short", "Both", "NA", tfwt_output[i, j, f_i, w_i]]
                                if long_range_neighbors[i, j] == 1 :
                                    temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Long", "Both", "NA", tfwt_output[i, j, f_i, w_i]]
                            if group == "Epileptic" :
                                if short_range_neighbors[i, j] == 1 :
                                    temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Short", "Both", "Cross", tfwt_output[i, j, f_i, w_i]]
                                if long_range_neighbors[i, j] == 1 :
                                    temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Long", "Both", "Cross", tfwt_output[i, j, f_i, w_i]]

                        if elec_locs[selected_channels[i]] == "gauche" and elec_locs[selected_channels[j]] == "gauche" :
                            if group == "Control" :
                                if short_range_neighbors[i, j] == 1 :
                                    temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Short", "Left", "NA", tfwt_output[i, j, f_i, w_i]]
                                if long_range_neighbors[i, j] == 1 :
                                    temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Long", "Left", "NA", tfwt_output[i, j, f_i, w_i]]
                            if group == "Epileptic" :
                                if elec_locs[selected_channels[i]] == epi_loc:
                                    if short_range_neighbors[i, j] == 1 :
                                        temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Short", "Left", "Ipsi", tfwt_output[i, j, f_i, w_i]]
                                    if long_range_neighbors[i, j] == 1 :
                                        temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Long", "Left", "Ipsi", tfwt_output[i, j, f_i, w_i]]
                                else :
                                    if short_range_neighbors[i, j] == 1 :
                                        temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Short", "Left", "Contra", tfwt_output[i, j, f_i, w_i]]
                                    if long_range_neighbors[i, j] == 1 :
                                        temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Long", "Left", "Contra", tfwt_output[i, j, f_i, w_i]]

                        if elec_locs[selected_channels[i]] == "droite" and elec_locs[selected_channels[j]] == "droite" :
                            if group == "Control" :
                                if short_range_neighbors[i, j] == 1 :
                                    temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Short", "Right", "NA", tfwt_output[i, j, f_i, w_i]]
                                if long_range_neighbors[i, j] == 1 :
                                    temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Long", "Right", "NA", tfwt_output[i, j, f_i, w_i]]
                            if group == "Epileptic" :
                                if elec_locs[selected_channels[i]] == epi_loc:
                                    if short_range_neighbors[i, j] == 1 :
                                        temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Short", "Right", "Ipsi", tfwt_output[i, j, f_i, w_i]]
                                    if long_range_neighbors[i, j] == 1 :
                                        temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Long", "Right", "Ipsi", tfwt_output[i, j, f_i, w_i]]
                                else :
                                    if short_range_neighbors[i, j] == 1 :
                                        temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Short", "Right", "Contra", tfwt_output[i, j, f_i, w_i]]
                                    if long_range_neighbors[i, j] == 1 :
                                        temp_df.loc[len(temp_df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], selected_channels[j] + "-" + selected_channels[i], "Long", "Right", "Contra", tfwt_output[i, j, f_i, w_i]]
        df = pd.concat([df, temp_df], ignore_index=True)
        print("df.shape", df.shape)
    return df

# Parameters
frequencies = np.array([2.5, 5.5, 8.5, 11.5, 15.5, 24., 50.])
bandwidths = np.array([3., 3., 3., 3., 5., 12., 40.])

measure_name       = "ImCoh"
window_length      = 2.
window_overlapping = 0.

short_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/short_range.npy")
long_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/long_range.npy")

selection = ztrcore.readSelection("/home/clebreto/programming/come/zither/resources/selection/16battacharya.sel")

recordings = pl.Path("/home/clebreto/Data/Nice/")

condition = "yf"

condition_name = ""
if condition =="yo" :
    condition_name = "Eyes Open"
else :
    condition_name = "Eyes Closed"

A_files = np.array(list(recordings.glob("./Temoins/temoin_*/*.edf")))
B_files = np.array(list(recordings.glob("./Epi_ZE_Yf_ultimate/*/*.edf")))

# Ignore patients looking like outliers
#to_ignore = ["e224033c80", "7f4499ef3e", "e5cb01cc3a", "53fdbcb694", "f8766225fd", "8f91885dd8", "221150004d", "f6491b9dd4"]
to_ignore = []
B_files = np.array([b for b in B_files if (b.parts[-2]) not in to_ignore])

#A_files = np.random.choice(A_files, size=10, replace=False)
#B_files = np.random.choice(B_files, size=10, replace=False)

columns = ['Measure', 'Group', 'Subject', 'Recording', 'Frequency', 'Pair', 'Pair Range', 'Side', 'Laterality', 'Value']
df_control = measure(columns, "Control", A_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection)
df_epileptic = measure(columns, "Epileptic", B_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection)

df = pd.concat([df_control, df_epileptic], ignore_index=True)
name = "ultimate_all_subj_glm_all_outliers_bhattacharya_" + condition + "_all_freqs_" + measure_name + "_wl_" + str(window_length) + "_wo_" + str(window_overlapping)
shutil.copy("impact.py", name + ".py")
df.to_csv(name + ".csv")
