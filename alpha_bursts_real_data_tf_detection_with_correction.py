import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba
import matplotlib

import scipy as sc
import scipy.ndimage.filters as filters
import scipy.signal
from scipy.spatial import distance_matrix

import copy

from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def plotTFAmplitudes(fg, ax, time, frequencies, atf, events=None, event='', color='red', title='', xlabel='', ylabel='', norm=False) :
        time = np.asarray(time);
        if time.ndim != 1 :
            raise ValueError("time : must be a 1d array")
        frequencies = np.asarray(frequencies);
        if frequencies.ndim != 1 :
            raise ValueError("frequencies : must be a 1d array")

        sampling_rate = 1. / (time[1] - time[0])
        amplitudes = atf

        if norm is True :
            if events is not None:
                not_event_cc = np.ndarray(shape=(amplitudes.shape[0], 0))
                skip = False
                for e_i in range(len(events)) :
                    if events[e_i].uid != event :
                        if skip :
                            skip = False
                            continue
                        not_event_cc = np.concatenate((not_event_cc, amplitudes[:, int((events[e_i].timestamp / 1000 - time[0]) * sampling_rate) : int((events[e_i + 1].timestamp / 1000 - time[0]) * sampling_rate)]), axis=(1))
                        skip = True
                skip = False

                mean = np.expand_dims(np.mean(not_event_cc, axis=(1)), axis=(1))
                std = np.expand_dims(np.std(not_event_cc, axis=(1)), axis=(1))
                amplitudes = (amplitudes - mean) / std

        ratio = 1. / 10.

        x_decimation = 1
        y_decimation = 1

        if amplitudes.shape[0] < amplitudes.shape[1] :
          x_decimation = int(ratio * amplitudes.shape[1] / amplitudes.shape[0])

        amplitudes_to_show = amplitudes[::y_decimation, ::x_decimation]
        frequencies_to_show =  amplitudes_to_show.shape[0]
        time_to_show = amplitudes_to_show.shape[1]

        duration = time[-1] - time[0]

        ms = ax.imshow(amplitudes_to_show, origin='lower', cmap='binary')
        ax.set_xticks(np.linspace(0, time_to_show, 6))
        ax.set_xticklabels(["${0:.0f}$".format(x) for x in np.linspace(time[0], time[-1], 6)])
        ax.set_xlabel(xlabel, fontsize=40)
        ax.set_yticks(np.linspace(0, frequencies_to_show, 4))
        ax.set_yticklabels(["${0:.0f}$".format(x) for x in np.linspace(frequencies[0], frequencies[-1], 4)])
        ax.set_ylabel(ylabel, fontsize=40)
        ax.set_title(title)

        dvd = make_axes_locatable(ax)
        cax = dvd.append_axes('right', size='1%', pad=0.05)
        fg.colorbar(ms, cax=cax, orientation='vertical')

        handles = []
        if events is not None:
            skip = False
            for e_i in range(len(events)) :
                if events[e_i].uid in event :
                    if skip :
                        skip = False
                        continue
                    handles.append(ptc.Patch(edgecolor=color, facecolor=(0,0,0,0.0), fill=True, label=events[e_i].uid))
                    ax.add_patch(ptc.Rectangle( ((events[e_i].timestamp / 1000. - time[0]) * sampling_rate / x_decimation, 0), (events[e_i + 1].timestamp - events[e_i].timestamp) / 1000. * sampling_rate / x_decimation, amplitudes.shape[0], edgecolor = color, facecolor = (0,0,0,0.0), fill=True))
                    skip = True
            skip = False
        ax.legend(handles=handles)

def yoyf(signal, events_reader_output, event="") :
    events = []
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if events_reader_output[e_i].uid == event :
            events.append(signal[:, int(events_reader_output[e_i].timestamp / 1000. * sampling_rate):int(events_reader_output[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return events

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

## Event selection
paradigm = "alpha"
event = "alpha"
subject_id = "0001"

## Channel selection
channels_selection = np.array(['O1'])

sampling_rate = 512
max_lag_duration = 10.  #seconds
max_lag_size = int(max_lag_duration * sampling_rate)  #samples

files = sorted(recordings.glob("./*11*/*" + str(subject_id) + "_" + paradigm + ".edf"))
files = [f for f in files]

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(files[0]))
reader_edf_file.run()

signal        = reader_edf_file.output()[:32, :] # 33 is GSR
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/" + paradigm + ".evt")
events_reader.run()
events_reader_output = events_reader.output()

## Selects only certain electrodes, and filter the signal
indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
signal = signal[indices_selection]
channels = channels[indices_selection]
nb_channels = len(channels)

## Recovers the event as a list of matrices
signals = yoyf(signal, events_reader_output, event)
duration = 20.

## Computes the wavelet coeficients with the wavelet transform first
fmin = 8
fmax = 12
fstep = .01
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 5.

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signals[0])
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt_ampli = np.copy(np.abs(wavelet_fourier.output()))[0]
wt_phase = np.copy(np.angle(wavelet_fourier.output()))[0]

local_maxima = filters.maximum_filter(wt_ampli, (3,3), mode="nearest")==wt_ampli
ma_wt_ampli = np.ma.masked_where(local_maxima, wt_ampli)
ma_wt_phase = np.ma.masked_where(local_maxima, wt_phase)

locations = np.array(np.where(local_maxima==True))

## Reconstruct the signal
burst_duration = 0.5
bursts_frequencies = locations[0] * fstep + fmin
bursts_durations   = np.ones(shape=locations[0].shape[0]) * burst_duration
bursts_locations   = locations[1] / sampling_rate
bursts_amplitudes  = wt_ampli[locations[0], locations[1]]
bursts_phases      = wt_phase[locations[0], locations[1]]

print("Number of local maxima detected : ", locations.shape[1])

regenerated_signal = np.zeros(shape=(1, int(sampling_rate * duration)))
time = np.arange(0, duration, 1./sampling_rate)
for l, a, f, d, p  in zip(bursts_locations, bursts_amplitudes, bursts_frequencies, bursts_durations, bursts_phases) :
    s = d / (2. * np.sqrt(2*np.log(2)))
    G = np.exp(- ((time - l)**2) / (2. * s**2))
    regenerated_signal += a * G * np.cos(2 * np.pi * f * (time - l) + p)

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(regenerated_signal)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
regenerated_wt_ampli = np.copy(np.abs(wavelet_fourier.output()))[0]
regenerated_wt_phase = np.copy(np.angle(wavelet_fourier.output()))[0]

highpass = ztrcore.ztrProcessFilterHighPassOffline()
highpass.setSignal(signals[0]);
highpass.setFrequency(fmin);
highpass.setSamplingRate(sampling_rate);
highpass.setOrder(4);
highpass.run();
highpass_output = np.copy(highpass.output());

lowpass = ztrcore.ztrProcessFilterLowPassOffline()
lowpass.setSignal(highpass_output);
lowpass.setFrequency(fmax);
lowpass.setSamplingRate(sampling_rate);
lowpass.setOrder(4);
lowpass.run();
lowpass_output = np.copy(lowpass.output());

# Fix the amplitudes
distances = np.zeros(shape=(len(locations[1]), len(locations[1])))
for i, (bl_i, bf_i, bp_i) in enumerate(zip(bursts_locations, bursts_frequencies, bursts_phases)) :
    bl_i = bl_i - bp_i/(2.*np.pi*bf_i)
    for j, (bl_j, bf_j, bp_j)  in enumerate(zip(bursts_locations, bursts_frequencies, bursts_phases)) :
        bl_j = bl_j - bp_j/(2.*np.pi*bf_j)
        distances[i, j] = abs((bl_j - bl_i))

S = burst_duration / (2. * np.sqrt(2.*np.log(2.))) * np.ones(shape=distances.shape)
M = np.exp(-(distances*distances)/(2. * S * S))
bursts_amplitudes_corrected = np.linalg.solve(M, bursts_amplitudes)
print(np.sum(bursts_amplitudes_corrected - bursts_amplitudes))

regenerated_signal_corrected = np.zeros(shape=(1, int(sampling_rate * duration)))
time = np.arange(0, duration, 1./sampling_rate)
for l, a, f, d, p  in zip(bursts_locations, bursts_amplitudes_corrected, bursts_frequencies, bursts_durations, bursts_phases) :
    s = d / (2. * np.sqrt(2*np.log(2)))
    G = np.exp(- ((time - l)**2) / (2. * s**2))
    regenerated_signal_corrected += a * G * np.cos(2 * np.pi * f * (time - l) + p)

lowpass_output[0][:int(1*sampling_rate)]=0
lowpass_output[0][-int(1*sampling_rate):]=0
regenerated_signal[0][:int(1*sampling_rate)]=0
regenerated_signal[0][-int(1*sampling_rate):]=0
regenerated_signal_corrected[0][:int(3*sampling_rate)]=0
regenerated_signal_corrected[0][-int(3*sampling_rate):]=0

lp_normalized = (lowpass_output[0] - np.min(lowpass_output[0]))/(np.max(lowpass_output[0]) - np.min(lowpass_output[0]))
rg_normalized = (regenerated_signal[0] - np.min(regenerated_signal[0]))/(np.max(regenerated_signal[0]) - np.min(regenerated_signal[0]))
rg_normalized_corrected = (regenerated_signal_corrected[0] - np.min(regenerated_signal_corrected[0]))/(np.max(regenerated_signal_corrected[0]) - np.min(regenerated_signal_corrected[0]))
# rg_normalized_corrected = regenerated_signal_corrected[0]

plt.clf()
fg, ax = plt.subplots(10, 1)
ax[0].plot(np.linspace(0, duration, signals[0].shape[1]), lowpass_output[0], color='black')
ax[0].margins(x=0)
ax[1].imshow(ma_wt_ampli)
ax[2].imshow(ma_wt_phase)
ax[3].plot(np.linspace(0, duration, signals[0].shape[1]), regenerated_signal[0])
ax[3].margins(x=0)
ax[4].imshow(regenerated_wt_ampli)
ax[5].imshow(regenerated_wt_phase)
ax[6].plot(np.linspace(0, duration, signals[0].shape[1]), lp_normalized)
ax[6].plot(np.linspace(0, duration, signals[0].shape[1]), rg_normalized)
ax[6].margins(x=0)
ax[7].plot(np.linspace(0, duration, signals[0].shape[1]), lp_normalized - rg_normalized)
ax[7].margins(x=0)
ax[8].plot(np.linspace(0, duration, signals[0].shape[1]), lp_normalized)
ax[8].plot(np.linspace(0, duration, signals[0].shape[1]), rg_normalized_corrected)
ax[8].margins(x=0)
ax[9].plot(np.linspace(0, duration, signals[0].shape[1]), lp_normalized - rg_normalized_corrected)
ax[9].margins(x=0)
# ax[0].plot(np.linspace(0, signals[0].shape[1] / sampling_rate, signals[0].shape[1]), signals[0][0], color='black')
# ax[0].set_xlabel("$Time (s.)$")
# ax[0].set_ylabel(r'$Voltage (\mu V)$')
# ax[0].text(-0.075, 1.1, r'$\mathbf{a}$', transform=ax[0].transAxes, size=40)
# plotTFAmplitudes(fg, ax[1], np.linspace(0, signals[0].shape[1]/sampling_rate, signals[0].shape[1]), frequencies, wt[0], xlabel=r'$Time (s.)$', ylabel=r'$\nu$')
# ax[1].text(-0.075, 1.1, r'$\mathbf{b}$', transform=ax[1].transAxes, size=40)
# alpha = np.linspace(1, 0, max_lag_size)
# points = np.array([np.linspace(0, max_lag_duration, max_lag_size), autocorrelations[0][0]]).T.reshape(-1, 1, 2)
# segments = np.concatenate([points[:-1], points[1:]], axis=1)
# colors = [to_rgba('black', alpha[v_i]) for v_i in range(autocorrelations[0][0].shape[0])]
# line = LineCollection(segments, colors=colors)
# ax[2].add_collection(line)
# ax[2].set_xlim(0, max_lag_duration)
# ax[2].set_ylim(-1, 1)
# ax[2].set_xlabel("$Lag, \delta_k (s.)$")
# ax[2].set_ylabel(r'$\rho$')
# ax[2].text(-0.075, 1.1, r'$\mathbf{c}$', transform=ax[2].transAxes, size=40)
# plotTFAmplitudes(fg, ax[3], np.linspace(0, autocorrelations[0].shape[1]/sampling_rate, autocorrelations[0].shape[1]), frequencies, autocorr_wt[0], xlabel=r'$Lag, \delta_k (s.)$', ylabel=r'$\nu$')
# ax[3].text(-0.075, 1.1, r'$\mathbf{d}$', transform=ax[3].transAxes, size=40)

name = paradigm + "_bursts_real_data_tf_detection_with_correction"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((60, 80), forward=False)
fg.savefig(name + ".png", dpi=50, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
