import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def yoyf(signal, events_reader_output, event="") :
    events = []
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if events_reader_output[e_i].uid == event :
            events.append(signal[:, int(events_reader_output[e_i].timestamp / 1000. * sampling_rate):int(events_reader_output[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return events

def autocorr(x, max_lag_size) :
    result = np.zeros(max_lag_size)
    x -= np.mean(x)
    result[0] = 1
    for k in range(1, max_lag_size) :
        result[k] = np.sum(x[:-k]*np.conj(x[k:])) / np.sqrt((np.sum(x[:-k]*x[:-k])*np.sum(x[k:]*x[k:])))
    return result

# def autocorr(x) :
#     result = np.correlate(x, x, mode='full')
#     return result[result.size//2:]

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

## Event selection
event = "alpha"

## Channel selection
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])
channels_colors = np.array(['red', 'blue', 'green', 'orange', 'grey'])
nb_channels = channels_selection.shape[0] #If all channels are available in the recordings

max_lag_duration = 10.  #seconds
max_lag_size = int(max_lag_duration * sampling_rate)  #samples

autocorrelations = []
files = recordings.glob("./2020_11_*/*01_alpha.edf")
for f in files :
    print(str(f))
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32, :] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Reads the event as they were presented during the recording
    events_reader = ztrcore.ztrProcessEventsReader()
    events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
    events_reader.run()
    events_reader_output = events_reader.output()

    ## Selects only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    signal = signal[indices_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    ## Recovers the event as a list of matrices
    signals = yoyf(signal, events_reader_output, event)
    for s in signals :
        autocorrelation = np.empty(shape=(nb_channels, max_lag_size))
        for c_i, c in enumerate(s) :
            # c = c-np.mean(c)
            # ax[c_i].plot(autocorr(c)/np.sum(c * c), color = channels_colors[c_i], linewidth=0.1)
            autocorrelation[c_i] = autocorr(c, max_lag_size)
        autocorrelations.append(autocorrelation)

plt.clf()
fg, ax = plt.subplots(nb_channels, 1)
for c_i in range(nb_channels):
    ax[c_i].set_title(channels_selection[c_i])
for s in autocorrelations:
    for c_i in range(nb_channels):
        ax[c_i].plot(s[c_i], color = channels_colors[c_i], linewidth=0.1)

fg.suptitle("Autocorrelation, " + event)

name = "autocorrelation_" + event + "_channels_other_correlation"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 15.5), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})

fg, ax = plt.subplots(1, 1)
ztrplotting.plotTFAmplitudes(ax, np.linspace(0, max_lag_duration, int(max_lag_duration*sampling_rate)), np.arange(1., 33, 1.), np.mean(autocorrelations[2], axis=0))
fg.show()
