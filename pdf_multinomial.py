import matplotlib.pyplot as plt
import sys, os, shutil
import numpy as np

fg = plt.figure()
ax = plt.axes(projection ='3d')

n = 90
pa = 0.05
pb = 0.05

x = []
y = []
z = []
for k in range(n):
    for l in range(n-k):
       x.append(k)
       y.append(l)
       p = np.math.factorial(n)/(np.math.factorial(k)*np.math.factorial(l)*np.math.factorial(n-l-k))*pa**k*pb**l*(1-pa-pb)**(n-k-l)
       z.append(p)

# #P(K<ko, L<lo)
# k0 = 15
# l0 = 45
# P_inf = 0
# for k in range(k0):
#     for l in range(l0):
#         P_inf += np.math.factorial(n)/(np.math.factorial(k)*np.math.factorial(l)*np.math.factorial(n-l-k))*pa**k*pb**l*(1-pa-pb)**(n-k-l)

# #P(K>ko, L>lo)
# k0 = 15
# l0 = 45
# P_sup = 0
# for k in range(k0, n-l0):
#     for l in range(l0, n-k):
#         P_sup += np.math.factorial(n)/(np.math.factorial(k)*np.math.factorial(l)*np.math.factorial(n-l-k))*pa**k*pb**l*(1-pa-pb)**(n-k-l)

# #P(K<ko, L>lo)
# k0 = 15
# l0 = 45
# P_inf_sup = 0
# for k in range(0, k0):
#     for l in range(l0, n-k):
#         P_inf_sup += np.math.factorial(n)/(np.math.factorial(k)*np.math.factorial(l)*np.math.factorial(n-l-k))*pa**k*pb**l*(1-pa-pb)**(n-k-l)

# #P(K>ko, L<lo)
# k0 = 15
# l0 = 45
# P_sup_inf = 0
# for l in range(l0, n):
#     for k in range(k0, n-l):
#         P_sup_inf += np.math.factorial(n)/(np.math.factorial(k)*np.math.factorial(l)*np.math.factorial(n-l-k))*pa**k*pb**l*(1-pa-pb)**(n-k-l)

sort = sorted(zip(x, y, z), key=lambda x: x[2])
i = 0
r = 0
while r < 0.05 :
    r += sort[i][2]
    i+=1

xblue = [x[0] for x in sort[:i]]
xred = [x[0] for x in sort[i:]]
yblue = [x[1] for x in sort[:i]]
yred = [x[1] for x in sort[i:]]
zblue = [x[2] for x in sort[:i]]
zred = [x[2] for x in sort[i:]]
ax.scatter(xblue, yblue, zblue, c = 'blue')
ax.scatter(xred, yred, zred, c = 'red')
plt.show()

name = "pdf_multinomial"
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
