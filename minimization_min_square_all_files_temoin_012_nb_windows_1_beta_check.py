import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")

recordings = pl.Path("/user/clebreto/home/Data/Nice/")

subject_id = "temoin_012"

nb_windows = 1

fmin = 2.
fmax = 35.
fstep = 0.5
frequencies = np.arange(fmin, fmax, fstep)

wavelet_width = 12.

files = np.array(list(recordings.glob("./Temoins/" + subject_id + "/*.edf")))

# Crop at begin and end of the signal
crop = 3

nb_values = []
sampling_rates = []
nb_channels = []
minima = []
maxima = []
for f in files :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal = reader_edf_file.output()[:21] # above 21 are bio signals
    nb_channels.append(signal.shape[0])
    sampling_rates.append(reader_edf_file.samplingRate())
    nb_values.append(signal.shape[1])

sampling_rate = sampling_rates[0]
nb_channels = nb_channels[0]
min_nb_values = np.min(np.array(nb_values))

for f in files :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal = reader_edf_file.output()[:21] # above 21 are bio signals

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(3.)
    bp.setFrequency(25.)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()

    signal_filtered = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
    minima.append(np.min(signal_filtered))
    maxima.append(np.max(signal_filtered))

minimum = np.min(minima)
maximum = np.max(maxima)

# Weights
ev = np.zeros(shape=(0, nb_windows, nb_channels)) # nb_files, nb_windows, nb_channels
w = np.zeros(shape=(0, nb_windows, nb_channels)) # nb_files, nb_windows, nb_channels
# Residuals
residual_ev = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
residual_w = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
residual_mean = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
# Time Frequency analysis
mean_tf_ev = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_w = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_mean = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_ev_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_w_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_mean_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
# Distribution analysis
bars_old_reference = []
bars_ev = []
bars_w = []
bins = np.histogram([], range=(minimum, maximum), bins=100)[1]

for c_i in range(nb_channels) :
    bars_old_reference.append(np.histogram([], range=(minimum, maximum), bins=100)[0])
    bars_ev.append(np.histogram([], range=(minimum, maximum), bins=100)[0])
    bars_w.append(np.histogram([], range=(minimum, maximum), bins=100)[0])

for f in tqdm(files) :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal        = reader_edf_file.output()[:21, :min_nb_values]
    channels      = np.array(reader_edf_file.channels()[:21])

    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(3.)
    bp.setFrequency(25.)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()
    signal_filtered = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]

    ev_file = np.zeros(shape=(nb_windows, nb_channels))
    w_file = np.zeros(shape=(nb_windows, nb_channels))
    residual_ev_file = np.zeros(shape=(nb_windows))
    residual_w_file = np.zeros(shape=(nb_windows))
    residual_mean_file = np.zeros(shape=(nb_windows))
    for w_i in range(nb_windows) :
        start = int(w_i * signal_filtered.shape[1] / nb_windows)
        end   = int((w_i + 1) * signal_filtered.shape[1] / nb_windows)
        M = signal_filtered[:, start : end]
        n = M.shape[0]
        H = M.dot(M.T)

        if np.linalg.matrix_rank(H) == signal.shape[0]: # Ensures matrix is full rank
            ##### sum{w_i^2}=1 #####
            (e_val, e_vec) = np.linalg.eig(H)
            s_e_vec = np.real(e_vec[:, np.argmin(e_val)]) # Select the vector associated to the smaller eigen value

            #s_e_vec_norm = s_e_vec / np.sum(s_e_vec)
            ref_ev = np.dot(s_e_vec, signal_filtered[:, start : end])
            ref_ev_unfiltered = np.dot(s_e_vec, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)][:, start : end])

            ev_file[w_i] = s_e_vec

            residual_ev_file[w_i] = np.dot(ref_ev, ref_ev) / ref_ev.shape[0]

            wf_ev = ztrcore.ztrProcessWaveletFourier()
            wf_ev.setFrequencies(frequencies)
            wf_ev.setSignal(np.expand_dims(ref_ev, axis=0))
            wf_ev.setWidth(wavelet_width)
            wf_ev.setSamplingRate(sampling_rate)
            wf_ev.run()
            wf_ev_o = np.squeeze(wf_ev.output(), axis=0)
            mean_tf_ev[:, start : end] += wf_ev_o

            wf_ev.setSignal(np.expand_dims(ref_ev_unfiltered, axis=0))
            wf_ev.run()
            wf_ev_o_unfiltered = np.squeeze(wf_ev.output(), axis=0)
            mean_tf_ev_unfiltered[:, start : end] += wf_ev_o_unfiltered

            ##### sum{w_i}=1 #####
            K = H[0:-1,0:-1]
            h = H[-1,0:-1]
            a = H[-1,-1]
            o = np.ones((n-1))

            Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
            B = a*o-h

            x = np.linalg.solve(Q,B)
            x = np.append(x, 1 - np.dot(x,o))
            ref_w = np.dot(x, signal_filtered[:, start : end])
            ref_w_unfiltered = np.dot(x, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)][:, start : end])

            w_file[w_i] = x
            residual_w_file[w_i] = np.dot(ref_w, ref_w) / ref_w.shape[0]

            wf_w = ztrcore.ztrProcessWaveletFourier()
            wf_w.setFrequencies(frequencies)
            wf_w.setSignal(np.expand_dims(ref_w, axis=0))
            wf_w.setWidth(wavelet_width)
            wf_w.setSamplingRate(sampling_rate)
            wf_w.run()
            wf_w_o = np.squeeze(wf_w.output(), axis=0)
            mean_tf_w[:, start : end] += wf_w_o

            wf_w.setSignal(np.expand_dims(ref_w_unfiltered, axis=0))
            wf_w.run()
            wf_w_o_unfiltered = np.squeeze(wf_w.output(), axis=0)
            mean_tf_w_unfiltered[:, start : end] += wf_w_o_unfiltered

            ##### mean reference #####
            mean = np.ones(signal_filtered.shape[0]) / signal_filtered.shape[0]
            ref_mean = np.dot(mean, signal_filtered[:, start : end])
            ref_mean_unfiltered = np.dot(mean, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)][:, start : end])

            residual_mean_file[w_i] = np.dot(ref_mean, ref_mean) / ref_mean.shape[0]

            wf_mean = ztrcore.ztrProcessWaveletFourier()
            wf_mean.setFrequencies(frequencies)
            wf_mean.setSignal(np.expand_dims(ref_mean, axis=0))
            wf_mean.setWidth(wavelet_width)
            wf_mean.setSamplingRate(sampling_rate)
            wf_mean.run()
            wf_mean_o = np.squeeze(wf_mean.output(), axis=0)
            mean_tf_mean[:, start : end] += wf_mean_o

            wf_mean.setSignal(np.expand_dims(ref_mean_unfiltered, axis=0))
            wf_mean.run()
            wf_mean_o_unfiltered = np.squeeze(wf_mean.output(), axis=0)
            mean_tf_mean_unfiltered[:, start : end] += wf_mean_o_unfiltered

            for c_i in range(signal_filtered.shape[0]):
                bars_old_reference[c_i] += np.histogram(signal_filtered[c_i, start : end], range=(minimum, maximum), bins=100)[0]
                bars_ev[c_i] += np.histogram(signal_filtered[c_i, start : end] - ref_ev, range=(minimum, maximum), bins=100)[0]
                bars_w[c_i] += np.histogram(signal_filtered[c_i, start : end] - ref_w, range=(minimum, maximum), bins=100)[0]

        else:
            print("Matrix is not full rank")

    ev = np.concatenate((ev, np.expand_dims(ev_file, axis=0)), axis=0)
    w = np.concatenate((w, np.expand_dims(w_file, axis=0)), axis=0)
    residual_ev = np.concatenate((residual_ev, np.expand_dims(residual_ev_file, axis=0)), axis=0)
    residual_w = np.concatenate((residual_w, np.expand_dims(residual_w_file, axis=0)), axis=0)
    residual_mean = np.concatenate((residual_mean, np.expand_dims(residual_mean_file, axis=0)), axis=0)

# Mean over recordings
mean_tf_ev /= residual_mean.shape[0]
mean_tf_w /= residual_mean.shape[0]
mean_tf_mean /= residual_mean.shape[0]

mean_tf_ev_unfiltered /= residual_mean.shape[0]
mean_tf_w_unfiltered /= residual_mean.shape[0]
mean_tf_mean_unfiltered /= residual_mean.shape[0]

vmin = min(np.min(np.abs(mean_tf_ev)), np.min(np.abs(mean_tf_w)), np.min(np.abs(mean_tf_mean)))
vmax = max(np.max(np.abs(mean_tf_ev)), np.max(np.abs(mean_tf_w)), np.max(np.abs(mean_tf_mean)))
vmin_unfiltered = min(np.min(np.abs(mean_tf_ev_unfiltered)), np.min(np.abs(mean_tf_w_unfiltered)), np.min(np.abs(mean_tf_mean_unfiltered)))
vmax_unfiltered = max(np.max(np.abs(mean_tf_ev_unfiltered)), np.max(np.abs(mean_tf_w_unfiltered)), np.max(np.abs(mean_tf_mean_unfiltered)))

bar_min = min(np.min(bars_ev), np.min(bars_w), np.min(bars_old_reference)) # Most likely 0...
bar_max = max(np.max(bars_ev), np.max(bars_w), np.max(bars_old_reference))

plt.clf()
fg, ax = plt.subplots(9, 3)
positions = np.arange(0, ev.shape[0], 1)
ev_f = [ef for ef in np.reshape(ev, (ev.shape[0], ev.shape[1] * ev.shape[2]))]
ev_c = [ec for ec in np.reshape(ev.T, (ev.shape[2], ev.shape[1] * ev.shape[0]))]
w_f = [wf for wf in np.reshape(w, (w.shape[0], w.shape[1] * w.shape[2]))]
w_c = [wc for wc in np.reshape(w.T, (w.shape[2], w.shape[1] * w.shape[0]))]
ax[0, 0].boxplot(w_f, showfliers=False, showmeans=True)
ax[0, 0].set_title("sum{w_i}=1")
ax[1, 0].boxplot(w_c, showfliers=False, showmeans=True, labels=channels)
ax[1, 0].set_title("sum{w_i}=1")
ax[0, 1].boxplot(ev_f, showfliers=False, showmeans=True)
ax[0, 1].set_title("sum{w_i^2}=1")
ax[1, 1].boxplot(ev_c, labels=channels)
ax[1, 1].set_title("sum{w_i^2}=1")
for b_i, b in enumerate(bars_w) :
    ax[2, 0].plot(bins[:-1], b, label=channels[b_i])
ax[2, 0].set_ylim([bar_min, bar_max])
ax[2, 0].legend(ncol=3)
ax[2, 0].set_title("sum{w_i}=1")
for b_i, b in enumerate(bars_ev) :
    ax[2, 1].plot(bins[:-1], b, label=channels[b_i])
ax[2, 1].set_ylim([bar_min, bar_max])
ax[2, 1].legend(ncol=3)
ax[2, 1].set_title("sum{w_i^2}=1")
for b_i, b in enumerate(bars_old_reference) :
    ax[2, 2].plot(bins[:-1], b, label=channels[b_i])
ax[2, 2].set_ylim([bar_min, bar_max])
ax[2, 2].legend(ncol=3)
ax[2, 2].set_title("Filtered signal, old reference (CPz)")
# ztrplotting.plotTopography(ax[3, 0], np.mean(w, axis=(0, 1)), headset)
# ztrplotting.plotTopography(ax[3, 1], np.mean(ev, axis=(0, 1)), headset)
ax[4, 1].scatter(positions, np.mean(residual_ev, axis=1), label="sum{w_i^2}=1")
ax[4, 1].scatter(positions, np.mean(residual_w, axis=1), label="sum{w_i}=1")
ax[4, 1].scatter(positions, np.mean(residual_mean, axis=1), label="sum{w_i}=1, w_i = 1/n")
ax[4, 1].set_yscale('log')
ax[4, 1].legend()
ax[4, 1].set_title("Residuals")
ztrplotting.plotTFAmplitudes(ax[5, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_w, ratio = 2. / 10., title="sum{w_i}=1 TF mean over channels and experiments\n Alpha band filtering")
ztrplotting.plotTFAmplitudes(ax[5, 1], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_ev, ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over channels and experiments\n Alpha band filtering")
ztrplotting.plotTFAmplitudes(ax[5, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_mean, ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over channels and experiments\n Alpha band filtering")
ztrplotting.plotTFAmplitudes(ax[6, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_w, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i}=1 TF mean over channels and experiments\n Alpha band filtering")
ztrplotting.plotTFAmplitudes(ax[6, 1], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_ev, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over channels and experiments\n Alpha band filtering")
ztrplotting.plotTFAmplitudes(ax[6, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_mean, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over channels and experiments\n Alpha band filtering")

ztrplotting.plotTFAmplitudes(ax[7, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_w_unfiltered, ratio = 2. / 10., title="sum{w_i}=1 TF mean over channels and experiments\n No filtering")
ztrplotting.plotTFAmplitudes(ax[7, 1], np.linspace(crop, duration - crop , int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_ev_unfiltered, ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over channels and experiments\n No filtering")
ztrplotting.plotTFAmplitudes(ax[7, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_mean_unfiltered, ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over channels and experiments\n No filtering")
ztrplotting.plotTFAmplitudes(ax[8, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_w_unfiltered, vmin=vmin_unfiltered, vmax=vmax_unfiltered, ratio = 2. / 10., title="sum{w_i}=1 TF mean over channels and experiments\n No filtering")
ztrplotting.plotTFAmplitudes(ax[8, 1], np.linspace(crop, duration - crop , int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_ev_unfiltered, vmin=vmin_unfiltered, vmax=vmax_unfiltered, ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over channels and experiments\n No filtering")
ztrplotting.plotTFAmplitudes(ax[8, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_mean_unfiltered, vmin=vmin_unfiltered, vmax=vmax_unfiltered, ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over channels and experiments\n No filtering")

name = "minimization_min_square_all_files_" + subject_id + "_nb_windows_" + str(nb_windows) + "_beta_check"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((35, 50), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
