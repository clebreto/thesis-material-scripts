import numpy as np
import matplotlib.pyplot as plt
import ztrcore
import sys, os, shutil

sampling_rate = 512
t = np.arange(0., 1., 1. / sampling_rate)
f = 10.
ref = np.sin(10. * 2. * np.pi * t + 1. * np.pi) + np.sin(15. * 2. * np.pi * t + 1. * np.pi) + np.sin(20. * 2. * np.pi * t + 1. * np.pi)
sin1 = np.sin(f * 2. * np.pi * t)
sin2 = np.sin(f * 2. * np.pi * t + 0.5 * np.pi)
sin3 = np.sin(f * 2. * np.pi * t + 1.5 * np.pi)
signal = np.array((sin1 - ref, sin2 - ref, sin3 - ref))
signal = signal - np.mean(signal, axis=0)

fmin = 9.
fmax = 11.
fstep = 0.1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signal)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(wavelet_fourier.output())

wt_diff = np.array((np.angle(wt[0][10]) - np.angle(wt[1][10]), np.angle(wt[0][10]) - np.angle(wt[2][10])))

wt_diff_corrected = np.where(wt_diff > np.pi, wt_diff - 2 * np.pi, wt_diff)
wt_diff_corrected = np.where(wt_diff_corrected < -np.pi, wt_diff_corrected + 2 * np.pi, wt_diff_corrected)

plt.clf()

fg, ax = plt.subplots(1, 4)

ax[0].plot(t, sin1, color='blue')
ax[0].plot(t, sin2, color='red', linestyle='dashed', label="+ pi / 2.")
ax[0].plot(t, sin3, color='red', linestyle='dotted', label="+ 3. pi / 2.")
ax[0].legend()

ax[1].plot(t, np.angle(wt[0][10]), color='blue')
ax[1].plot(t, np.angle(wt[1][10]), color='red', linestyle='dashed', label="+ pi / 2.")
ax[1].plot(t, np.angle(wt[2][10]), color='red', linestyle='dotted', label="+ 3. pi / 2.")
ax[1].legend()

ax[2].plot(t, wt_diff[0], color='red', linestyle='dashed' , label="+ pi / 2.")
ax[2].plot(t, wt_diff[1], color='red', linestyle='dotted', label="+ 3. pi / 2.")
ax[2].legend()

ax[3].plot(t, wt_diff_corrected[0], color='red', linestyle='dashed', label="+ pi / 2.")
ax[3].plot(t, wt_diff_corrected[1], color='red', linestyle='dotted', label="+ 3. pi / 2.")
ax[3].legend()

name = "two_sinus_dephased_mean_ref_sumHz_" + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
fg.savefig(name + ".png", dpi=400, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
