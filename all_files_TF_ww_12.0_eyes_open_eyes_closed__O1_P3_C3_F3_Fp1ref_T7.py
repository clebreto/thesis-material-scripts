import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def plotTFAmplitudes(fg, ax, time, frequencies, atf, events=None, event='', color='red', title='', norm=False) :

        time = np.asarray(time);
        if time.ndim != 1 :
            raise ValueError("time : must be a 1d array")
        frequencies = np.asarray(frequencies);
        if frequencies.ndim != 1 :
            raise ValueError("frequencies : must be a 1d array")

        sampling_rate = 1. / (time[1] - time[0])
        amplitudes = atf

        if norm is True :
            if events is not None:
                not_event_cc = np.ndarray(shape=(amplitudes.shape[0], 0))
                skip = False
                for e_i in range(len(events)) :
                    if events[e_i].uid != event :
                        if skip :
                            skip = False
                            continue
                        not_event_cc = np.concatenate((not_event_cc, amplitudes[:, int((events[e_i].timestamp / 1000 - time[0]) * sampling_rate) : int((events[e_i + 1].timestamp / 1000 - time[0]) * sampling_rate)]), axis=(1))
                        skip = True
                skip = False

                mean = np.expand_dims(np.mean(not_event_cc, axis=(1)), axis=(1))
                std = np.expand_dims(np.std(not_event_cc, axis=(1)), axis=(1))
                amplitudes = (amplitudes - mean) / std

        ratio = 1. / 10.

        x_decimation = 1
        y_decimation = 1

        if amplitudes.shape[0] < amplitudes.shape[1] :
          x_decimation = int(ratio * amplitudes.shape[1] / amplitudes.shape[0])

        amplitudes_to_show = amplitudes[::y_decimation, ::x_decimation]
        frequencies_to_show =  amplitudes_to_show.shape[0]
        time_to_show = amplitudes_to_show.shape[1]

        duration = time[-1] - time[0]

        ms = ax.matshow(amplitudes_to_show, origin='lower')
        ax.set_xticks(np.linspace(0, time_to_show, 20))
        ax.set_xticklabels(["{0:.1f}".format(x) for x in np.linspace(time[0], time[-1], 20)])
        ax.set_yticks(np.linspace(0, frequencies_to_show, 10))
        ax.set_yticklabels(["{0:.1f}".format(x) for x in np.linspace(frequencies[0], frequencies[-1], 10)])
        ax.set_title(title)

        dvd = make_axes_locatable(ax)
        cax = dvd.append_axes('right', size='5%', pad=0.05)
        fg.colorbar(ms, cax=cax)

        handles = []
        if events is not None:
            skip = False
            for e_i in range(len(events)) :
                if events[e_i].uid in event :
                    if skip :
                        skip = False
                        continue
                    handles.append(ptc.Patch(edgecolor=color, facecolor=(0,0,0,0.0), fill=True, label=events[e_i].uid))
                    ax.add_patch(ptc.Rectangle( ((events[e_i].timestamp / 1000. - time[0]) * sampling_rate / x_decimation, 0), (events[e_i + 1].timestamp - events[e_i].timestamp) / 1000. * sampling_rate / x_decimation, amplitudes.shape[0], edgecolor = color, facecolor = (0,0,0,0.0), fill=True))
                    skip = True
            skip = False
        ax.legend(handles=handles)

def autocorr(s, max_lag_size) :
    result = np.zeros(shape=(s.shape[0], max_lag_size))
    s -= np.expand_dims(np.mean(s, axis=(1)), axis=(1))
    result[:, 0] = np.ones(shape=(s.shape[0]))
    for k in range(1, max_lag_size) :
        result[:, k] = np.sum(s[:, k:]*np.conj(s[:, :-k]), axis=(1))/(np.sqrt(np.sum(s[:, :-k]*s[:, :-k], axis=(1))*np.sum(s[:, k:]*s[:, k:], axis=(1))))
    return result

def autovar(s, max_lag_size) :
    result = np.zeros(shape=(s.shape[0], max_lag_size))
    s -= np.expand_dims(np.mean(s, axis=(1)), axis=(1))
    result[:, 0] = np.ones(shape=(s.shape[0]))
    for k in range(1, max_lag_size) :
        result[:, k] = np.var(s[:, k:]*np.conj(s[:, :-k]), axis=(1))
    return result

def yoyf(signal, events_reader_output, event="") :
    events = []
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if events_reader_output[e_i].uid == event :
            events.append(signal[:, int(events_reader_output[e_i].timestamp / 1000. * sampling_rate):int(events_reader_output[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return events

## Channel selection
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])

## Wavelet Transform
fmin = 7.
fmax = 17.
fstep = 0.1

wavelet_width = 12.

subject_id = "0001"
reference_electrode = "T7"
window_length = 5. # seconds ## Delay window analysis

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events_reader_output = events_reader.output()

max_lag_duration = 5.
recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")
files = sorted(recordings.glob("./*/*" + subject_id + "_alpha.edf"))
files = [f for f in files]

nb_values = []
sampling_rates = []
nb_channels = []
for f in files :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32] # 33 is GSR
    nb_channels.append(signal.shape[0])
    sampling_rates.append(reader_edf_file.samplingRate())
    nb_values.append(signal.shape[1])

sampling_rate = sampling_rates[0]
nb_channels = nb_channels[0]
min_nb_values = np.min(np.array(nb_values))
max_lag_size = int(max_lag_duration * sampling_rate)

frequencies = np.arange(fmin, fmax, fstep)

events = ["baseline", "alpha"]

wt = {}
wt["baseline"] = np.zeros(shape=(5, frequencies.shape[0], 10240))
wt["alpha"] = np.zeros(shape=(5, frequencies.shape[0], 10240))

for f in tqdm(files) :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32, :min_nb_values] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    ## Select only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    signal = signal[indices_selection] - signal[[np.where(channels == c)[0][0] for c in [reference_electrode]]]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    for e in events :
        signals = yoyf(signal, events_reader_output, e)
        temp = np.zeros(shape=(nb_channels, frequencies.shape[0], signals[0].shape[1]))
        for s in signals :
            wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
            wavelet_fourier.setFrequencies(frequencies)
            wavelet_fourier.setSignal(s)
            wavelet_fourier.setWidth(wavelet_width)
            wavelet_fourier.setSamplingRate(sampling_rate)
            wavelet_fourier.run()
            temp += np.copy(np.abs(wavelet_fourier.output())) / len(signals)

        wt[e] += temp

wt["baseline"] /= len(files)
wt["alpha"] /= len(files)

wt_baseline_mean_channels = np.mean(wt["baseline"], axis=(0))
wt_alpha_mean_channels = np.mean(wt["alpha"], axis=(0))

wt_baseline_std_channels = np.std(wt["baseline"], axis=(0))
wt_alpha_std_channels = np.std(wt["alpha"], axis=(0))

plt.clf()
fg, ax = plt.subplots(channels.shape[0], 2)
for c_i,c in enumerate(channels) :
    plotTFAmplitudes(fg, ax[c_i, 0], np.linspace(0, max_lag_size/sampling_rate, min_nb_values), frequencies, wt["baseline"][c_i], title="Eyes Open " + c)
    plotTFAmplitudes(fg, ax[c_i, 1], np.linspace(0, max_lag_size/sampling_rate, min_nb_values), frequencies, wt["alpha"][c_i], title="Eyes Closed " + c)
fg.suptitle("Morlet Wavelet Transform over " + str(len(files)) +" recordings Eyes Open / Eyes Closed\n Reference : " + reference_electrode)
# Mean on channels of
print(channels)
name = "all_files_TF_ww_" + str(wavelet_width) + "_eyes_open_eyes_closed_"
for csi in channels_selection :
    name += "_" + csi
name += "ref_" + reference_electrode
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((30, 55), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
