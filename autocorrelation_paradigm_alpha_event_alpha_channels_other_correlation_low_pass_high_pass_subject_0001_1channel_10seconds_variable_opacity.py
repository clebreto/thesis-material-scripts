import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba

import scipy as sc
from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'axes.labelsize': 'x-large',
    'axes.titlesize':'x-large',
    'xtick.labelsize':40,
    'ytick.labelsize':40
})

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def yoyf(signal, events_reader_output, event="") :
    events = []
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if events_reader_output[e_i].uid == event :
            events.append(signal[:, int(events_reader_output[e_i].timestamp / 1000. * sampling_rate):int(events_reader_output[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return events

def autocorr(s, max_lag_size, nb_samples=False) :
    result = np.zeros(shape=(s.shape[0], max_lag_size))
    s -= np.expand_dims(np.mean(s, axis=(1)), axis=(1))
    result[:, 0] = np.ones(shape=(s.shape[0]))
    for k in range(1, max_lag_size) :
        result[:, k] = np.sum(s[:, k:]*np.conj(s[:, :-k]), axis=(1))/(np.sqrt(np.sum(s[:, :-k]*s[:, :-k], axis=(1))*np.sum(s[:, k:]*s[:, k:], axis=(1))))
    if nb_samples == False :
            return result
    else :
        result_nb_samples = np.zeros(shape=(max_lag_size))
        result_nb_samples[0] = s.shape[1]
        for k in range(1, max_lag_size) :
            result_nb_samples[k] = s[:, k:].shape[1]
        return (result, result_nb_samples)

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

## Event selection
paradigm = "alpha"
event = "alpha"
subject_id = "0001"

## Channel selection
channels_selection = np.array(['O1'])
channels_colors = np.array(['red', 'blue', 'green', 'orange', 'grey'])
nb_channels = channels_selection.shape[0] #If all channels are available in the recordings

sampling_rate = 512
max_lag_duration = 10.  #seconds
max_lag_size = int(max_lag_duration * sampling_rate)  #samples

autocorrelations = []
autocorrelations_nb_samples = []
files = sorted(recordings.glob("./*11*/*" + str(subject_id) + "_" + paradigm + ".edf"))
files = [f for f in files]
for f in tqdm(files) :
    print(f)
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal        = reader_edf_file.output()[:32, :] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Reads the event as they were presented during the recording
    events_reader = ztrcore.ztrProcessEventsReader()
    events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/" + paradigm + ".evt")
    events_reader.run()
    events_reader_output = events_reader.output()

    ## Selects only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    signal = signal[indices_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    highpass = ztrcore.ztrProcessFilterHighPassOffline()
    highpass.setSignal(signal);
    highpass.setFrequency(1.);
    highpass.setSamplingRate(sampling_rate);
    highpass.setOrder(4);
    highpass.run();
    highpass_output = np.copy(highpass.output());

    lowpass = ztrcore.ztrProcessFilterLowPassOffline()
    lowpass.setSignal(highpass_output);
    lowpass.setFrequency(30.);
    lowpass.setSamplingRate(sampling_rate);
    lowpass.setOrder(4);
    lowpass.run();
    lowpass_output = np.copy(lowpass.output());

    ## Recovers the event as a list of matrices
    signals = yoyf(lowpass_output, events_reader_output, event)
    for s in signals :
        autocorrelation = autocorr(s, max_lag_size, nb_samples=True)
        autocorrelations.append(autocorrelation[0])
        autocorrelations_nb_samples.append(autocorrelation[1])

autocorrelations_nb_samples = np.array(autocorrelations_nb_samples)

plt.clf()
time = np.linspace(0, max_lag_size / sampling_rate, max_lag_size)
alpha = (autocorrelations_nb_samples - np.min(autocorrelations_nb_samples)) / (np.max(autocorrelations_nb_samples) - np.min(autocorrelations_nb_samples))
fg, ax = plt.subplots(nb_channels, 1)

for s in autocorrelations:
    for c_i in range(nb_channels):
        points = np.array([time, s[c_i]]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        colors = [to_rgba('black', alpha[c_i][v_i]) for v_i in range(s[c_i].shape[0])]
        line = LineCollection(segments, colors=colors)
        line.set_linewidth(0.1)
        lc = ax.add_collection(line)
        ax.set_xlim(0, max_lag_duration)
        ax.set_ylim(-1, 1)
        ax.set_xlabel(r'Lag, $\delta_k$ (s.)', fontsize=40)
        ax.set_ylabel(r'$\rho$', fontsize=40)
#fg.colorbar(lc, ax=ax)

name = "autocorrelation_paradigm_" + paradigm + "_event_" + event + "_channels_other_correlation_low_pass_high_pass_subject_" + str(subject_id) + "_1channel_10seconds_variable_opacity"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((45, 15), forward=False)
fg.savefig(name, dpi=300, orientation='landscape', bbox_inches='tight')
