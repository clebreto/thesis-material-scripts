t = 0
print(t, "instruction {\"control\":\"start\", \"text\":\"Des instructions vous seront données à l'oral, ainsi qu'à l'écrit dans un encart prévu à cet effet, comme vous pouvez vous en assurer à l'instant.\", \"track\":\"/user/clebreto/home/Documents/thesis/neurofeedback/instruction_0.wav\"}")
t+=10000
print(t, "instruction {\"control\":\"stop\", \"text\":\"\"}")
t+=1000
print(t, "instruction {\"control\":\"start\", \"text\":\"Vous allez être invité(e) soit à fixer l'écran passivement, soit à chercher une stratégie cognitive de votre choix (calcul mental, imagination motrice...) pour controler l'altitude d'une mongolfière dans un environement virtuel, soit à vous reposer et à vous détendre. A l'exception des périodes de repos, nous vous demandons d'éviter les mouvements, ainsi que d'éviter de cligner des yeux.\", \"track\":\"/user/clebreto/home/Documents/thesis/neurofeedback/instruction_1.wav\"}")
t+=20000
print(t, "instruction {\"control\":\"stop\", \"text\":\"\"}")
t+=1000
print(t, "instruction {\"control\":\"start\", \"text\":\"Clignez des yeux à répétition pendant 10 secondes.\"}")
t+=5000
print(t, "instruction {\"control\":\"stop\"}")
t+=1000
print(t, "eyes_blinking {\"control\":\"start\", \"exp_status\":\"training_eye_removal\"}")
t+=10000
print(t, "eyes_blinking {\"control\":\"stop\", \"exp_status\":\"evaluation_eye_removal\"}")
t+=1000
print(t, "instruction {\"control\":\"start\", \"text\":\"Fixez l'écran passivement pendant 30 secondes.\", \"track\":\"/user/clebreto/home/Documents/thesis/neurofeedback/instruction_2.wav\"}")
t+=5000
print(t, "instruction {\"control\":\"stop\"}")
t+=1000
print(t, "passive_fixation {\"control\":\"start\"}")
t+=30000
print(t, "passive_fixation {\"control\":\"stop\"}")
for i in range(6):
    t+=1000
    print(t, "instruction {\"control\":\"start\", \"text\":\"Tentez d'élever la mongolfière avec la stratégie cognitive de votre choix pendant une minute et 15 secondes.\", \"track\":\"/user/clebreto/home/Documents/thesis/neurofeedback/instruction_3.wav\"}")
    t+=10000
    print(t, "instruction {\"control\":\"stop\"}")
    t+=1000
    print(t, "upregulation {\"control\":\"start\"}")
    t+=75000
    print(t, "upregulation {\"control\":\"stop\"}")
    t+=1000
    print(t, "instruction {\"control\":\"start\", \"text\":\"Détendez vous, relaxez vous, bougez si vous en sentez le besoin, l'enregistrement reprend dans 15 secondes.\", \"track\":\"/user/clebreto/home/Documents/thesis/neurofeedback/instruction_4.wav\"}")
    t+=10000
    print(t, "instruction {\"control\":\"stop\"}")
    t+=1000
    print(t, "relaxation {\"control\":\"start\"}")
    t+=15000
    print(t, "relaxation {\"control\":\"stop\"}")
    t+=1000
print(t, "instruction {\"control\":\"start\", \"text\":\"Fixez l'écran passivement pendant 30 secondes.\", \"track\":\"/user/clebreto/home/Documents/thesis/neurofeedback/instruction_2.wav\"}")
t+=5000
print(t, "instruction {\"control\":\"stop\"}")
t+=1000
print(t, "passive_fixation {\"control\":\"start\"}")
t+=30000
print(t, "passive_fixation {\"control\":\"stop\"}")
t+=1000
print(t, "instruction {\"control\":\"start\", \"text\":\"La séance est terminée. Nous vous remercions chaleureusement pour votre participation.\", \"description\":\"Request the subject to keep his eyes open.\", \"track\":\"/user/clebreto/home/Documents/thesis/neurofeedback/instruction_5.wav\"}")
t+=20000
print(t, "instruction {\"control\":\"stop\", \"text\":\"\"}")
t+=1000
print(t, "end {\"control\":\"end\", \"text\":\"END\"}")
