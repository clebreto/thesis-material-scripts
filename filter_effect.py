import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq
from scipy import signal

import matplotlib
matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 30,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 20

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

Fs = 300

sampling_rate = 512
t = np.linspace(0, 1, sampling_rate)
sig = np.cos(5 * 2. * np.pi * t) + 0.4 * np.random.rand(t.shape[0]);

sos = signal.butter(4, [3,6], btype='band', fs=sampling_rate, output='sos')
sosfilt = signal.sosfilt(sos, sig)
filtfilt = signal.sosfiltfilt(sos,sig)

plt.clf()
fg, ax = plt.subplots(1, 1)

ax.plot(t, sig, label="Original signal")
ax.plot(t, sosfilt, label="Butterworth causal filter")
ax.plot(t, filtfilt, label="Forward + Backward Butterworth filter (filtfilt)")
ax.legend()

name = "filter_effect"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((20, 10), forward=False)
plt.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
