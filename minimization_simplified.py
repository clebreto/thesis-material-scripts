import numpy as np
import sys, os, shutil
import matplotlib.pyplot as plt
duration = 10
sampling_rate = 512
nb_samples = duration * sampling_rate
nb_channels = 2
signal  = np.zeros(shape=(nb_channels, nb_samples))
channels = np.array(["cos", "cos_pi"])

signal[0] = np.cos(np.linspace(0, duration, nb_samples))
signal[1] = np.cos(np.linspace(0, duration, nb_samples) + np.pi )

M = signal

## Optimal solution
w_opt = np.array([0.5, 0.5])
print("Optimal", np.dot(w_opt, signal))
print("Optimal score", np.dot(np.dot(np.dot(w_opt, signal), signal.T), w_opt.T))


n = M.shape[0]
H = M.dot(M.T) # Hessian ?

K = H[0:-1,0:-1]
h = H[-1,0:-1]
a = H[-1,-1]
o = np.ones(shape=(n-1))


Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
B = a*o-h

x = np.linalg.solve(Q,B)
w = np.append(x,1-np.dot(x,o))
print(w)


mix = np.dot(w,M)
plt.clf()
fg, ax = plt.subplots(2, 1)
ax[0].plot(mix, label="Mix")
ax[0].plot(signal[0], label=channels[0])
ax[0].plot(signal[1], label=channels[1])
ax[0].legend()

table = ax[1].table(cellText=[[v] for v in w],
                    rowLabels=[k for k in channels],
                    loc='center')
ax[1].get_xaxis().set_visible(False)
ax[1].get_yaxis().set_visible(False)
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].spines['bottom'].set_visible(False)
ax[1].spines['left'].set_visible(False)

cell_dict = table.get_celld()
for c in cell_dict.values():
    c.set_width(0.3)
    c.set_height(0.2)

name = "minimization_simplified"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 10), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
