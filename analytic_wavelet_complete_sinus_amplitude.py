import numpy as np
import matplotlib.pyplot as plt
import sys, os, shutil
import ztrcore
import ztrplotting

duration = 20.
sampling_rate = 512
nb_samples = int(duration * sampling_rate)

t_signal = np.linspace(0., duration, nb_samples)
f_signal = 13.
t_0 = 7
omega_s = 10
signal = np.cos(2. * np.pi * f_signal * t_signal + np.pi / 2.) * np.exp(-(2 * (t_signal - t_0)**2 * np.pi**2 * f_signal**2) / (omega_s**2))


fmin = 7.
fmax = 17.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 10.

##### WT C++ #####
wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(np.expand_dims(signal, axis=(0)))
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(np.abs(wavelet_fourier.output()))[0] / sampling_rate
##################

##### WT Python #####
tf_signal = np.concatenate((signal, np.flip(signal)))
t_morlet = np.linspace(-duration, duration, nb_samples * 2)
tf = np.empty(shape=(frequencies.shape[0], nb_samples), dtype=complex)
for f_i, f in enumerate(frequencies) :
    w = 2. * np.pi * f
    sigma = wavelet_width / w;
    morlet = 1. / np.sqrt(sigma * np.sqrt(np.pi)) * np.exp(1j * w * t_morlet) * np.exp(- t_morlet * t_morlet / (2 * sigma * sigma))
    tf[f_i] = np.fft.ifft(np.fft.fft(morlet) * np.fft.fft(tf_signal))[nb_samples:]
tf = np.abs(tf) / sampling_rate
###################

print(np.sum(tf-wt))

check_0 = np.zeros(shape=wt.shape)
check_1 = np.zeros(shape=wt.shape)
for f_i, f in enumerate(frequencies) :
    nu_s = f_signal
    nu_w = f_i
    omega_s = omega_s
    omega_w = wavelet_width
    phi_s = np.pi/2.
    check_0[f_i] = (np.sqrt(nu_w) * np.sqrt(omega_w) * omega_s) / (2 * np.sqrt(np.sqrt(np.pi)) * np.sqrt(nu_s ** 2 * omega_w ** 2 + nu_w ** 2 * omega_s ** 2)) * np.abs(np.exp((-4j * np.pi * nu_s ** 3 * t_0 * omega_w ** 2 + (-omega_s ** 2 * omega_w ** 2 - 4 * np.pi ** 2 * (t_signal - t_0) ** 2 * nu_w ** 2 + 4j * np.pi * omega_w ** 2 * (t_signal - t_0) * nu_w + -2j * phi_s * omega_w ** 2) * nu_s ** 2 + (-4j * np.pi * t_signal * nu_w ** 2 - 2 * nu_w * omega_w ** 2) * omega_s ** 2 * nu_s - 2 * omega_s ** 2 * nu_w ** 2 * (phi_s * 1j + omega_w ** 2 / 2)) / (2 * nu_s ** 2 * omega_w ** 2 + 2 * nu_w ** 2 * omega_s ** 2)) + np.exp((4j * np.pi * nu_s ** 3 * t_0 * omega_w ** 2 + (-omega_s ** 2 * omega_w ** 2 - 4 * np.pi ** 2 * (t_signal - t_0) ** 2 * nu_w ** 2 + 4j * np.pi * omega_w ** 2 * (t_signal - t_0) * nu_w + 2j * phi_s * omega_w ** 2) * nu_s ** 2 + (4j * np.pi * t_signal * nu_w ** 2 + 2 * nu_w * omega_w ** 2) * omega_s ** 2 * nu_s + 2 * omega_s ** 2 * nu_w ** 2 * (phi_s * 1j - omega_w ** 2 / 2)) / (2 * nu_s ** 2 * omega_w ** 2 + 2 * nu_w ** 2 * omega_s ** 2)))


plt.clf()
fg, ax = plt.subplots(4, 1)
ax[0].plot(signal)
ztrplotting.plotTFAmplitudes(fg, ax[1], np.linspace(0, duration, nb_samples), frequencies, wt, xlabel=r'$Time, \delta_k (s.)$', ylabel=r'$\nu$')
ztrplotting.plotTFAmplitudes(fg, ax[2], np.linspace(0, duration, nb_samples), frequencies, tf, xlabel=r'$Time, \delta_k (s.)$', ylabel=r'$\nu$')
ztrplotting.plotTFAmplitudes(fg, ax[3], np.linspace(0, duration, nb_samples), frequencies, check_0, xlabel=r'$Time, \delta_k (s.)$', ylabel=r'$\nu$')

name = "analytic_wavelet_complete_sinus_amplitude"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 4.), forward=False)
fg.savefig(name + ".png", dpi=300, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
