import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2020_11_24/2020_11_24_16_27_27_0001_alpha.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()[:32] # 33 is GSR
sampling_rate = reader_edf_file.samplingRate()
channels      = reader_edf_file.channels()[:32] # 33 is GSR
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events_reader_output = events_reader.output()

## Reads the headset to recover the closest pairs of electrodes
headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")
X = headset.positions()
Y = sc.spatial.distance.cdist(X, X, 'euclidean')
distance_threshold = 1.
## To compute on the closest pairs and only in 1 direction (arbitrary)
Y_mask = np.triu(np.where(Y < distance_threshold, np.ones(shape=Y.shape), np.zeros(shape=Y.shape)), 1)

fmin = 7.
fmax = 17.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 5.

## Computes the wavelet coeficients with the wavelet trasnform first
wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signal)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(wavelet_fourier.output())

## Exttract the amplitude and the phase
amplitude = np.abs(wt)
phase = np.angle(wt)

window_length = .5 # seconds
window_size = int(window_length * sampling_rate)
nb_windows = int(nb_values / window_size)

def yoyf(t) :
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if t > events_reader_output[e_i].timestamp / 1000. and t < events_reader_output[e_i + 1].timestamp / 1000. :
            return events_reader_output[e_i].uid
        skip = True

## Then analyze window by window (it's not possible to allocate enough memory to compute all the diff)
bars_diff_yo, bins_yo = np.histogram([], bins=100, range=[-np.pi, np.pi])
bars_diff_yf, bins_yf = np.histogram([], bins=100, range=[-np.pi, np.pi])
count_yo = 0
count_yf = 0
for w_i in range(nb_windows) :
    w_start = int(w_i * window_size)
    w_end = int((w_i + 1) * window_size)
    w_phase = phase[:, :,  w_start : w_end]
    w_phase_diff = np.empty(shape=(wt.shape[0], wt.shape[0], wt.shape[1], window_size))
    for c_i in range(Y_mask.shape[0]) :
        for c_j in range(c_i + 1, Y_mask.shape[1]) :
            w_phase_diff[c_i, c_j] = w_phase[c_j, :, :] - w_phase[c_i, :, :]
    ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
    w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
    w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)
    ## Filter to keep only the computed values of interest (upper triangle, close electrodes)
    w_phase_diff = w_phase_diff[np.nonzero(Y_mask)]
    ## Check if the center of the window is in yo or yf
    if yoyf((w_end + w_start) / 2 / sampling_rate) == "alpha" :
        bars_diff_yf += np.histogram(w_phase_diff, bins=100, range=[-np.pi, np.pi])[0]
        count_yf += w_phase_diff.size
    else :
        bars_diff_yo += np.histogram(w_phase_diff, bins=100, range=[-np.pi, np.pi])[0]
        count_yo += w_phase_diff.size


width = 0.7 * (bins_yo[1] - bins_yo[0])
center = (bins_yo[:-1] + bins_yo[1:]) / 2
plt.clf()
plt.bar(center, bars_diff_yo / count_yo, align='center', width=width, color="blue", alpha=0.5, label="Baseline")
plt.bar(center, bars_diff_yf / count_yf, align='center', width=width, color="red", alpha=0.5, label="Eyes Closed")
plt.legend()

name = "histo_delay_closest_1_window_" + str(window_length) + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
