import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2020_11_19/2020_11_19_17_16_21_0001_calculus.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()[:32, 3 * 512:]
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels()[:32])
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/calculus.evt")
events_reader.run()
events_reader_output = events_reader.output()

fmin = 4.
fmax = 17.
fstep = .1
frequency = np.arange(fmin, fmax, fstep)
time = np.linspace(0., duration, nb_values)
w_width = 20.

channels_selection = np.array(['FC5', 'FC1', 'FC2', 'FC6'])
indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]

name = "TF_calculus_baseline_nowindow_fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(w_width)

ztrplotting.plotTF(time, frequency, signal[indices_selection[2]], w_width=w_width, events=events_reader_output, event='calculus', color='white', norm=True)#, save=name)

#shutil.copy("impact.py", name + ".py")
