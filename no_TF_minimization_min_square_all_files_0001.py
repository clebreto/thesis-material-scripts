import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

subject_id = "0001"

fmin = 2.
fmax = 35.
fstep = 0.5
frequencies = np.arange(fmin, fmax, fstep)

wavelet_width = 12.

files = np.array(list(recordings.glob("./*/*" + subject_id + "_alpha.edf")))

# Crop at begin and end of the signal
crop = 3

nb_values = []
sampling_rates = []
nb_channels = []
minima = []
maxima = []
for f in files :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal = reader_edf_file.output()[:32] # 33 is GSR
    nb_channels.append(signal.shape[0])
    sampling_rates.append(reader_edf_file.samplingRate())
    nb_values.append(signal.shape[1])

sampling_rate = sampling_rates[0]
nb_channels = nb_channels[0]
min_nb_values = np.min(np.array(nb_values))

for f in files :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal = reader_edf_file.output()[:32] # 33 is GSR

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(3.)
    bp.setFrequency(10.)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()

    signal_filtered = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
    minima.append(np.min(signal_filtered))
    maxima.append(np.max(signal_filtered))

minimum = np.min(minima)
maximum = np.max(maxima)

# Weights
ev = np.zeros(shape=(0, nb_channels))
w = np.zeros(shape=(0, nb_channels))

# Residuals
residual_ev = np.zeros(shape=(0))
residual_w = np.zeros(shape=(0))
residual_mean = np.zeros(shape=(0))

# Time Frequency analysis
mean_tf_ev = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_w = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_mean = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_ev_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_w_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_mean_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)

# Distribution analysis
bars_old_reference = []
bars_ev = []
bars_w = []
bins = np.histogram([], range=(minimum, maximum), bins=100)[1]

for c_i in range(nb_channels) :
    bars_old_reference.append(np.histogram([], range=(minimum, maximum), bins=100)[0])
    bars_ev.append(np.histogram([], range=(minimum, maximum), bins=100)[0])
    bars_w.append(np.histogram([], range=(minimum, maximum), bins=100)[0])

for f in tqdm(files) :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal        = reader_edf_file.output()[:32, :min_nb_values]
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32])

    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(3.)
    bp.setFrequency(10.)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()
    signal_filtered = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]

    M = signal_filtered
    n = M.shape[0]
    H = M.dot(M.T)

    if np.linalg.matrix_rank(H) == signal.shape[0]:
        ##### sum{w_i^2}=1 #####
        (e_val, e_vec) = np.linalg.eig(H)
        s_e_vec = np.real(e_vec[:, np.argmin(e_val)]) # Select the vector associated to the smaller eigen value

        s_e_vec_norm = s_e_vec / np.sum(s_e_vec)
        ev = np.vstack((ev, np.expand_dims(s_e_vec_norm, axis=0)))
        ref_ev = np.dot(s_e_vec_norm, signal_filtered)
        residual_ev = np.append(residual_ev, np.dot(ref_ev, ref_ev) / signal_filtered.shape[1])
        ref_ev_unfiltered = np.dot(s_e_vec_norm, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)])

        wf_ev = ztrcore.ztrProcessWaveletFourier()
        wf_ev.setFrequencies(frequencies)
        wf_ev.setSignal(np.expand_dims(ref_ev, axis=0))
        wf_ev.setWidth(wavelet_width)
        wf_ev.setSamplingRate(sampling_rate)
        wf_ev.run()
        wf_ev_o = wf_ev.output()
        mean_tf_ev += np.mean(wf_ev_o, axis=0) # Mean over the channels

        wf_ev.setSignal(np.expand_dims(ref_ev_unfiltered, axis=0))
        wf_ev.run()
        wf_ev_o_unfiltered = wf_ev.output()
        mean_tf_ev_unfiltered += np.mean(wf_ev_o_unfiltered, axis=0) # Mean over the channels

        ##### sum{w_i}=1 #####
        K = H[0:-1,0:-1]
        h = H[-1,0:-1]
        a = H[-1,-1]
        o = np.ones((n-1))

        Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
        B = a*o-h

        x = np.linalg.solve(Q,B)
        x = np.append(x, 1 - np.dot(x,o))
        w = np.vstack((w, np.expand_dims(x, axis=0)))
        ref_w = np.dot(x, signal_filtered)
        residual_w = np.append(residual_w, np.dot(ref_w, ref_w) / signal_filtered.shape[1])
        ref_w_unfiltered = np.dot(x, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)])

        wf_w = ztrcore.ztrProcessWaveletFourier()
        wf_w.setFrequencies(frequencies)
        wf_w.setSignal(np.expand_dims(ref_w, axis=0))
        wf_w.setWidth(wavelet_width)
        wf_w.setSamplingRate(sampling_rate)
        wf_w.run()
        wf_w_o = wf_w.output()
        mean_tf_w += np.mean(wf_w_o, axis=0) # Mean over the channels

        wf_w.setSignal(np.expand_dims(ref_w_unfiltered, axis=0))
        wf_w.run()
        wf_w_o_unfiltered = wf_w.output()
        mean_tf_w_unfiltered += np.mean(wf_w_o_unfiltered, axis=0) # Mean over the channels

        ##### mean reference #####
        mean = np.ones(signal_filtered.shape[0]) / signal_filtered.shape[0]
        ref_mean = np.dot(mean, signal_filtered)
        residual_mean = np.append(residual_mean, np.dot(ref_mean, ref_mean) / signal_filtered.shape[1])
        ref_mean_unfiltered = np.dot(mean, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)])

        wf_mean = ztrcore.ztrProcessWaveletFourier()
        wf_mean.setFrequencies(frequencies)
        wf_mean.setSignal(np.expand_dims(ref_mean, axis=0))
        wf_mean.setWidth(wavelet_width)
        wf_mean.setSamplingRate(sampling_rate)
        wf_mean.run()
        wf_mean_o = wf_mean.output()
        mean_tf_mean += np.mean(wf_mean_o, axis=0) # Mean over the channels

        wf_mean.setSignal(np.expand_dims(ref_mean_unfiltered, axis=0))
        wf_mean.run()
        wf_mean_o_unfiltered = wf_mean.output()
        mean_tf_mean_unfiltered += np.mean(wf_mean_o_unfiltered, axis=0) # Mean over the channels

        for c_i in range(signal_filtered.shape[0]):
            bars_old_reference[c_i] += np.histogram(signal_filtered[c_i], range=(minimum, maximum), bins=100)[0]
            bars_ev[c_i] += np.histogram(signal_filtered[c_i] - ref_ev, range=(minimum, maximum), bins=100)[0]
            bars_w[c_i] += np.histogram(signal_filtered[c_i] - ref_w, range=(minimum, maximum), bins=100)[0]

    else:
        print("Matrix is not full rank")

# Mean over recordings
mean_tf_ev /= residual_mean.shape[0]
mean_tf_w /= residual_mean.shape[0]
mean_tf_mean /= residual_mean.shape[0]

mean_tf_ev_unfiltered /= residual_mean.shape[0]
mean_tf_w_unfiltered /= residual_mean.shape[0]
mean_tf_mean_unfiltered /= residual_mean.shape[0]

vmin = min(np.min(np.abs(mean_tf_ev)), np.min(np.abs(mean_tf_w)), np.min(np.abs(mean_tf_mean)))
vmax = max(np.max(np.abs(mean_tf_ev)), np.max(np.abs(mean_tf_w)), np.max(np.abs(mean_tf_mean)))

plt.clf()
fg, ax = plt.subplots(9, 3)
positions = np.arange(0, ev.shape[0], 1)
ev_f = [ef for ef in ev]
ev_c = [ec for ec in ev.T]
w_f = [wf for wf in w]
w_c = [wc for wc in w.T]
ax[0, 0].boxplot(w_f, showfliers=False, showmeans=True)
ax[0, 0].set_title("sum{w_i}=1")
ax[1, 0].boxplot(w_c, showfliers=False, showmeans=True, labels=channels)
ax[1, 0].set_title("sum{w_i}=1")
ax[0, 1].boxplot(ev_f, showfliers=False, showmeans=True)
ax[0, 1].set_title("sum{w_i^2}=1")
ax[1, 1].boxplot(ev_c, labels=channels)
ax[1, 1].set_title("sum{w_i^2}=1")
for b_i, b in enumerate(bars_w) :
    ax[2, 0].plot(bins[:-1], b, label=channels[b_i])
ax[2, 0].legend(ncol=3)
ax[2, 0].set_title("sum{w_i}=1")
for b_i, b in enumerate(bars_ev) :
    ax[2, 1].plot(bins[:-1], b, label=channels[b_i])
ax[2, 1].legend(ncol=3)
ax[2, 1].set_title("sum{w_i^2}=1")
for b_i, b in enumerate(bars_old_reference) :
    ax[2, 2].plot(bins[:-1], b, label=channels[b_i])
ax[2, 2].legend(ncol=3)
ax[2, 2].set_title("Filtered signal, old reference (CPz)")
ztrplotting.plotTopography(ax[3, 0], np.mean(w, axis=0), headset)
ztrplotting.plotTopography(ax[3, 1], np.mean(ev, axis=0), headset)
ax[4, 1].scatter(positions, residual_ev, label="sum{w_i^2}=1")
ax[4, 1].scatter(positions, residual_w, label="sum{w_i}=1")
ax[4, 1].scatter(positions, residual_mean, label="sum{w_i}=1, w_i = 1/n")
ax[4, 1].legend()
ax[4, 1].set_title("Residuals")
# ztrplotting.plotTFAmplitudes(ax[5, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_w, ratio = 2. / 10., title="sum{w_i}=1 TF mean over channels and experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(ax[5, 1], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_ev, ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over channels and experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(ax[5, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_mean, ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over channels and experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(ax[6, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_w, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i}=1 TF mean over channels and experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(ax[6, 1], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_ev, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over channels and experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(ax[6, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_mean, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over channels and experiments\n Alpha band filtering")

# ztrplotting.plotTFAmplitudes(ax[7, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_w_unfiltered, ratio = 2. / 10., title="sum{w_i}=1 TF mean over channels and experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(ax[7, 1], np.linspace(crop, duration - crop , int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_ev_unfiltered, ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over channels and experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(ax[7, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_mean_unfiltered, ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over channels and experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(ax[8, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_w_unfiltered, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i}=1 TF mean over channels and experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(ax[8, 1], np.linspace(crop, duration - crop , int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_ev_unfiltered, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over channels and experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(ax[8, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, mean_tf_mean_unfiltered, vmin=vmin, vmax=vmax, ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over channels and experiments\n No filtering")

name = "no_TF_minimization_min_square_all_files_" + subject_id
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((35, 50), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
