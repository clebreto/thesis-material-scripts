import numpy as np
import matplotlib.pyplot as plt
from scipy import stats, optimize, interpolate
import sys, os, shutil
import pathlib as pl
import ztrcore

## To load dynamic zither plugins
ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def measure(files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection, montage) :
    short_range_pairs_indices = np.nonzero(np.triu(short_range_neighbors))
    short_range_nb_pairs = len(short_range_neighbors[short_range_pairs_indices])
    long_range_pairs_indices = np.nonzero(np.triu(long_range_neighbors))
    long_range_nb_pairs = len(long_range_neighbors[long_range_pairs_indices])

    # Get the different ids
    basenames = []
    for f in files :
        basename = os.path.basename(os.path.dirname(f))
        if basename not in basenames :
            basenames.append(basename)

    short_range_output = {}
    long_range_output = {}
    for b in basenames :
        short_range_output[b] = np.empty(shape=(short_range_nb_pairs, frequencies.shape[0], 0))
        long_range_output[b] = np.empty(shape=(long_range_nb_pairs, frequencies.shape[0], 0))

    for f in files :
        basename = os.path.basename(os.path.dirname(f))
        reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
        reader_edf_file.setFilePath(str(f))
        reader_edf_file.run()
        data          = reader_edf_file.output()
        sampling_rate = reader_edf_file.samplingRate()
        channels      = reader_edf_file.channels() # C x T (real)

        channels_selector = ztrcore.ztrProcessChannelsSelector()
        channels_selector.setChannels(channels)
        channels_selector.setSignal(data)
        channels_selector.setSelection(selection)
        channels_selector.run()
        selected_data   = channels_selector.output() # C x T (real)
        selected_channels = channels_selector.outputChannels()

        nb_channels = selected_data.shape[0]
        nb_values   = selected_data.shape[1]
        duration  = nb_values / sampling_rate

        # Filter the signal on several frequency bands (center_freq + bandwidth)
        filter_bank = ztrcore.ztrProcessFilterBankOffline()
        filter_bank.setFrequencies(frequencies);
        filter_bank.setBandwidths(bandwidths);
        filter_bank.setSamplingRate(sampling_rate);
        filter_bank.setSignal(selected_data);
        filter_bank.setOrder(6);
        filter_bank.run();
        filter_bank_output = filter_bank.output() # C x F x T (real)

        # Compute for each frequency band the analytic signal
        hilbert_bank = ztrcore.ztrProcessHilbertBank()
        hilbert_bank.setSamplingRate(sampling_rate);
        hilbert_bank.setSignal(filter_bank_output);
        hilbert_bank.run();
        hilbert_bank_output = hilbert_bank.output(); # C x F x T (complex)

        # Compute the synchrony measure on windows
        tfwt = ztrcore.ztrProcessTFWindowT(measure_name, hilbert_bank_output, sampling_rate, window_length, window_overlapping)
        tfwt.run()
        tfwt_output = tfwt.output() # C x C x F x T (real)

        nb_channels    = tfwt_output.shape[0];
        nb_frequencies = tfwt_output.shape[2];
        nb_windows     = tfwt_output.shape[3];

        temp_short_range_output = np.empty(shape=(short_range_nb_pairs, nb_frequencies, nb_windows))
        temp_long_range_output  = np.empty(shape=(long_range_nb_pairs, nb_frequencies, nb_windows))

        s_k = 0
        l_k = 0
        for i in range(nb_channels) :
            for j in range(i + 1, nb_channels) :
                if short_range_neighbors[i, j] == 1 :
                    temp_short_range_output[s_k, :, :] = tfwt_output[i, j, :, :]
                    s_k += 1
                if long_range_neighbors[i, j] == 1 :
                    temp_long_range_output[l_k, :, :] = tfwt_output[i, j, :, :]
                    l_k += 1

        short_range_output[basename] = np.concatenate((short_range_output[basename], temp_short_range_output), axis=2)
        long_range_output[basename] = np.concatenate((long_range_output[basename], temp_long_range_output), axis=2)
    return (short_range_output, long_range_output)

# Parameters
frequencies = np.array([2.5, 5.5, 8.5, 11.5, 15.5, 24., 50.])
bandwidths = np.array([3., 3., 3., 3., 5., 12., 40.])
frequencies_names = ["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"]

measure_name       = "Plv"
window_length      = 2.
window_overlapping = 0.

short_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/short_range.npy")
long_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/long_range.npy")

selection = ztrcore.readSelection("/home/clebreto/programming/come/zither/resources/selection/16battacharya.sel")
montage   = ztrcore.readMontage("/home/clebreto/programming/come/zither/resources/montages/21Longitudinal.mtg")

recordings = pl.Path("/home/clebreto/Data/Nice/")

condition = "yf"

condition_name = ""
if condition =="yo" :
    condition_name = "Eyes Open"
else :
    condition_name = "Eyes Closed"

A_files = np.array(list(recordings.glob("./Epi_ZE_connue_SP/*/*" + condition + "*.edf")))
# Ignore patients looking like outliers
# to_ignore = ["e224033c80", "7f4499ef3e", "e5cb01cc3a", "53fdbcb694", "f8766225fd", "8f91885dd8"]
to_ignore = []
A_files = np.array([a for a in A_files if (a.parts[-2]) not in to_ignore])

B_files = np.array(list(recordings.glob("./Temoins/temoin_*/*" + condition + "*.edf")))

(A_short_range_output, A_long_range_output) = measure(A_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection, montage)
(B_short_range_output, B_long_range_output) = measure(B_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection, montage)

# Get max nb of windows :
max_nb_windows = 0
for p in A_short_range_output.values() :
    if p[:, 4, :].size > max_nb_windows :
        max_nb_windows = p[:, 4, :].size
for p in A_long_range_output.values() :
    if p[:, 4, :].size > max_nb_windows :
        max_nb_windows = p[:, 4, :].size
for p in B_short_range_output.values() :
    if p[:, 4, :].size > max_nb_windows :
        max_nb_windows = p[:, 4, :].size
for p in B_long_range_output.values() :
    if p[:, 4, :].size > max_nb_windows :
        max_nb_windows = p[:, 4, :].size

plt.clf()
fg, ax = plt.subplots(2, 2)

frequency = 6
positions_epi = np.arange(0, len(A_short_range_output.items()), 1)
flattened_epi_short = []
widths_epi_short = []
for patient in A_short_range_output.items() :
    flattened_epi_short.append(patient[1][:, :, :].flatten())
    #widths_epi_short.append(patient[1][:, :, :].size / max_nb_windows)
    widths_epi_short.append(1.)
ax[0, 0].boxplot(flattened_epi_short, positions = positions_epi, widths=widths_epi_short, showfliers=False, showmeans=True)
ax[0, 0].set_ylim(0, 1)
ax[0, 0].set_xticks(np.arange(0, len(A_short_range_output.keys()), 1))
ax[0, 0].set_xticklabels(A_short_range_output.keys())
ax[0, 0].tick_params(axis='x', labelrotation = 45)
ax[0, 0].set_title("Epileptic short range")

flattened_epi_long = []
widths_epi_long = []
for patient in A_long_range_output.items() :
    flattened_epi_long.append(patient[1][:, :, :].flatten())
    #widths_epi_long.append(patient[1][:, :, :].size / max_nb_windows)
    widths_epi_long.append(1.)
ax[1, 0].boxplot(flattened_epi_long, positions = positions_epi, widths=widths_epi_long, showfliers=False, showmeans=True)
ax[1, 0].set_ylim(0, 1)
ax[1, 0].set_xticks(np.arange(0, len(A_long_range_output.keys()), 1))
ax[1, 0].set_xticklabels(A_long_range_output.keys())
ax[1, 0].tick_params(axis='x', labelrotation = 45)
ax[1, 0].set_title("Epileptic long range")

positions_hea = np.arange(0, len(B_short_range_output.items()), 1)
flattened_hea_short = []
widths_hea_short = []
for patient in B_short_range_output.items() :
    flattened_hea_short.append(patient[1][:, :, :].flatten())
    #widths_hea_short.append(patient[1][:, :, :].size / max_nb_windows)
    widths_hea_short.append(1.)

ax[0, 1].boxplot(flattened_hea_short, positions = positions_hea, widths=widths_hea_short, showfliers=False, showmeans=True)
ax[0, 1].set_ylim(0, 1)
ax[0, 1].set_xticks(np.arange(0, len(B_short_range_output.keys()), 1))
ax[0, 1].set_xticklabels(B_short_range_output.keys())
ax[0, 1].tick_params(axis='x', labelrotation = 45)
ax[0, 1].set_title("Healthy short range")

flattened_hea_long = []
widths_hea_long = []
for patient in B_long_range_output.items() :
    flattened_hea_long.append(patient[1][:, :, :].flatten())
    #widths_hea_long.append(patient[1][:, :, :].size / max_nb_windows)
    widths_hea_long.append(1.)
ax[1, 1].boxplot(flattened_hea_long, positions = positions_hea, widths=widths_hea_long, showfliers=False, showmeans=True)
ax[1, 1].set_ylim(0, 1)
ax[1, 1].set_xticks(np.arange(0, len(B_long_range_output.keys()), 1))
ax[1, 1].set_xticklabels(B_long_range_output.keys())
ax[1, 1].tick_params(axis='x', labelrotation = 45)
ax[1, 1].set_title("Healthy long range")

plt.tight_layout()

name = "outliers_bhattacharya_variability_all_freqs_" + condition_name + "_all_" + measure_name + "_wl_" + str(window_length) + "_wo_" + str(window_overlapping)
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((25., 8.), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
