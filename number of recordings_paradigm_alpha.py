import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import FuncFormatter, MultipleLocator, FormatStrFormatter
from scipy import stats
from scipy.stats import vonmises
from tqdm import tqdm

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

paradigm = 'alpha' # possible values : 'alpha', 'calculus'
subject_ids = ["0001", "0002", "0003", "0004", "0005", "0006", "0007", "0008", "0009"]

subject_nb_files = {}
for subject_id in subject_ids :
    files = sorted(recordings.glob("./*/*" + str(subject_id) + "_" + paradigm + ".edf"))
    files = [f for f in files]
    subject_nb_files[subject_id] = len(files)
    print(subject_id, len(files))

name = "number of recordings_paradigm_" + paradigm
shutil.copy("impact.py", name + ".py")
