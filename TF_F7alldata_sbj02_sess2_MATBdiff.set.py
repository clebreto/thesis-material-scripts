import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import mne

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

channels_section = ['P3', 'F7', 'F5', 'F3', 'F1', 'F2', 'F4', 'F6', 'AF3', 'AFz', 'AF4', 'Fp1',
          'Fp2']

file_path_root = "/user/clebreto/home/Data/Hackathlon/P02/S2/eeg/alldata_sbj02_sess2_"
condition = ['RS', 'MARBeasy', 'MATBmed', 'MATBdiff']

signals = []
for c in condition :
    epochs = mne.io.read_epochs_eeglab(file_path, verbose=False)  # FIR 1-40 Hz
    epochs = epochs.pick_channels(channels_section)
    data = epochs.get_data()
    signal = np.zeros(shape=(data.shape[1], data.shape[2]*data.shape[0]))
    for e in range(data.shape[0]) :
        signal[:, e * data.shape[2] : (e +1) * data.shape[2]] = data[e]
    print(c, signal.shape)
    signals.append(signal)

signal = np.hstack(signals)

sampling_rate = 250.

nb_values = signal.shape[1]
duration = nb_values / sampling_rate
print(duration)

plt.clf()
fig, ax = plt.subplots(2, 1)

time = np.linspace(0, duration, nb_values)
frequencies = np.arange(5, 70, 0.1)

ztrplotting.plotTFAmplitudes(ax[0], time, frequencies, signal[1], w_width=13, events='', event='')
ztrplotting.plotTFPhases(ax[1], time, frequencies, signal[1], w_width=13, events='', event='')

name = "TF_" + "F7" + file_path.name
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((90., 20.), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
