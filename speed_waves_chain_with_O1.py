import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 10,
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

colors = np.array(["black", "red", "blue", "cyan"])
labels = np.array(['O1-P3', 'O1-C3', 'O1-F3', 'O1-Fp1'])
labels_2 = np.array(['Sum O1-P3', 'Sum O1-C3', 'Sum O1-F3', 'Sum O1-Fp1'])
X = np.array([5.5, 11.5, 17., 25.])#cm
#Eyes Closed P3-O1 (8.479692480561244, 0.3992100625286519, 1.0)
#Eyes Closed C3-P3 (5.168242856914855, 0.3215749946781761, 1.0)
#Eyes Closed F3-C3 (8.55732772567275, 0.13812084021130966, 1.0)
#Eyes Closed Fp1-F3 (14.366433517402772, 0.09008167464632888, 1.0)
Y = np.array([0.3992100625286519, 0.3215749946781761, 0.13812084021130966, 0.09008167464632888])#rad
for i in range(1, Y.shape[0]):
    Y[i] = Y[i-1]+Y[i]
#Eyes Open P3-O1 (8.557884868850918, 0.3992933054325575, 1.0)
#Eyes Open C3-O1 (1.8420863770579845, 0.8414004787437921, 1.0)
#Eyes Open F3-O1 (0.9367568606397745, 1.3416658307892964, 1.0)
#Eyes Open Fp1-O1 (0.6455284034580415, 1.9757143373824362, 1.0)
Y2 = np.array([0.3992933054325575, 0.8414004787437921, 1.3416658307892964, 1.9757143373824362])#rad

#rad to ms
Y /= (2.*np.pi) / 100
Y2 /= (2.*np.pi) / 100

m, b = np.polyfit(X, Y, 1)

plt.clf()
plt.plot(X, m*X+b, '--k') #'--k'=black dashed line, 'yo' = yellow circle marker

for (x, y, l, c) in zip(X, Y, labels, colors):
    plt.scatter(x, y, color=c, label=l, s=200)

for (x, y, l, c) in zip(X, Y2, labels_2, colors):
    plt.scatter(x, y, color=c, label=l, marker='2', s=500)

plt.legend()
plt.xlabel("Distance (cm)")
plt.ylabel("Delay (ms)")
plt.tight_layout()

name = "speed_waves_chain_with_O1"
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
