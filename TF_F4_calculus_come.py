import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
import mne

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/home/clebreto/Data/Neurofeedback/2020_11_24/2020_11_24_17_02_29_0001_calculus.edf")

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()[:32] # 33 is GSR
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

channel = 'F4'
channel_index = np.argwhere(channels == channel)[0][0]
nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/calculus.evt")
events_reader.run()
events = events_reader.output()

time = np.linspace(0, duration, nb_values)
frequencies = np.arange(3., 15, 0.05)

fig, ax = plt.subplots(1, 1)
ztrplotting.plotTFAmplitudes(ax, time, frequencies, signal[channel_index], w_width=70, events=events, event=['calculus'], color='red', channel_name=channel)

name = "TF_" + channel + "_calculus" + "_come"
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((90., 20.), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
