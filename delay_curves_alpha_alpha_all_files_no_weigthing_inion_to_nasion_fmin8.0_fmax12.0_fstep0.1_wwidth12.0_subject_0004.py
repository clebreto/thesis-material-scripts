import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from mpl_toolkits.axes_grid1 import make_axes_locatable

import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")
positions = headset.positions()
labels    = np.array(headset.labels())
electrodes_positions = dict(zip(labels, positions))

x = [positions[x, 0] for x in range(len(positions))]
y = [positions[y, 1] for y in range(len(positions))]

pairs_labels = [["Oz", "POz"], ["POz", "Pz"], ["Pz", "Cz"], ["Cz", "Fz"], ["Fz", "Fpz"]]
pairs_indices = []
for p in pairs_labels :
    pairs_indices.append([np.where(labels == c)[0][0] for c in p])


short_range_neighbors = np.zeros(shape=(len(labels), len(labels)))
for p_i in pairs_indices :
    short_range_neighbors[p_i[1], p_i[0]] = 1

pairs_indices = np.nonzero(short_range_neighbors)
nb_pairs = len(short_range_neighbors[pairs_indices])

pairs_x = []
pairs_y = []
pairs_labels = []
for p_i in range(nb_pairs):
    pairs_labels.append(labels[pairs_indices[1][p_i]] + "-" + labels[pairs_indices[0][p_i]])
    pairs_x.append((positions[pairs_indices[1][p_i]][0] + positions[pairs_indices[0][p_i]][0]) / 2.)
    pairs_y.append((positions[pairs_indices[1][p_i]][1] + positions[pairs_indices[0][p_i]][1]) / 2.)

## Wavelet Transform
fmin = 8.
fmax = 12.
fstep = 0.1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

subject_id = "0004"
nb_files = 0
files = sorted(recordings.glob("./*/*" + subject_id + "_alpha.edf"))
min_nb_values = float('inf')
for f in files :
    nb_files += 1
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32] # 33 is GSR
    nb_values = signal.shape[1]
    if nb_values < min_nb_values :
        min_nb_values = nb_values

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events = events_reader.output()

sampling_rate = 512.

window_length = 2. # seconds
window_size = int(window_length * sampling_rate)
nb_windows = int(min_nb_values / window_size)


l = int(np.ceil(np.sqrt(nb_files)))
c = int(np.ceil(nb_files / l))
plt.clf()
fig, ax = plt.subplots(l, c)

files = sorted(recordings.glob("./*/*" + subject_id + "_alpha.edf"))
i = 0
skip = False
for f in files :
    print(str(f))
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32, :min_nb_values] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output())

    ## Exttract the amplitude and the phase
    amplitude = np.abs(wt)
    phase = np.angle(wt)

    diff = np.zeros(shape=(nb_pairs, nb_windows))
    for w_i in range(nb_windows) :
        w_start = int(w_i * window_size)
        w_end = int((w_i + 1) * window_size)
        w_phase = phase[:, :,  w_start : w_end]
        w_phase_diff = np.empty(shape=(nb_pairs, wt.shape[1], window_size))
        for p_i in range(nb_pairs) :
            w_phase_diff[p_i] = w_phase[pairs_indices[1][p_i]] - w_phase[pairs_indices[0][p_i]]
        ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
        w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
        w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)

        diff[:, w_i] = np.mean(w_phase_diff, axis=(1, 2))

    for p_i in range(nb_pairs) :
        ax[int(np.floor(i / l)), i % c].plot(diff[p_i], label=pairs_labels[p_i], linewidth=0.3)
    ax[int(np.floor(i / l)), i % c].legend()
    ax[int(np.floor(i / l)), i % c].set_title(f.stem)

    i += 1

name = "delay_curves_alpha_alpha_all_files_no_weigthing_inion_to_nasion_" + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width) + "_subject_" + subject_id
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((15.5, 12.5), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
