import numpy as np
import ztrcore
import numpy as np
import matplotlib.pyplot as plt
import shutil

def euler(x_1, system, d_t):
    return x_1 + d_t * system.derivative(x_1)

def rk4(x_1, system, d_t):
    k1 = system.derivative(x_1)
    k2 = system.derivative(x_1 + d_t / 2. * k1)
    k3 = system.derivative(x_1 + d_t / 2. * k2)
    k4 = system.derivative(x_1 + d_t * k3)
    return x_1 + d_t / 6. * (k1 + 2 * k2 + 2 * k3 + k4)

class Rossler:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def derivative(self, x):
        dx = np.empty(shape=x.shape)
        dx[0] = - self.a * (x[1] + x[2])
        dx[1] =   self.a * (x[0] + self.b * x[1])
        dx[2] =   self.a * (self.b + x[2] * x[0] - self.c * x[2])
        return dx

class Lorenz:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        self.x_b = None
        self.c_s = 0.

    def setCoupling(x_b, c_s):
        self.x_b = np.zeros(shape=(3))
        self.c_s = c_s

    def derivative(self, x):
        dx = np.empty(shape=x.shape)
        dx[0] = self.a * (x[1] - x[0])
        dx[1] = self.b * x[0] - x[1] - x[0] * x[2] + self.c_s * self.x_b[1] * self.x_b[1]
        dx[2] = x[0] * x[1] - self.c * x[2]
        return dx

class Hindmarshrose:
    def __init__(self, a, b, c, d, s, r, I, qi):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.s = s
        self.r = r
        self.I = I
        self.qi = qi

        self.x_b = np.zeros(shape=(3))
        self.c_s = 0

    def setCoupling(self, x_b, c_s):
        self.x_b = x_b
        self.c_s = c_s

    def derivative(self, x):
        dx = np.empty(shape=x.shape)
        dx[0] = 1000 * (x[1] - self.a * x[0] * x[0] * x[0] + self.b * x[0] * x[0] - x[2] + self.I - self.c_s * (x[0] - self.x_b[0]))
        dx[1] = 1000 * (self.c - self.d * x[0] * x[0] - x[1])
        dx[2] = 1000 * (self.r * (self.s * (x[0] + self.qi) - x[2]))
        return dx

hindmarshrose_a = Hindmarshrose(1., 3., 1., 5., 4., 0.006, 3., 1.56)
hindmarshrose_b = Hindmarshrose(1., 3., 1., 5., 4., 0.006, 3., 1.57)

#Them the time interval and the step size
t_start = 0.
t_end   = 30. #seconds
sampling_rate = 51200.
d_t     = 1. / sampling_rate #sampling step = 1. / sampling_rate

numsteps = int((t_end - t_start) / d_t)

t = np.linspace(t_start, t_end, numsteps)

x_a = np.empty(shape=(3, numsteps))
x_b = np.empty(shape=(3, numsteps))

#Initial conditions
x_a[0, 0] = 0.
x_a[1, 0] = 0.
x_a[2, 0] = 2.6

x_b[0, 0] = 4.
x_b[1, 0] = 2.
x_b[2, 0] = 2.6

k0_switch = 10.  * sampling_rate #samples
k1_switch = 20.  * sampling_rate #samples

c_s = 0.09 # coupling strength
for k in range(0, t.shape[0] - 1):
    if k <= k0_switch :
        c_s = 0.0
    elif k <= k1_switch :
        c_s = 0.09
    else :
        c_s = 0.3

    hindmarshrose_a.setCoupling(x_b[:, k], c_s)
    hindmarshrose_b.setCoupling(x_a[:, k], c_s)
    x_a[:, k+1] = euler(x_a[:, k],  hindmarshrose_a, t[k+1] - t[k])
    x_b[:, k+1] = euler(x_b[:, k],  hindmarshrose_b, t[k+1] - t[k])

x = np.concatenate((x_a[0:1, :], x_b[0:1, :]), axis=0)

# subsample :
resampling_rate = 512
resampling = int(sampling_rate / resampling_rate)
x = x[:, ::resampling]

ztr_process_wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
ztr_process_wavelet_fourier.setFrequencies(np.arange(1, 100, 1.))
ztr_process_wavelet_fourier.setSignal(x)
ztr_process_wavelet_fourier.setWidth(7.)
ztr_process_wavelet_fourier.setSamplingRate(resampling_rate)
ztr_process_wavelet_fourier.run()
ztr_process_wavelet_fourier_output = ztr_process_wavelet_fourier.output()

window_length = 0.5
window_overlapping = 0.125
ztr_process_window_t = ztrcore.ztrProcessTFWindowT("Plv", ztr_process_wavelet_fourier_output, resampling_rate, window_length, window_overlapping)
ztr_process_window_t.run()
ztr_process_window_t_output_fourier = ztr_process_window_t.output()

nb_values = ztr_process_wavelet_fourier_output.shape[2]

plt.rcdefaults()
plt.figure()

fig, axes = plt.subplots(5, 1, figsize=(8,8))

axes[0].plot(t[::resampling], x[0], color="blue")
axes[1].plot(t[::resampling], x[1], color="red")
axes[2].imshow(np.abs(ztr_process_wavelet_fourier_output[0][:, ::30]))
axes[3].matshow(np.abs(ztr_process_wavelet_fourier_output[1][:, ::30]))
axes[4].imshow(ztr_process_window_t_output_fourier[0, 1])

name = "system_hindmarsh_rose"
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((20.5, 20.5), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})

print("end")
