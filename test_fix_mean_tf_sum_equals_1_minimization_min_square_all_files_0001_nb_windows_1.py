import ztrcore
import ztrplotting
import numpy as np
from scipy.linalg import eigh
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

import matplotlib
matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 30,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 20

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")

#recordings = pl.Path("/user/clebreto/home/Data/Nice/")
recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

subject_id = "0001"

#Filter
center_freq = 10.
bandwidth = 3.

files = np.array(list(recordings.glob("./*/*" + subject_id + "_alpha.edf")))

# Crop at begin and end of the signal
crop = 3

f = files[0]

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(f))
reader_edf_file.run()

signal       = reader_edf_file.output()[:32, :min_nb_values]
channels     = np.array(reader_edf_file.channels()[:32])
nb_channels  = channels.shape[0]
nb_values    = signal.shape[1]
duration     = nb_values / sampling_rate

bp = ztrcore.ztrProcessFilterBandPassOffline()
bp.setBandwidth(bandwidth)
bp.setFrequency(center_freq)
bp.setOrder(4)
bp.setSamplingRate(sampling_rate)
bp.setSignal(signal)
bp.run()
signal_filtered = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]

ev_file = np.zeros(shape=(nb_windows, nb_channels))
w_file = np.zeros(shape=(nb_windows, nb_channels))
ratio_file = np.zeros(shape=(nb_windows, nb_channels))

M = signal_filtered
n = M.shape[0]
S = signal[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
H = M.dot(M.T)
G = S.dot(S.T)

if np.linalg.matrix_rank(H) == signal.shape[0]: # Ensures matrix is full rank
    ##### ratio #####
    (ratio_e_val, ratio_e_vec) = np.linalg.eig(np.dot(np.linalg.inv(G), H))
    #(ratio_e_val, ratio_e_vec) = eigh(H, G)
    s_ratio_e_vec = np.real(ratio_e_vec[:, np.argmin(ratio_e_val)]) # Select the vector associated to the smaller eigen value
    ref_ratio = np.dot(s_ratio_e_vec, signal_filtered)

    ratio_file[w_i] = s_ratio_e_vec

    ##### sum{w_i^2}=1 #####
    (e_val, e_vec) = np.linalg.eig(H)
    s_e_vec = np.real(e_vec[:, np.argmin(e_val)]) # Select the vector associated to the smaller eigen value

    #s_e_vec_norm = s_e_vec / np.sum(s_e_vec)
    ref_ev = np.dot(s_e_vec, signal_filtered)

    ev_file[w_i] = s_e_vec

    ##### sum{w_i}=1 #####
    K = H[0:-1,0:-1]
    h = H[-1,0:-1]
    a = H[-1,-1]
    o = np.ones((n-1))

    Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
    B = a*o-h

    x = np.linalg.solve(Q,B)
    x = np.append(x, 1 - np.dot(x,o))
    ref_w = np.dot(x, signal_filtered)

    ##### mean reference #####
    mean = np.ones(signal_filtered.shape[0]) / signal_filtered.shape[0]
    ref_mean = np.dot(mean, signal_filtered)
else:
    print("Matrix is not full rank")


hp = ztrcore.ztrProcessFilterHighPassOffline()
hp.setFrequency(2.)
hp.setSamplingRate(sampling_rate)
hp.setOrder(4)
hp.setSignal(signal)
hp.run()
signal = np.copy(hp.output())[:, int(crop*sampling_rate): -int(crop*sampling_rate)]

bs = ztrcore.ztrProcessFilterBandStopOffline()
bs.setBandwidth(bandwidth)
bs.setFrequency(center_freq)
bs.setOrder(4)
bs.setSamplingRate(sampling_rate)
bs.setSignal(signal)
bs.run()
signal = np.copy(bs.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)])

lp = ztrcore.ztrProcessFilterLowPassOffline()
lp.setFrequency(30.)
lp.setSamplingRate(sampling_rate)
lp.setOrder(4)
lp.setSignal(signal)
lp.run()
signal = np.copy(lp.output())[:, int(crop*sampling_rate): -int(crop*sampling_rate)]

ref_ratio_unfiltered = np.dot(s_ratio_e_vec, signal)
ref_ev_unfiltered = np.dot(s_e_vec, signal)
ref_w_unfiltered = np.dot(x, signal)
ref_mean_unfiltered = np.dot(mean, signal)

plt.clf()
fg, ax = plt.subplots(5, 2)
print("weights", x)
print("energy signal", np.mean(signal[29] * signal[29]))
print("energy mean", np.mean(ref_mean_unfiltered * ref_mean_unfiltered))
print("energy x", np.mean(ref_w_unfiltered * ref_w_unfiltered))
print("energy s_e_vec", np.mean(ref_ev_unfiltered * ref_ev_unfiltered))
print("energy ratio", np.mean(ref_ratio_unfiltered * ref_ratio_unfiltered))


ax[0, 0].plot(M[29], linewidth=0.2)
ax[0, 0].plot(M[29]-ref_mean, linewidth=0.2)

ax[1, 0].plot(M[29], linewidth=0.2)
ax[1, 0].plot(M[29]-ref_w, linewidth=0.2)

ax[2, 0].plot(M[29], linewidth=0.2)
ax[2, 0].plot(M[29]-ref_ev, linewidth=0.2)

ax[3, 0].plot(M[29], linewidth=0.2)
ax[3, 0].plot(M[29]-ref_ratio, linewidth=0.2)

ax[4, 0].plot(ref_mean, linewidth=0.2)
ax[4, 0].plot(ref_w, linewidth=0.2)
ax[4, 0].plot(ref_ratio, linewidth=0.2)
ax[4, 0].plot(ref_ev, linewidth=0.2)

start = 95
stop = 115
time = np.linspace(start, stop, int(sampling_rate * (stop-start)))
ax[0, 1].plot(time, signal[29][int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2)
ax[0, 1].plot(time, signal[29][int(start*sampling_rate):int(stop*sampling_rate)]-ref_mean_unfiltered[int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2, label="mean")
ax[0, 1].legend()

ax[1, 1].plot(time, signal[29][int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2)
ax[1, 1].plot(time, signal[29][int(start*sampling_rate):int(stop*sampling_rate)]-ref_w_unfiltered[int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2, label="sum 1")
ax[1, 1].legend()

ax[2, 1].plot(time, signal[29][int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2)
ax[2, 1].plot(time, signal[29][int(start*sampling_rate):int(stop*sampling_rate)]-ref_ev_unfiltered[int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2, label="sum square 1")
ax[2, 1].legend()

ax[3, 1].plot(time, signal[29][int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2)
ax[3, 1].plot(time, signal[29][int(start*sampling_rate):int(stop*sampling_rate)]-ref_ratio_unfiltered[int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2, label="ratio")
ax[3, 1].legend()

ax[4, 1].plot(time, ref_mean_unfiltered[int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2, label="mean")
ax[4, 1].plot(time, ref_w_unfiltered[int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2, label="w")
ax[4, 1].plot(time, ref_ratio_unfiltered[int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2, label="ratio")
ax[4, 1].plot(time, ref_ev_unfiltered[int(start*sampling_rate):int(stop*sampling_rate)], linewidth=0.2, label="ev")
ax[4, 1].legend()

name = "test_fix_mean_tf_sum_equals_1_minimization_min_square_all_files_" + subject_id + "_nb_windows_" + str(nb_windows)
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((60, 20), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
