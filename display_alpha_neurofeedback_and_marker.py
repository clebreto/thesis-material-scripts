import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

marker_path = "/home/clebreto/Data/Neurofeedback/2021_04_21/2021_04_22_13_59_31__ztrMarkerOneChunkPower_type_Legendre_order_2_center_10_bandwidth_6_1.edf"
file_path = "/home/clebreto/Data/Neurofeedback/2021_04_21/2021_04_22_13_59_31_neurofeedback_clean_bandit_0001.edf"

marker_path = "/user/clebreto/home/Documents/test/2021_04_29_18_54_33__ztrMarkerOneChunkPower_type_Legendre_order_2_center_10_bandwidth_6_1.edf"
file_path = "/user/clebreto/home/Documents/test/2021_04_29_18_54_32__0.edf"

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(marker_path)
reader_edf_file.run()
marker = reader_edf_file.output()
marker_sr = reader_edf_file.samplingRate()
marker_channels  = np.array(reader_edf_file.channels())
print(marker_channels)
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(file_path)
reader_edf_file.run()
data = reader_edf_file.output()
data_sr = reader_edf_file.samplingRate()
data_channels  = np.array(reader_edf_file.channels())

bp = ztrcore.ztrProcessFilterBandPassOffline()
bp.setBandwidth(6.)
bp.setFrequency(10.)
bp.setOrder(4)
bp.setSamplingRate(data_sr)
bp.setSignal(data)
bp.run()
data = bp.output()

f = np.abs(np.copy(bp.output()))
duration_marker =  marker.shape[1] / marker_sr
duration_data   =  data.shape[1] / data_sr

time_marker = np.linspace(0, duration_marker, marker.shape[1])
time_data = np.linspace(0, duration_data, data.shape[1])

fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel('time (s)')
ax1.set_ylabel('marker', color=color)
ax1.plot(time_marker, marker[0], color=color)
ax1.set_ylim(0., 400.)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('sin', color=color)  # we already handled the x-label with ax1
ax2.plot(time_data, data[0], color=color)
ax2.tick_params(axis='y', labelcolor=color)
ax1.set_ylim(-400., 400.)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()


name = "display_alpha_neurofeedback_and_marker"
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
