import numpy as np
import matplotlib.pyplot as plt
from scipy import stats, optimize, interpolate
import pathlib as pl
import ztrcore
import shutil
import pandas as pd

## To load dynamic zither plugins
ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def nb_epochs_per_subject(files) :
    epochs = {}
    for f in files :
        subject = list(f.parts)[-2]
        if subject in epochs :
            epochs[subject] += 1
        else :
            epochs[subject] = 1
    return epochs

def measure(columns, group, files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection) :

    df = pd.DataFrame(columns=columns)

    short_range_pairs_indices = np.nonzero(np.triu(short_range_neighbors))
    short_range_nb_pairs = len(short_range_neighbors[short_range_pairs_indices])
    long_range_pairs_indices = np.nonzero(np.triu(long_range_neighbors))
    long_range_nb_pairs = len(long_range_neighbors[long_range_pairs_indices])

    short_range_output = np.empty(shape=(short_range_nb_pairs, frequencies.shape[0], 0))
    long_range_output = np.empty(shape=(long_range_nb_pairs, frequencies.shape[0], 0))

    for f in files :
        reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
        reader_edf_file.setFilePath(str(f))
        reader_edf_file.run()
        data          = reader_edf_file.output()
        sampling_rate = reader_edf_file.samplingRate()
        channels      = reader_edf_file.channels() # C x T (real)

        channels_selector = ztrcore.ztrProcessChannelsSelector()
        channels_selector.setChannels(channels)
        channels_selector.setSignal(data)
        channels_selector.setSelection(selection)
        channels_selector.run()
        selected_data   = channels_selector.output() # C x T (real)
        selected_channels = channels_selector.outputChannels()

        nb_channels = selected_data.shape[0]
        nb_values   = selected_data.shape[1]
        duration  = nb_values / sampling_rate

        filter_band_stop = ztrcore.ztrProcessFilterBandStopOffline()
        filter_band_stop.setBandwidth(7);
        filter_band_stop.setFrequency(50);
        filter_band_stop.setSamplingRate(sampling_rate);
        filter_band_stop.setSignal(selected_data);
        filter_band_stop.setOrder(6);
        filter_band_stop.run();
        filter_band_stop_output = filter_band_stop.output() # C x T (real)

        # Filter the signal on several frequency bands (center_freq + bandwidth)
        filter_bank = ztrcore.ztrProcessFilterBankOffline()
        filter_bank.setFrequencies(frequencies);
        filter_bank.setBandwidths(bandwidths);
        filter_bank.setSamplingRate(sampling_rate);
        filter_bank.setSignal(filter_band_stop_output);
        filter_bank.setOrder(6);
        filter_bank.run();
        filter_bank_output = filter_bank.output() # C x F x T (real)

        # Compute for each frequency band the analytic signal
        hilbert_bank = ztrcore.ztrProcessHilbertBank()
        hilbert_bank.setSamplingRate(sampling_rate);
        hilbert_bank.setSignal(filter_bank_output);
        hilbert_bank.run();
        hilbert_bank_output = hilbert_bank.output(); # C x F x T (complex)

        # Compute the synchrony measure on windows
        tfwt = ztrcore.ztrProcessTFWindowT(measure_name, hilbert_bank_output, sampling_rate, window_length, window_overlapping)
        tfwt.run()
        tfwt_output = tfwt.output() # C x C x F x T (real)

        nb_channels    = tfwt_output.shape[0];
        nb_frequencies = tfwt_output.shape[2];
        nb_windows     = tfwt_output.shape[3];

        temp_short_range_output = np.empty(shape=(short_range_nb_pairs, nb_frequencies, nb_windows))
        temp_long_range_output  = np.empty(shape=(long_range_nb_pairs, nb_frequencies, nb_windows))
        #df = pd.DataFrame(columns=['Measure Name', 'Group', 'Subject', 'Recording', 'Frequency', 'Pair', 'Value'])
        s_k = 0
        l_k = 0
        for i in range(nb_channels) :
            for j in range(i + 1, nb_channels) :
                if short_range_neighbors[i, j] == 1 :
                    for f_i in range(nb_frequencies) :
                           for w_i in range(nb_windows) :
                               df.loc[len(df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], "Short", tfwt_output[i, j, f_i, w_i]]
                if long_range_neighbors[i, j] == 1 :
                    for f_i in range(nb_frequencies) :
                        for w_i in range(nb_windows) :
                            df.loc[len(df.index)] = [measure_name, group, list(f.parts)[-2], str(f), frequencies[f_i], "Long", tfwt_output[i, j, f_i, w_i]]
        print("df.shape", df.shape)
    return df

def pltTableFiles(ax, nb_epochs_per_subject) :
    table = ax.table(cellText=[[v] for v in nb_epochs_per_subject.values()],
                 rowLabels=[k for k in nb_epochs_per_subject.keys()],
                 loc='bottom')
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    cell_dict = table.get_celld()
    for c in cell_dict.values():
        c.set_width(0.3)
        c.set_height(0.2)

# Parameters
frequencies = np.array([2.5, 5.5, 8.5, 11.5, 15.5, 24., 50.])
bandwidths = np.array([3., 3., 3., 3., 5., 12., 40.])

measure_name       = "Plv"
window_length      = 1.
window_overlapping = 0.

short_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/short_range.npy")
long_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/long_range.npy")

selection = ztrcore.readSelection("/home/clebreto/programming/come/zither/resources/selection/16battacharya.sel")

recordings = pl.Path("/home/clebreto/Data/Nice/")

condition = "yf"

condition_name = ""
if condition =="yo" :
    condition_name = "Eyes Open"
else :
    condition_name = "Eyes Closed"

A_files = np.array(list(recordings.glob("./Temoins/temoin_*/*" + condition + "*.edf")))
B_files = np.array(list(recordings.glob("./Epi_ZE_connue_SP/*/*" + condition + "*.edf")))

# Ignore patients looking like outliers
to_ignore = ["e224033c80", "7f4499ef3e", "e5cb01cc3a", "53fdbcb694", "f8766225fd", "8f91885dd8", "221150004d", "f6491b9dd4"]
B_files = np.array([b for b in B_files if (b.parts[-2]) not in to_ignore])

A_files = np.random.choice(A_files, size=10, replace=False)
B_files = np.random.choice(B_files, size=10, replace=False)

columns = ['Measure Name', 'Group', 'Subject', 'Recording', 'Frequency', 'Pair', 'Value']
df_control = measure(columns, "Control", A_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection)
df_epileptic = measure(columns, "Epileptic", B_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection)

df = pd.concat([df_control, df_epileptic], ignore_index=True)
name = "glm_ignore_outliers_bhattacharya_" + condition + "_all_freqs_" + measure_name + "_wl_" + str(window_length) + "_wo_" + str(window_overlapping)
shutil.copy("impact.py", name + ".py")
df.to_csv(name + ".csv")

from glm.glm import GLM
from glm.families import Gaussian, Bernoulli, Poisson, Exponential
linear_model = GLM(family=Gaussian())
#linear_model.fit(df, formula="Value ~ Group + Subject + Recording + Frequency + Pair")
linear_model.fit(df, formula="Value ~ Group + Subject + Frequency")
linear_model.summary()
print(linear_model.coef_, linear_model.coef_covariance_matrix_, linear_model.coef_standard_error_,linear_model.p_values_)
