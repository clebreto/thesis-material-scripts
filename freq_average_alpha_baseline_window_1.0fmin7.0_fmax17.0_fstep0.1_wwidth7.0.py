import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2020_11_24/2020_11_24_16_27_27_0001_alpha.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()[:32]
sampling_rate = reader_edf_file.samplingRate()
channels      = reader_edf_file.channels()[:32]
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events_reader_output = events_reader.output()

fmin = 7.
fmax = 17.
fstep = .1
nb_frequencies = int((fmax - fmin) / fstep)

width = 7.
window_length = 1. # seconds
window_size = window_length * sampling_rate

nb_windows = int(nb_values / window_size)
wavelet_fourier_output = np.empty(shape=(nb_channels, nb_frequencies, 0))
## Computes the wavelet coeficients with the wavelet trasnform by window
for w_i in range(nb_windows - 1) :
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(np.arange(fmin, fmax, fstep))
    wavelet_fourier.setSignal(signal[:, int(w_i * window_size) : int((w_i + 1) * window_size)])
    wavelet_fourier.setWidth(width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wavelet_fourier_output = np.concatenate((wavelet_fourier_output, np.copy(wavelet_fourier.output())), axis=(2))

time = np.linspace(0., duration, wavelet_fourier_output.shape[2])
wavelet_fourier_output_events = {"baseline":np.empty(shape=(wavelet_fourier_output.shape[0], wavelet_fourier_output.shape[1], 0)), "alpha":np.empty(shape=(wavelet_fourier_output.shape[0], wavelet_fourier_output.shape[1], 0))}
skip = False
for e_i in range(len(events_reader_output)) :
    if skip :
        skip = False
        continue
    indices = (time > events_reader_output[e_i].timestamp / 1000.) & (time < events_reader_output[e_i + 1].timestamp / 1000.)
    wavelet_fourier_output_events[events_reader_output[e_i].uid] = np.concatenate((wavelet_fourier_output_events[events_reader_output[e_i].uid], wavelet_fourier_output[:, :, indices]), axis=(2))
    skip = True

plt.clf()
freq_mean = np.mean(np.abs(wavelet_fourier_output_events["alpha"]), axis=(0, 2))
freq_std = np.std(np.abs(wavelet_fourier_output_events["alpha"]), axis=(0, 2))
x = np.linspace(fmin, fmax, int((fmax-fmin) / fstep))
plt.plot(x, freq_mean, label="alpha")
plt.fill_between(x, freq_mean - freq_std, freq_mean + freq_std, alpha=0.1)
freq_mean = np.mean(np.abs(wavelet_fourier_output_events["baseline"]), axis=(0, 2))
freq_std = np.std(np.abs(wavelet_fourier_output_events["baseline"]), axis=(0, 2))
plt.plot(x, freq_mean, label="baseline")
plt.fill_between(x, freq_mean - freq_std, freq_mean + freq_std, alpha=0.1)
plt.legend()

name = "freq_average_alpha_baseline_window_" + str(window_length) + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(width)
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
