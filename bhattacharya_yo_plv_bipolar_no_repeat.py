import numpy as np
import matplotlib.pyplot as plt
from scipy import stats, optimize, interpolate
import sys, os, shutil
import pathlib as pl
import ztrcore

## To load dynamic zither plugins
ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()


montage = [["Fp1", "Fp2"], ["F4", "F8"], ["C4", "T4"], ["P4", "T6"], ["O1", "O2"], ["T5", "P3"], ["T3", "C3"], ["F7", "F3"]]

short_range_neighbors = np.array([[0, 1, 0, 0, 0, 0, 0, 1],
                                  [1, 0, 1, 0, 0, 0, 0, 0],
                                  [0, 1, 0, 1, 0, 0, 0, 0],
                                  [0, 0, 1, 0, 1, 0, 0, 0],
                                  [0, 0, 0, 1, 0, 1, 0, 0],
                                  [0, 0, 0, 0, 1, 0, 1, 0],
                                  [0, 0, 0, 0, 0, 1, 0, 1],
                                  [1, 0, 0, 0, 0, 0, 1, 0]])

long_range_neighbors = np.array([[0, 0, 1, 1, 1, 1, 1, 0],
                                 [0, 0, 0, 1, 1, 1, 1, 1],
                                 [1, 0, 0, 0, 1, 1, 1, 1],
                                 [1, 1, 0, 0, 0, 1, 1, 1],
                                 [1, 1, 1, 0, 0, 0, 1, 1],
                                 [1, 1, 1, 1, 0, 0, 0, 1],
                                 [1, 1, 1, 1, 1, 0, 0, 0],
                                 [0, 1, 1, 1, 1, 1, 0, 0]])

def measure(files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, montage) :
    short_range_pairs_indices = np.nonzero(np.triu(short_range_neighbors))
    short_range_nb_pairs = len(short_range_neighbors[short_range_pairs_indices])
    long_range_pairs_indices = np.nonzero(np.triu(long_range_neighbors))
    long_range_nb_pairs = len(long_range_neighbors[long_range_pairs_indices])

    short_range_output = np.empty(shape=(short_range_nb_pairs, frequencies.shape[0], 0))
    long_range_output = np.empty(shape=(long_range_nb_pairs, frequencies.shape[0], 0))

    for f in files :
        reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
        reader_edf_file.setFilePath(str(f))
        reader_edf_file.run()
        data          = reader_edf_file.output()
        sampling_rate = reader_edf_file.samplingRate()
        channels      = np.array(reader_edf_file.channels()) # C x T (real)

        montage_indices = []
        for m in montage :
            montage_indices.append([np.where(channels == m[0])[0][0], np.where(channels == m[1])[0][0]])
        montage_indices = np.array(montage_indices)

        data = data[montage_indices[:, 0]] - data[montage_indices[:, 1]]
        channels = montage

        nb_channels = data.shape[0]
        nb_values   = data.shape[1]
        duration  = nb_values / sampling_rate

        # Filter the signal on several frequency bands (center_freq + bandwidth)
        filter_bank = ztrcore.ztrProcessFilterBankOffline()
        filter_bank.setFrequencies(frequencies);
        filter_bank.setBandwidths(bandwidths);
        filter_bank.setSamplingRate(sampling_rate);
        filter_bank.setSignal(data);
        filter_bank.setOrder(6);
        filter_bank.run();
        filter_bank_output = filter_bank.output() # C x F x T (real)

        # Compute for each frequency band the analytic signal
        hilbert_bank = ztrcore.ztrProcessHilbertBank()
        hilbert_bank.setSamplingRate(sampling_rate);
        hilbert_bank.setSignal(filter_bank_output);
        hilbert_bank.run();
        hilbert_bank_output = hilbert_bank.output(); # C x F x T (complex)

        # Compute the synchrony measure on windows
        tfwt = ztrcore.ztrProcessTFWindowT(measure_name, hilbert_bank_output, sampling_rate, window_length, window_overlapping)
        tfwt.run()
        tfwt_output = tfwt.output() # C x C x F x T (real)

        nb_channels    = tfwt_output.shape[0];
        nb_frequencies = tfwt_output.shape[2];
        nb_windows     = tfwt_output.shape[3];

        temp_short_range_output = np.empty(shape=(short_range_nb_pairs, nb_frequencies, nb_windows))
        temp_long_range_output  = np.empty(shape=(long_range_nb_pairs, nb_frequencies, nb_windows))

        s_k = 0
        l_k = 0
        for i in range(nb_channels) :
            for j in range(i + 1, nb_channels) :
                if short_range_neighbors[i, j] == 1 :
                    temp_short_range_output[s_k, :, :] = tfwt_output[i, j, :, :]
                    s_k += 1
                if long_range_neighbors[i, j] == 1 :
                    temp_long_range_output[l_k, :, :] = tfwt_output[i, j, :, :]
                    l_k += 1

        short_range_output = np.concatenate((short_range_output, temp_short_range_output), axis=2)
        long_range_output = np.concatenate((long_range_output, temp_long_range_output), axis=2)
    return (short_range_output, long_range_output)

#epileptiques A, temoins B
def pltCompareManwhitneyU(ax, A, B, title=None, A_name="", B_name="") :
    nb_pairs = A.shape[0]
    nb_frequencies = A.shape[1]

    manwhitneyu_A = np.zeros((nb_frequencies))
    for f in range(nb_frequencies) :
        for c in range(nb_pairs) :
            if(stats.mannwhitneyu(A[c, f, :], B[c, f, :], use_continuity=False, alternative='greater').pvalue < 0.05) :
                manwhitneyu_A[f] += 1

    manwhitneyu_B = np.zeros((nb_frequencies))
    for f in range(nb_frequencies) :
        for c in range(nb_pairs) :
            if(stats.mannwhitneyu(B[c, f, :], A[c, f, :], use_continuity=False, alternative='greater').pvalue < 0.05) :
                manwhitneyu_B[f] += 1

    manwhitneyu_A /= nb_pairs
    manwhitneyu_B /= nb_pairs

    x = np.arange(nb_frequencies)
    width = 0.35

    ax.bar(x - width / 2, manwhitneyu_B, width, label=B_name, color="black", edgecolor="black")
    ax.bar(x + width / 2, manwhitneyu_A, width, label=A_name, color="white", edgecolor="black")
    ax.set_xticks(x)
    ax.set_xticklabels(["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"])
    ax.legend()
    if title != None :
        ax.set_title(title)

def pltCompareHistograms(ax, A, B, title=None, title_position=None, A_name="", B_name="") :
    ax.hist(A[:,:,:].flatten(), alpha = 0.5, label = A_name, density = True)
    ax.hist(B[:,:,:].flatten(), alpha = 0.5, label = B_name, density = True)
    if title != None :
        if title_position == 'left' :
            ax.set_title(title, rotation='vertical', x=-0., y=0.)
        else :
            ax.set_title(title)
    ax.legend()

def pltCompareHistogramsFreq(ax, A, B, f, title=None, title_position=None, A_name="", B_name="") :
    ax.hist(A[:,f,:].flatten(), alpha = 0.5, label = A_name, density = True)
    ax.hist(B[:,f,:].flatten(), alpha = 0.5, label = B_name, density = True)
    if title != None :
        if title_position == 'left' :
            ax.set_title(title, rotation='vertical', x=-0., y=0.)
        else :
            ax.set_title(title)
    ax.legend()

# Parameters
frequencies = np.array([2.5, 5.5, 8.5, 11.5, 15.5, 24., 50.])
bandwidths = np.array([3., 3., 3., 3., 5., 12., 40.])
frequencies_names = ["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"]

measure_name       = "Plv"
window_length      = 8.
window_overlapping = 7.

recordings = pl.Path("/home/clebreto/Data/Nice/")

A_files = np.array(list(recordings.glob("./Epi_ZE_connue_SP/*/*yo*.edf")))
B_files = np.array(list(recordings.glob("./Temoins/temoin_*/*_yo*.edf")))

(A_short_range_output, A_long_range_output) = measure(A_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, montage)
(B_short_range_output, B_long_range_output) = measure(B_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, montage)

nb_frequencies = len(frequencies)

plt.clf()
fg, ax = plt.subplots(nb_frequencies + 2, 2, figsize=(15.5, 12.5))
print(A_long_range_output.shape)
pltCompareManwhitneyU(ax[0, 0], A_short_range_output, B_short_range_output, title="Short range", A_name="Seizure", B_name="Control")
pltCompareManwhitneyU(ax[0, 1], A_long_range_output, B_long_range_output, title="Long range", A_name="Seizure", B_name="Control")

pltCompareHistograms(ax[1, 0], A_short_range_output, B_short_range_output, title="Mixed", title_position='left', A_name="Seizure", B_name="Control")
pltCompareHistograms(ax[1, 1], A_long_range_output, B_long_range_output, A_name="Seizure", B_name="Control")

for f in range(nb_frequencies) :
    pltCompareHistogramsFreq(ax[2 + f, 0], A_short_range_output, B_short_range_output, f, title=frequencies_names[f], title_position='left', A_name="Seizure", B_name="Control")
    pltCompareHistogramsFreq(ax[2 + f, 1], A_long_range_output, B_long_range_output, f, A_name="Seizure", B_name="Control")

fg.suptitle("Eyes Open, PLV, Control nb windows : " + str(A_short_range_output.shape[2]) + " Epileptic nb windows :" + str(B_short_range_output.shape[2]))

plt.tight_layout()

name = "bhattacharya_yo_plv_bipolar_no_repeat"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 12.5), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
