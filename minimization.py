import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")


## Channel selection
channels_selection = np.array(['O2', 'P4', 'C4', 'F4', 'Fp2'])

## Wavelet Transform
fmin = 9.
fmax = 11
fstep = 0.5

wavelet_width = 12.

subject_id = "0001"

files = recordings.glob("./*/*" + subject_id + "_alpha.edf")

f = next(files)

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(f))
reader_edf_file.run()

signal        = reader_edf_file.output()[:, :]
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels()[:])

# Selection
# indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
# signal = signal[indices_selection]
# channels = channels[indices_selection]

nb_values = signal.shape[1]
duration = nb_values / sampling_rate

bp = ztrcore.ztrProcessFilterBandPassOffline()
bp.setBandwidth(3.)
bp.setFrequency(10.)
bp.setOrder(4)
bp.setSamplingRate(sampling_rate)
bp.setSignal(signal)
bp.run()
signal_filtered = bp.output()[:, 3*512: -3*512]

# Solution 1
M = signal_filtered
n = M.shape[0]
H = M.dot(M.T)

K = H[0:-1,0:-1]
h = H[-1,0:-1]
a = H[-1,-1]
o = np.ones((n-1))

Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
B = a*o-h

x = np.linalg.solve(Q,B)
w = np.append(x,1-np.dot(x,o))

# Solution 2
print(np.linalg.eig(H))

mix = np.dot(w,M)
plt.clf()
fg, ax = plt.subplots(2, 1)
ax[0].plot(signal_filtered[0], label=channels[0])
ax[0].plot(signal_filtered[4], label=channels[4])
ax[0].plot(mix, label="Mix")
ax[0].legend()

table = ax[1].table(cellText=[[v] for v in w],
                    rowLabels=[k for k in channels],
                    loc='center')
ax[1].get_xaxis().set_visible(False)
ax[1].get_yaxis().set_visible(False)
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].spines['bottom'].set_visible(False)
ax[1].spines['left'].set_visible(False)

cell_dict = table.get_celld()
for c in cell_dict.values():
    c.set_width(0.3)
    c.set_height(0.1)

name = "minimization"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 10), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
