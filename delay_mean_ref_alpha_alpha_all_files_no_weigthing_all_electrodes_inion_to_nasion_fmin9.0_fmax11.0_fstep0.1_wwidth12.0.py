import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.cm as cm
import matplotlib.colors as clrs
from mpl_toolkits.axes_grid1 import make_axes_locatable

import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

# Compute the a Delaunay triangulation to get close neighbors
headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")
positions = headset.positions()
labels    = np.array(headset.labels())
electrodes_positions = dict(zip(labels, positions))

x = [positions[x, 0] for x in range(len(positions))]
y = [positions[y, 1] for y in range(len(positions))]

triangulation = tri.Triangulation(x, y)

pairs_labels_in_headset = [["O1", "P3"], ["P3", "C3"], ["C3", "F3"], ["F3", "Fp1"], ["Oz", "POz"], ["POz", "Pz"], ["Pz", "Cz"], ["Cz", "Fz"], ["Fz", "Fpz"], ["O2", "P4"], ["P4", "C4"], ["C4", "F4"], ["F4", "Fp2"], ["P7", "T7"], ["T7", "F7"], ["P8", "T8"], ["T8", "F8"]]

pairs_labels_in_signal = [["O1", "P3"], ["P3", "C3"], ["C3", "F3"], ["F3", "Fp1"], ["Oz", "POz"], ["POz", "Pz"], ["Pz", "Cz"], ["Cz", "Fz"], ["Fz", "Fpz"], ["O2", "P4"], ["P4", "C4"], ["C4", "F4"], ["F4", "Fp2"], ["P7", "T7"], ["T7", "F7"], ["P8", "T8"], ["T8", "F8"]]

pairs_indices_in_headset = []
for p in pairs_labels_in_headset :
    pairs_indices_in_headset.append([np.where(labels == c)[0][0] for c in p])
nb_pairs = len(pairs_labels_in_headset)

pairs_x = []
pairs_y = []
for p_i in range(nb_pairs):
    pairs_x.append((positions[pairs_indices_in_headset[p_i][0]][0] + positions[pairs_indices_in_headset[p_i][1]][0]) / 2.)
    pairs_y.append((positions[pairs_indices_in_headset[p_i][0]][1] + positions[pairs_indices_in_headset[p_i][1]][1]) / 2.)

pairs_triangulation = tri.Triangulation(pairs_x, pairs_y)

## Wavelet Transform
fmin = 9.
fmax = 11.
fstep = 0.1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events = events_reader.output()

files = sorted(recordings.glob("./2020_11_*/*01_alpha.edf"))
files = [f for f in files]
nb_files = len(files)

l = int(np.ceil(np.sqrt(nb_files)))
c = int(np.ceil(nb_files / l))
plt.clf()
fig, ax = plt.subplots(l, c)

i = 0
skip = False
for f in files :
    print(str(f))
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = np.copy(reader_edf_file.output(), order='C')[:32, :] # 33 is GSR
    signal = signal - np.mean(signal, axis=0)
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    pairs_indices_in_signal = []
    for p in pairs_labels_in_signal :
        pairs_indices_in_signal.append([np.where(channels == c)[0][0] for c in p])

    nb_channels = len(channels)

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output(), order='C')

    ## Exttract the amplitude and the phase
    amplitude = np.abs(wt)
    phase = np.angle(wt)

    window_length = 2. # seconds
    window_size = int(window_length * sampling_rate)
    nb_windows = int(nb_values / window_size)

    phase_diff_alpha  = np.zeros(shape=(nb_pairs))
    nb_alpha_events = 0
    for e_i in range(len(events)) :
        if events[e_i].uid == 'alpha' :
            if skip :
                skip = False
                continue
            phase_event = phase[:, :, int(events[e_i].timestamp / 1000. * sampling_rate) : int(events[e_i + 1].timestamp / 1000. * sampling_rate)]
            print(phase_event.data.contiguous)
            phase_diff_event = np.empty(shape=(nb_pairs, phase_event.shape[1], phase_event.shape[2]))
            for p_i in range(nb_pairs) :
                phase_diff_event[p_i] = phase_event[pairs_indices_in_signal[p_i][1]] - phase_event[pairs_indices_in_signal[p_i][0]]
            ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
            phase_diff_event = np.where(phase_diff_event > np.pi, phase_diff_event - 2 * np.pi, phase_diff_event)
            phase_diff_event = np.where(phase_diff_event < -np.pi, phase_diff_event + 2 * np.pi, phase_diff_event)

            nb_alpha_events += 1

            phase_diff_alpha += np.mean(phase_diff_event, axis=(1, 2))

            skip = True

    delay_alpha = phase_diff_alpha / nb_alpha_events

    colo = ax[int(np.floor(i / c)), i % c].tricontourf(pairs_triangulation, delay_alpha, cmap=cm.RdBu, norm=clrs.Normalize(vmin=-np.pi/2., vmax=np.pi / 2.))
    for ep in electrodes_positions.items() :
        ax[int(np.floor(i / c)), i % c].annotate(ep[0], xy=(ep[1][0], ep[1][1]))
    ax[int(np.floor(i / c)), i % c].scatter(pairs_x, pairs_y, color='red', s=1)
    ax[int(np.floor(i / c)), i % c].scatter(x, y, color='black', s=1)
    ax[int(np.floor(i / c)), i % c].set_title(f.stem)
    divider = make_axes_locatable(ax[int(np.floor(i / c)), i % c])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(cm.ScalarMappable(cmap=cm.RdBu, norm=clrs.Normalize(vmin=-np.pi/2., vmax=np.pi/2.)), cax=cax, orientation='vertical')

    i += 1

name = "delay_mean_ref_alpha_alpha_all_files_no_weigthing_all_electrodes_inion_to_nasion_" + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((15.5, 12.5), forward=False)
fig.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
