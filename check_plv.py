import ztrcore

import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba
import matplotlib

import scipy as sc
import scipy.ndimage.filters as filters
import scipy.signal
from scipy.spatial import distance_matrix

import copy

from tqdm import tqdm

ztrcore.initialize()

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

unwrap_phase_1 = 2 * np.pi * 1 * np.linspace(0., 5., 5 * 512) + np.random.rand(5*512)
unwrap_phase_2 = (2 * np.pi * 1 * np.linspace(0., 5., 5 * 512) + np.pi / 2.) + np.random.rand(5*512)

phase_1 = unwrap_phase_1 % (2 * np.pi)
phase_2 = unwrap_phase_2 % (2 * np.pi)

diff_phase = phase_2 - phase_1
diff_unwrap_phase = unwrap_phase_2 - unwrap_phase_1

fix_diff_phase = np.where(diff_phase < - np.pi, diff_phase + 2. * np.pi, diff_phase)
fix_diff_phase = np.where(fix_diff_phase > np.pi, fix_diff_phase - 2. * np.pi, fix_diff_phase)

plt.clf()
fg, ax = plt.subplots(7, 1)
ax[0].plot(unwrap_phase_1, color='blue', label="2 * np.pi * 1 * np.linspace(0., 10., 10 * 512)")
ax[0].plot(unwrap_phase_2, color='red', label="2 * np.pi * 1 * np.linspace(0., 10., 10 * 512) + np.pi / 2.")
ax[0].legend()
ax[1].scatter(unwrap_phase_1, unwrap_phase_2)
ax[2].plot(diff_unwrap_phase, color='green', label="unwraped phase_difference")

ax[3].plot(phase_1, color='blue', label="2 * np.pi * 1 * np.linspace(0., 10., 10 * 512) % 2. * np.pi")
ax[3].plot(phase_2, color='red', label="(2 * np.pi * 1 * np.linspace(0., 10., 10 * 512) + np.pi / 2.) % 2. * np.pi")
ax[3].legend()
ax[4].scatter(phase_1, phase_2)
ax[5].plot(diff_phase, color='green', label="phase difference")
ax[6].plot(fix_diff_phase, color='green', label="fixed phase difference")

name = "check_plv"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((20, 60), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
