import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
import mne

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

channels_selection = ['P3', 'F7', 'F5', 'F3', 'F1', 'F2', 'F4', 'F6', 'AF3', 'AFz', 'AF4', 'Fp1', 'Fp2']

channel = 'F7'
channel_index = channels_selection.index(channel)

file_path_root = "/user/clebreto/home/Data/Hackathlon/P03/S2/eeg/alldata_sbj03_sess2_"
conditions = ['RS', 'MATBeasy', 'MATBmed', 'MATBdiff']
conditions_colors = ['blue', 'blue', 'pink', 'pink', 'orange', 'orange', 'red', 'red']

durations = []
signals = []
for c in conditions :
    file_path = file_path_root + c + ".set"
    epochs = mne.io.read_epochs_eeglab(file_path, verbose=False)  # FIR 1-40 Hz
    epochs = epochs.pick_channels(channels_selection)
    data = epochs.get_data()
    signal = np.zeros(shape=(data.shape[1], data.shape[2]*data.shape[0]))
    sampling_rate = 250.
    durations.append(signal.shape[1] / sampling_rate)

    for e in range(data.shape[0]) :
        signal[:, e * data.shape[2] : (e +1) * data.shape[2]] = data[e]

    signals.append(signal)

signal = np.hstack(signals)

sampling_rate = 250.

nb_values = signal.shape[1]
duration = nb_values / sampling_rate
print(duration)

plt.clf()
fig, ax = plt.subplots(1, 1)

time = np.linspace(0, duration, nb_values)
frequencies = np.arange(5, 70, 1.)

class Event() :
    def __init__(self, uid, timestamp):
        self.uid = uid
        self.timestamp = timestamp

events=[]
start = 0
events.append(Event(conditions[0], start))
events.append(Event(conditions[0], start))
start += durations[0]
for i in range(1, len(conditions)) :
    events.append(Event(conditions[i], start * 1000))
    events.append(Event(conditions[i], start * 1000))
    start+=durations[i]

ztrplotting.plotTFAmplitudes(ax, time, frequencies, signal[channel_index], w_width=13, events=events, event=conditions, colors=conditions_colors, channel_name=channel)

name = "TF_" + channel + file_path_root.split("/")[-1]
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((90., 20.), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
