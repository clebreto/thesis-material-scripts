import numpy as np
import ztrcore
import matplotlib.pyplot as plt

def morletWavelet(time, f, nb_cycles) :
    s = nb_cycles / (2. * np.pi * f)
    a = - (time * time) / (2. * s * s)
    return 1. / np.sqrt(s * np.pi) * np.exp(a) * np.exp(1j * 2. * np.pi * f * time)

def morletWaveletConvolution(signal, frequencies, sampling_rate, wavelet_width = 7.) :

    assert len(signal.shape) == 2

    output  = np.zeros(shape=(signal.shape[0], frequencies.shape[0], signal.shape[1]), dtype=np.complex_)
    signal = np.hstack((signal, np.flip(signal, axis=1)))
    fft_signal = np.fft.fft(signal)

    duration = signal.shape[1] / sampling_rate
    time = np.linspace(-duration / 2., duration / 2., signal.shape[1])

    for f in range(len(frequencies)) :
        fft_kernel = np.fft.fft(morletWavelet(time, frequencies[f], wavelet_width))
        for c in range(signal.shape[0]) :
            output[c, f, :] = np.fft.ifft(fft_kernel * fft_signal[c])[signal.shape[1] - output.shape[2] : signal.shape[1]]

    return output

def compute(noise) :
    sampling_rate = 512
    start = 0
    stop = 100
    duration = stop - start
    nb_values = sampling_rate * duration

    time = np.linspace(start, stop, sampling_rate * duration)

    f0 = 10
    w0 = 2. * np.pi * f0

    f1 = 13
    w1 = 2. * np.pi * f1

    signal_a = np.hstack((np.cos(w0 * time[:int(1/3*nb_values)]),
                          np.cos(w1 * time[int(1/3*nb_values) : int(2/3*nb_values)]),
                          np.cos(w0 * time[int(2/3*nb_values):])))
    signal_a += noise * 2. * (np.random.random(signal_a.shape) - 0.5)

    signal_b = np.hstack((np.cos(w1 * time[:int(1/3*nb_values)]),
                          np.cos(w1 * time[int(1/3*nb_values) : int(2/3*nb_values)] + np.pi * 1/4),
                          np.cos(w1 * time[int(2/3*nb_values):])))
    signal_b += noise * 2. * (np.random.random(signal_a.shape) - 0.5)

    signal = np.vstack((signal_a, signal_b))

    wavelet_frequencies = np.arange(3, 23, 0.1)

    wavelet_output = morletWaveletConvolution(signal, wavelet_frequencies, sampling_rate, wavelet_width=6.)
    # plt.matshow(np.diff(np.angle(wavelet_output[0]), axis=1)[:, ::50]*sampling_rate/(2.*np.pi), vmin=00, vmax=20)
    # plt.show()

    window_length = 3.
    window_overlapping = 2.
    measure_name = "Plv"
    tfwt = ztrcore.ztrProcessTFWindowT(measure_name, wavelet_output, sampling_rate, window_length, window_overlapping)
    tfwt.run()
    tfwt_output = tfwt.output() # C x C x F x T (real)

    return tfwt_output[0, 1]

noises = np.arange(0., 10., 0.5)

fig, ax = plt.subplots(noises.shape[0], 2)
for n_i, n in enumerate(noises) :
    ft = compute(n)
    for f in range(ft.shape[0]) :
        ax[n_i, 0].plot(ft[f,:])
    ax[n_i, 1].plot(np.mean(ft, axis=0))
plt.show()

ft = compute(1.)
# for f in range(tfwt_output.shape[2]) :
#     plt.plot(tfwt_output[0,1][f])
# plt.show()

# Need to understand why this false synchrony in the beginning

# Need to create an example where there are "events" of synchronization lasting k seconds (for different values of k), and check the influence of the width of the wavelet and duration of the window on synchrony detection
