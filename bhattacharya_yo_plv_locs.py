import numpy as np
import matplotlib.pyplot as plt
from scipy import stats, optimize, interpolate
import sys, os, shutil
import pathlib as pl
import ztrcore

epi_locs = {"64fc24c729": "gauche",
        "6b19e32ead": "droite",
        "e817d2c6d9": "bilat",
        "1b5268a671": "droite",
        "acce9268a7": "gauche",
        "f8766225fd": "droite",
        "353815ea44": "gauche",
        "f329168a3c": "droite",
        "e5cb01cc3a": "gauche",
        "dd8905f979": "droite",
        "fb4e348625": "droite",
        "04c5b5c3ef": "gauche",
        "989f0aa488": "gauche",
        "ef5c2a4c56": "bilat",
        "8f91885dd8": "gauche",
        "76201bb427": "gauche",
        "02b0869b08": "droite",
        "e224033c80": "gauche",
        "f94d290a26": "gauche",
        "f6491b9dd4": "droite",
        "2fc02d6901": "droite"}

elec_locs = {"P3" : "gauche",
             "C3" : "gauche",
             "F3" : "gauche",
             "O1" : "gauche",
             "T5" : "gauche",
             "T3" : "gauche",
             "F7" : "gauche",
             "Fp1" : "gauche",
             "P4" : "droite",
             "C4" : "droite",
             "F4" : "droite",
             "O2" : "droite",
             "T6" : "droite",
             "T4" : "droite",
             "F8" : "droite",
             "Fp2" : "droite"}

##0fe96efe83     ELT/F bilat
##b164f591bd     ELT ? (absence d'anomalie EEG)

## To load dynamic zither plugins
ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def measure(files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection, montage) :
    short_range_pairs_indices = np.nonzero(np.triu(short_range_neighbors))
    short_range_nb_pairs = len(short_range_neighbors[short_range_pairs_indices])
    long_range_pairs_indices = np.nonzero(np.triu(long_range_neighbors))
    long_range_nb_pairs = len(long_range_neighbors[long_range_pairs_indices])

    short_range_output = {'ipsi' : [],
                          'contra' : [],
                          'cross' :  []}

    long_range_output = {'ipsi' : [],
                          'contra' : [],
                          'cross' :  []}

    for f in files :
        if os.path.basename(os.path.dirname(f)) not in epi_locs :
            print("not in epi_locs")
            continue

        epi_loc = epi_locs[os.path.basename(os.path.dirname(f))]

        if epi_loc != 'gauche' and epi_loc != 'droite' :
            print("epi_loc not gauche or droite ", epi_loc)
            continue

        reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
        reader_edf_file.setFilePath(str(f))
        reader_edf_file.run()
        data          = reader_edf_file.output()
        sampling_rate = reader_edf_file.samplingRate()
        channels      = reader_edf_file.channels() # C x T (real)

        channels_selector = ztrcore.ztrProcessChannelsSelector()
        channels_selector.setChannels(channels)
        channels_selector.setSignal(data)
        channels_selector.setSelection(selection)
        channels_selector.run()
        selected_data   = channels_selector.output() # C x T (real)
        selected_channels = channels_selector.outputChannels()

        nb_channels = selected_data.shape[0]
        nb_values   = selected_data.shape[1]
        duration  = nb_values / sampling_rate

        # Filter the signal on several frequency bands (center_freq + bandwidth)
        filter_bank = ztrcore.ztrProcessFilterBankOffline()
        filter_bank.setFrequencies(frequencies);
        filter_bank.setBandwidths(bandwidths);
        filter_bank.setSamplingRate(sampling_rate);
        filter_bank.setSignal(selected_data);
        filter_bank.setOrder(6);
        filter_bank.run();
        filter_bank_output = filter_bank.output() # C x F x T (real)

        # Compute for each frequency band the analytic signal
        hilbert_bank = ztrcore.ztrProcessHilbertBank()
        hilbert_bank.setSamplingRate(sampling_rate);
        hilbert_bank.setSignal(filter_bank_output);
        hilbert_bank.run();
        hilbert_bank_output = hilbert_bank.output(); # C x F x T (complex)

        # Compute the synchrony measure on windows
        tfwt = ztrcore.ztrProcessTFWindowT(measure_name, hilbert_bank_output, sampling_rate, window_length, window_overlapping)
        tfwt.run()
        tfwt_output = tfwt.output() # C x C x F x T (real)

        nb_channels    = tfwt_output.shape[0];
        nb_frequencies = tfwt_output.shape[2];
        nb_windows     = tfwt_output.shape[3];

        for i in range(nb_channels) :
            for j in range(i + 1, nb_channels) :
                if short_range_neighbors[i, j] == 1 :
                    if elec_locs[selected_channels[i]] == epi_loc  and elec_locs[selected_channels[j]] == epi_loc:
                        short_range_output['ipsi'].append(tfwt_output[i, j, :, :])
                    elif elec_locs[selected_channels[i]] != epi_loc and elec_locs[selected_channels[j]] != epi_loc:
                        short_range_output['contra'].append(tfwt_output[i, j, :, :])
                    else :
                        short_range_output['cross'].append(tfwt_output[i, j, :, :])
                if long_range_neighbors[i, j] == 1 :
                    if elec_locs[selected_channels[i]] == epi_loc  and elec_locs[selected_channels[j]] == epi_loc:
                        long_range_output['ipsi'].append(tfwt_output[i, j, :, :])
                    elif elec_locs[selected_channels[i]] != epi_loc and elec_locs[selected_channels[j]] != epi_loc:
                        long_range_output['contra'].append(tfwt_output[i, j, :, :])
                    else :
                        long_range_output['cross'].append(tfwt_output[i, j, :, :])

    return (short_range_output, long_range_output)

def pltCompareHistograms(ax, A, title=None, title_position=None) :
    ipsi_pairs = np.empty(shape=(0))
    for p in A['ipsi'] :
        ipsi_pairs = np.concatenate((ipsi_pairs, p.flatten()), axis=0)

    contra_pairs = np.empty(shape=(0))
    for p in A['contra'] :
        contra_pairs = np.concatenate((contra_pairs, p.flatten()), axis=0)

    cross_pairs = np.empty(shape=(0))
    for p in A['cross'] :
        cross_pairs = np.concatenate((cross_pairs, p.flatten()), axis=0)

    ax.hist(ipsi_pairs, alpha = 0.5, label = 'ipsi', density = True)
    ax.hist(contra_pairs, alpha = 0.5, label = 'contra', density = True)
    ax.hist(cross_pairs, alpha = 0.5, label = 'cross', density = True)

    if title != None :
        if title_position == 'left' :
            ax.set_title(title, rotation='vertical', x=-0., y=0.)
        else :
            ax.set_title(title)
    ax.legend()

def pltCompareHistogramsFreq(ax, A, f, title=None, title_position=None) :
    ipsi_pairs = np.empty(shape=(0))
    for p in A['ipsi'] :
        ipsi_pairs = np.concatenate((ipsi_pairs, p[f].flatten()), axis=0)

    contra_pairs = np.empty(shape=(0))
    for p in A['contra'] :
        contra_pairs = np.concatenate((contra_pairs, p[f].flatten()), axis=0)

    cross_pairs = np.empty(shape=(0))
    for p in A['cross'] :
        cross_pairs = np.concatenate((cross_pairs, p[f].flatten()), axis=0)

    ax.hist(ipsi_pairs, alpha = 0.5, label = 'ipsi', density = True)
    ax.hist(contra_pairs, alpha = 0.5, label = 'contra', density = True)
    ax.hist(cross_pairs, alpha = 0.5, label = 'cross', density = True)

    if title != None :
        if title_position == 'left' :
            ax.set_title(title, rotation='vertical', x=-0., y=0.)
        else :
            ax.set_title(title)
    ax.legend()
# Parameters
frequencies = np.array([2.5, 5.5, 8.5, 11.5, 15.5, 24., 50.])
bandwidths = np.array([3., 3., 3., 3., 5., 12., 40.])
frequencies_names = ["Delta", "Theta", "Alpha 1", "Alpha 2", "Beta 1", "Beta 2", "Gamma"]

measure_name       = "Plv"
window_length      = 8.
window_overlapping = 7.

short_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/short_range.npy")
long_range_neighbors = np.load("/home/clebreto/programming/come/zither/resources/long_range.npy")

selection = ztrcore.readSelection("/home/clebreto/programming/come/zither/resources/selection/16battacharya.sel")
montage   = ztrcore.readMontage("/home/clebreto/programming/come/zither/resources/montages/21Longitudinal.mtg")

recordings = pl.Path("/home/clebreto/Data/Nice/")

A_files = np.array(list(recordings.glob("./Epi_ZE_connue_SP/*/*yo*.edf")))

(A_short_range_output, A_long_range_output) = measure(A_files, measure_name, frequencies, bandwidths, window_length, window_overlapping, short_range_neighbors, long_range_neighbors, selection, montage)

nb_frequencies = len(frequencies)

plt.clf()
fg, ax = plt.subplots(nb_frequencies + 1, 2, figsize=(15.5, 12.5))

#pltCompareManwhitneyU(ax[0, 0], A_short_range_output, B_short_range_output, title="Short range", A_name="Seizure", B_name="Control")
pltCompareHistograms(ax[0, 0], A_short_range_output, title='Short range')
pltCompareHistograms(ax[0, 1], A_long_range_output, title='Long range')

for f in range(nb_frequencies) :
    pltCompareHistogramsFreq(ax[1 + f, 0], A_short_range_output, f, title=frequencies_names[f], title_position='left')
    pltCompareHistogramsFreq(ax[1 + f, 1], A_long_range_output, f)

fg.suptitle("Eyes Open, PLV")

plt.tight_layout()

name = "bhattacharya_yo_plv_locs"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 12.5), forward=False)
fg.savefig(name + ".png", dpi=200, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
