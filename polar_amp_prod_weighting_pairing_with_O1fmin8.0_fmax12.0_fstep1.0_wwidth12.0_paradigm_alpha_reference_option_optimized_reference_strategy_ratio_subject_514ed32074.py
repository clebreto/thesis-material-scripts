import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import FuncFormatter, MultipleLocator, FormatStrFormatter
from scipy import stats
from scipy.stats import vonmises
from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 40

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def multiple_formatter(denominator=2, number=np.pi, latex='\pi'):
    def gcd(a, b):
        while b:
            a, b = b, a%b
        return a
    def _multiple_formatter(x, pos):
        den = denominator
        num = int(np.rint(den*x/number))
        com = gcd(num,den)
        (num,den) = (int(num/com),int(den/com))
        if den==1:
            if num==0:
                return r'$0$'
            if num==1:
                return r'$%s$'%latex
            elif num==-1:
                return r'$-%s$'%latex
            else:
                return r'$%s%s$'%(num,latex)
        else:
            if num==1:
                return r'$\frac{%s}{%s}$'%(latex,den)
            elif num==-1:
                return r'$\frac{-%s}{%s}$'%(latex,den)
            else:
                return r'$\frac{%s%s}{%s}$'%(num,latex,den)
    return _multiple_formatter
​
class Multiple:
    def __init__(self, denominator=2, number=np.pi, latex='\pi'):
        self.denominator = denominator
        self.number = number
        self.latex = latex
​
    def locator(self):
        return plt.MultipleLocator(self.number / self.denominator)
​
    def formatter(self):
        return plt.FuncFormatter(multiple_formatter(self.denominator, self.number, self.latex))

#recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")
recordings = pl.Path("/home/clebreto/Data/Nice/Epi_ZE_Yf_ultimate/")

colors = np.array(["black", "red", "blue", "cyan", "orange"])
markers = np.array(["None", "+", "o", "x"])

## Channel selection
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])

## Wavelet Transform
fmin = 8.
fmax = 12.
fstep = 1.
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

paradigm = 'alpha' # possible values : 'alpha', 'calculus'
subject_id = "514ed32074"
pairing = 'with_O1' # possible values : 'with_O1', 'chain'
weighting = 'amp_prod_weighting' # possible values : 'amp_prod_weighting', 'no_weighting'
reference_option = 'optimized' # possible values : 'optimized', 'electrode'
reference_electrode = 'CPz' # if reference_option == 'electrode'
reference_strategy = 'ratio' # if reference_option = 'optimized', possible values : 'sum1', 'ratio'
crop = 3.

nb_files = 0
files = sorted(recordings.glob("./" + str(subject_id) + "/*.edf"))
files = [f for f in files]
min_nb_values = float('inf')
nb_channels = None
sampling_rate = 0
for f in tqdm(files) :
    nb_files += 1
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32] # 33 is GSR
    nb_values = signal.shape[1]
    nb_channels = signal.shape[0]
    if nb_values < min_nb_values :
        min_nb_values = nb_values

    sampling_rate = reader_edf_file.samplingRate()

    ## Select only certain electrodes, and filter the signal
    channels = np.array(reader_edf_file.channels())
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

Y_mask = []
## To compute on pairs
if pairing == 'chain' :
    Y_mask = np.eye(nb_channels, nb_channels, 1)
elif pairing == 'with_O1' :
    Y_mask = np.zeros(shape=(nb_channels, nb_channels))
    Y_mask[0, 1:] = np.ones(nb_channels - 1)
else :
    print("pairing not recognized")
    exit

pairs_indices = np.nonzero(Y_mask)
nb_pairs = len(Y_mask[pairs_indices])

bars_diff = {}
for f in tqdm(files) :
    print(str(f))

    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32, :min_nb_values] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Select only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    if reference_option == 'electrode' :
        if reference_electrode in channels :
            reference = signal[[np.where(channels == c)[0][0] for c in [reference_electrode]]]
            signal = signal - reference
    else :
        channels_not_selection = [c for c in channels if c not in channels_selection]
        indices_not_selection = [np.where(channels == c)[0][0] for c in channels_not_selection]
        reference_signal = signal[indices_not_selection]
        center_frequency = (fmax + fmin) / 2.
        bandwidth = (fmax - fmin)
        bp = ztrcore.ztrProcessFilterBandPassOffline()
        bp.setFrequency(center_frequency)
        bp.setBandwidth(bandwidth)
        bp.setOrder(4)
        bp.setSamplingRate(sampling_rate)
        bp.setSignal(reference_signal)
        bp.run()
        M = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
        H = M.dot(M.T)
        if np.linalg.matrix_rank(H) != reference_signal.shape[0]:
            print("MATRIX NOT FULL RANK : ", np.linalg.matrix_rank(H))
            continue
        if reference_strategy == 'ratio' :
            S = reference_signal[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
            G = S.dot(S.T)
            (ratio_e_val, ratio_e_vec) = np.linalg.eig(np.dot(np.linalg.inv(G), H))
            weights = np.real(ratio_e_vec[:, np.argmin(ratio_e_val)]) # Select the vector associated to the smallest eigen value
        elif reference_strategy == 'sum1' :
            K = H[0:-1,0:-1]
            h = H[-1,0:-1]
            a = H[-1,-1]
            o = np.ones((M.shape[0]-1))
            Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
            B = a*o-h

            print("Q Rank : ", np.linalg.matrix_rank(Q))
            x = np.linalg.solve(Q,B)
            weights = np.append(x, 1 - np.dot(x,o))
        else :
            print("Reference strategy not recognized")
            exit
        reference = np.dot(weights, reference_signal)
        signal = signal - reference

    if str(f) not in bars_diff.keys() :
        bars_diff[str(f)] = {}

    signal = signal[indices_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output())

    ## Extract the amplitude and the phase
    amplitude = np.abs(wt)
    phase = np.angle(wt)

    w_phase_diff = np.empty(shape=(nb_pairs, phase.shape[1], phase.shape[2]))
    w_amplitude_prod = np.empty(shape=(nb_pairs, amplitude.shape[1], amplitude.shape[2]))
    for p_i in range(nb_pairs) :
        w_phase_diff[p_i]     = phase[pairs_indices[1][p_i]] - phase[pairs_indices[0][p_i]]
        w_amplitude_prod[p_i] = amplitude[pairs_indices[1][p_i]] * amplitude[pairs_indices[0][p_i]]
    ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
    w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
    w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)

    bars_diff_block = []
    for p_i in range(nb_pairs) :
        if weighting == 'amp_prod_weighting' :
            bars_diff_block.append(np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi], weights=w_amplitude_prod[p_i])[0])
        elif weighting == 'no_weighting' :
            bars_diff_block.append(np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi])[0])
        # elif weighting == 'amp_mod_weighting' :
        #     # Since the phase differences in a window are not normaly distributed, we take the most representative value as the bin value of the highest bar
        #     bars, bins = np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi], weights=w_amplitude_prod[p_i])
            # diff[p_i][w_i] = bins[np.argmax(bars)]
        else :
            print("weighting not recognized")
    bars_diff[str(f)] = bars_diff_block

## Creates the histograms to fill with data :
##  - as many histograms as pairs of channels x 2 (for the two conditions)
##  - with as many bins as 100 (100 bins between all the possible phase lags (-pi -> pi)
bins = np.histogram([], bins=100, range=[-np.pi, np.pi], weights=[])[1]

width = 0.7 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2

locations = {}
concentrations = {}
for f in files :
    if str(f) not in bars_diff.keys():
        continue
    for p_i in range(nb_pairs) :
        if p_i not in locations.keys() :
            locations[p_i] = []
            concentrations[p_i] = []
    for p_i in range(nb_pairs) :
        #One histogram for each pair and each block
        data_from_histo = stats.rv_histogram((bars_diff[str(f)][p_i], bins)).rvs(size=1000)
        fitted_bars_diff = np.histogram(data_from_histo, bins=100, range=[-np.pi, np.pi])[0]
        (concentration, location, scale) = vonmises.fit(data_from_histo, fscale=1.)
        locations[p_i].append(location)
        concentrations[p_i].append(concentration)

plt.clf()
fig, ax = plt.subplots(1, 1, subplot_kw={'projection': 'polar'})
for p_i, p in enumerate(locations) :
    ax.plot(center, np.histogram(np.array(locations[p]), bins=100, range=[-np.pi, np.pi])[0], color=colors[p_i], marker=markers[1], label="ECR " + channels[pairs_indices[1][p_i]] + "-" + channels[pairs_indices[0][p_i]])
ax.legend()

name = "polar_" + weighting + "_pairing_" + pairing + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width) + "_paradigm_" + paradigm + "_reference_option_" + reference_option + "_reference_strategy_" + reference_strategy + "_subject_" + subject_id
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((20.5, 20.5), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
