import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import scipy as sc
from tqdm import tqdm

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def yoyf(signal, events_reader_output, event="") :
    events = []
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if events_reader_output[e_i].uid == event :
            events.append(signal[:, int(events_reader_output[e_i].timestamp / 1000. * sampling_rate):int(events_reader_output[e_i + 1].timestamp / 1000. * sampling_rate)])
        skip = True
    return events

def autocorr(s, max_lag_size) :
    result = np.zeros(shape=(s.shape[0], max_lag_size))
    s -= np.expand_dims(np.mean(s, axis=(1)), axis=(1))
    result[:, 0] = np.ones(shape=(s.shape[0]))
    for k in range(1, max_lag_size) :
        result[:, k] = np.sum(s[:, k:]*np.conj(s[:, :-k]), axis=(1))/(np.sqrt(np.sum(s[:, :-k]*s[:, :-k], axis=(1))*np.sum(s[:, k:]*s[:, k:], axis=(1))))
    return result

recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

## Event selection
event = "alpha"
subject_id = "0001"

## Channel selection
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])
channels_colors = np.array(['red', 'blue', 'green', 'orange', 'grey'])
nb_channels = channels_selection.shape[0] #If all channels are available in the recordings

sampling_rate = 512
max_lag_duration = 5.  #seconds
max_lag_size = int(max_lag_duration * sampling_rate)  #samples

autocorrelations = []
files = sorted(recordings.glob("./*_2021_*/*" + str(subject_id) + "_alpha.edf"))
files = [f for f in files]
for f in tqdm(files) :
    print(f)
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal        = reader_edf_file.output()[:32, :] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Reads the event as they were presented during the recording
    events_reader = ztrcore.ztrProcessEventsReader()
    events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
    events_reader.run()
    events_reader_output = events_reader.output()

    ## Selects only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    signal = signal[indices_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    highpass = ztrcore.ztrProcessFilterHighPassOffline()
    highpass.setSignal(signal);
    highpass.setFrequency(1.);
    highpass.setSamplingRate(sampling_rate);
    highpass.setOrder(4);
    highpass.run();
    highpass_output = np.copy(highpass.output());

    lowpass = ztrcore.ztrProcessFilterLowPassOffline()
    lowpass.setSignal(highpass_output);
    lowpass.setFrequency(30.);
    lowpass.setSamplingRate(sampling_rate);
    lowpass.setOrder(4);
    lowpass.run();
    lowpass_output = np.copy(lowpass.output());

    ## Recovers the event as a list of matrices
    signals = yoyf(lowpass_output, events_reader_output, event)
    for s in signals :
        autocorrelation = autocorr(s, max_lag_size)
        autocorrelations.append(autocorrelation)

plt.clf()
fg, ax = plt.subplots(nb_channels, 1)
for c_i in range(nb_channels):
    ax[c_i].set_title(channels_selection[c_i])
for s in autocorrelations:
    for c_i in range(nb_channels):
        ax[c_i].plot(np.linspace(0, max_lag_size / sampling_rate, max_lag_size), s[c_i], color = channels_colors[c_i], linewidth=0.1)
        ax[c_i].set_ylim(-1, 1)

fg.suptitle("Autocorrelation, " + event)

name = "autocorrelation_" + event + "_channels_other_correlation_low_pass_high_pass_subject_" + str(subject_id)
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((15.5, 15.5), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
