import ztrcore
import ztrplotting
import numpy as np
from scipy.linalg import eigh
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy as sc
from tqdm import tqdm

import matplotlib
matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 30,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 20

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")

#recordings = pl.Path("/user/clebreto/home/Data/Nice/")
recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")

subject_id = "0001"

nb_windows = 1

#Filter
center_freq = 16.
bandwidth = 3.

fmin = 2.
fmax = 35.
fstep = 0.5
frequencies = np.arange(fmin, fmax, fstep)

wavelet_width = 12.

files = np.array(list(recordings.glob("./*/*" + subject_id + "_alpha.edf")))

# Crop at begin and end of the signal
crop = 3

nb_values = []
sampling_rates = []
nb_channels = []
minima = []
maxima = []
for f in files :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal = reader_edf_file.output()[:32] # above are bio signals
    nb_channels.append(signal.shape[0])
    sampling_rates.append(reader_edf_file.samplingRate())
    nb_values.append(signal.shape[1])

sampling_rate = sampling_rates[0]
nb_channels = nb_channels[0]
min_nb_values = np.min(np.array(nb_values))

for f in files :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal = reader_edf_file.output()[:32] # above are bio signals

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(bandwidth)
    bp.setFrequency(center_freq)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()

    signal_filtered = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
    minima.append(np.min(signal_filtered))
    maxima.append(np.max(signal_filtered))

minimum = np.min(minima)
maximum = np.max(maxima)

# Weights
ev = np.zeros(shape=(0, nb_windows, nb_channels)) # nb_files, nb_windows, nb_channels
w = np.zeros(shape=(0, nb_windows, nb_channels)) # nb_files, nb_windows, nb_channels
ratio = np.zeros(shape=(0, nb_windows, nb_channels)) # nb_files, nb_windows, nb_channels
# Residuals
residual_ev = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
residual_w = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
residual_ratio = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
residual_mean = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows

residual_ev_ratio = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
residual_w_ratio = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
residual_ratio_ratio = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
residual_mean_ratio = np.zeros(shape=(0, nb_windows)) # nb_files, nb_windows
# Time Frequency analysis
mean_tf_ev = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_w = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_ratio = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_mean = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_ev_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_ratio_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_w_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
mean_tf_mean_unfiltered = np.zeros(shape=(frequencies.shape[0], int(min_nb_values - 2 * crop * sampling_rate)), dtype=complex)
# Distribution analysis
bars_old_reference = []
bars_ev = []
bars_w = []
bars_ratio = []
bins = np.histogram([], range=(minimum, maximum), bins=100)[1]

for c_i in range(nb_channels) :
    bars_old_reference.append(np.histogram([], range=(minimum, maximum), bins=100)[0])
    bars_ev.append(np.histogram([], range=(minimum, maximum), bins=100)[0])
    bars_w.append(np.histogram([], range=(minimum, maximum), bins=100)[0])

for f in tqdm(files) :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()

    signal        = reader_edf_file.output()[:32, :min_nb_values]
    channels      = np.array(reader_edf_file.channels()[:32])

    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setBandwidth(bandwidth)
    bp.setFrequency(center_freq)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(signal)
    bp.run()
    signal_filtered = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]

    ev_file = np.zeros(shape=(nb_windows, nb_channels))
    w_file = np.zeros(shape=(nb_windows, nb_channels))
    ratio_file = np.zeros(shape=(nb_windows, nb_channels))

    residual_ev_file = np.zeros(shape=(nb_windows))
    residual_w_file = np.zeros(shape=(nb_windows))
    residual_ratio_file = np.zeros(shape=(nb_windows))
    residual_mean_file = np.zeros(shape=(nb_windows))

    residual_ev_ratio_file = np.zeros(shape=(nb_windows))
    residual_w_ratio_file = np.zeros(shape=(nb_windows))
    residual_ratio_ratio_file = np.zeros(shape=(nb_windows))
    residual_mean_ratio_file = np.zeros(shape=(nb_windows))

    for w_i in range(nb_windows) :
        start = int(w_i * signal_filtered.shape[1] / nb_windows)
        end   = int((w_i + 1) * signal_filtered.shape[1] / nb_windows)
        M = signal_filtered[:, start : end]
        n = M.shape[0]
        S = signal[:, int(crop*sampling_rate): -int(crop*sampling_rate)][:, start : end]
        H = M.dot(M.T)
        G = S.dot(S.T)

        if np.linalg.matrix_rank(H) == signal.shape[0]: # Ensures matrix is full rank
            ##### ratio #####
            (ratio_e_val, ratio_e_vec) = np.linalg.eig(np.dot(np.linalg.inv(G), H))
            #(ratio_e_val, ratio_e_vec) = eigh(H, G)
            s_ratio_e_vec = np.real(ratio_e_vec[:, np.argmin(ratio_e_val)]) # Select the vector associated to the smaller eigen value
            ref_ratio = np.dot(s_ratio_e_vec, signal_filtered[:, start : end])
            ref_ratio_unfiltered = np.dot(s_ratio_e_vec, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)][:, start : end])

            ratio_file[w_i] = s_ratio_e_vec

            residual_ratio_file[w_i] = np.dot(ref_ratio, ref_ratio) / ref_ratio.shape[0]
            residual_ratio_ratio_file[w_i] = residual_ratio_file[w_i] / (np.dot(ref_ratio_unfiltered, ref_ratio_unfiltered) / ref_ratio.shape[0])

            wf_ratio = ztrcore.ztrProcessWaveletFourier()
            wf_ratio.setFrequencies(frequencies)
            wf_ratio.setSignal(np.expand_dims(ref_ratio, axis=0))
            wf_ratio.setWidth(wavelet_width)
            wf_ratio.setSamplingRate(sampling_rate)
            wf_ratio.run()
            wf_ratio_o = np.squeeze(wf_ratio.output(), axis=0)
            mean_tf_ratio[:, start : end] += wf_ratio_o

            wf_ratio.setSignal(np.expand_dims(ref_ratio_unfiltered, axis=0))
            wf_ratio.run()
            wf_ratio_o_unfiltered = np.squeeze(wf_ratio.output(), axis=0)
            mean_tf_ratio_unfiltered[:, start : end] += wf_ratio_o_unfiltered

            ##### sum{w_i^2}=1 #####
            (e_val, e_vec) = np.linalg.eig(H)
            s_e_vec = np.real(e_vec[:, np.argmin(e_val)]) # Select the vector associated to the smaller eigen value

            #s_e_vec_norm = s_e_vec / np.sum(s_e_vec)
            ref_ev = np.dot(s_e_vec, signal_filtered[:, start : end])
            ref_ev_unfiltered = np.dot(s_e_vec, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)][:, start : end])

            ev_file[w_i] = s_e_vec

            residual_ev_file[w_i] = np.dot(ref_ev, ref_ev) / ref_ev.shape[0]
            residual_ev_ratio_file[w_i] = residual_ev_file[w_i] / (np.dot(ref_ev_unfiltered, ref_ev_unfiltered) / ref_ev.shape[0])

            wf_ev = ztrcore.ztrProcessWaveletFourier()
            wf_ev.setFrequencies(frequencies)
            wf_ev.setSignal(np.expand_dims(ref_ev, axis=0))
            wf_ev.setWidth(wavelet_width)
            wf_ev.setSamplingRate(sampling_rate)
            wf_ev.run()
            wf_ev_o = np.squeeze(wf_ev.output(), axis=0)
            mean_tf_ev[:, start : end] += wf_ev_o

            wf_ev.setSignal(np.expand_dims(ref_ev_unfiltered, axis=0))
            wf_ev.run()
            wf_ev_o_unfiltered = np.squeeze(wf_ev.output(), axis=0)
            mean_tf_ev_unfiltered[:, start : end] += wf_ev_o_unfiltered

            ##### sum{w_i}=1 #####
            K = H[0:-1,0:-1]
            h = H[-1,0:-1]
            a = H[-1,-1]
            o = np.ones((n-1))

            print(np.outer(h,o)-np.outer(o,h))
            Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
            B = a*o-h

            x = np.linalg.solve(Q,B)
            x = np.append(x, 1 - np.dot(x,o))
            ref_w = np.dot(x, signal_filtered[:, start : end])
            ref_w_unfiltered = np.dot(x, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)][:, start : end])

            w_file[w_i] = x

            residual_w_file[w_i] = np.dot(ref_w, ref_w) / ref_w.shape[0]
            residual_w_ratio_file[w_i] = residual_w_file[w_i] / (np.dot(ref_w_unfiltered, ref_w_unfiltered) / ref_w.shape[0])

            wf_w = ztrcore.ztrProcessWaveletFourier()
            wf_w.setFrequencies(frequencies)
            wf_w.setSignal(np.expand_dims(ref_w, axis=0))
            wf_w.setWidth(wavelet_width)
            wf_w.setSamplingRate(sampling_rate)
            wf_w.run()
            wf_w_o = np.squeeze(wf_w.output(), axis=0)
            mean_tf_w[:, start : end] += wf_w_o

            wf_w.setSignal(np.expand_dims(ref_w_unfiltered, axis=0))
            wf_w.run()
            wf_w_o_unfiltered = np.squeeze(wf_w.output(), axis=0)
            mean_tf_w_unfiltered[:, start : end] += wf_w_o_unfiltered

            ##### mean reference #####
            mean = np.ones(signal_filtered.shape[0]) / signal_filtered.shape[0]
            ref_mean = np.dot(mean, signal_filtered[:, start : end])
            ref_mean_unfiltered = np.dot(mean, signal[:, int(crop * sampling_rate):-int(crop * sampling_rate)][:, start : end])

            residual_mean_file[w_i] = np.dot(ref_mean, ref_mean) / ref_mean.shape[0]
            residual_mean_ratio_file[w_i] = residual_mean_file[w_i] / (np.dot(ref_mean_unfiltered, ref_mean_unfiltered) / ref_mean.shape[0])

            wf_mean = ztrcore.ztrProcessWaveletFourier()
            wf_mean.setFrequencies(frequencies)
            wf_mean.setSignal(np.expand_dims(ref_mean, axis=0))
            wf_mean.setWidth(wavelet_width)
            wf_mean.setSamplingRate(sampling_rate)
            wf_mean.run()
            wf_mean_o = np.squeeze(wf_mean.output(), axis=0)
            mean_tf_mean[:, start : end] += wf_mean_o

            wf_mean.setSignal(np.expand_dims(ref_mean_unfiltered, axis=0))
            wf_mean.run()
            wf_mean_o_unfiltered = np.squeeze(wf_mean.output(), axis=0)
            mean_tf_mean_unfiltered[:, start : end] += wf_mean_o_unfiltered

            for c_i in range(signal_filtered.shape[0]):
                bars_old_reference[c_i] += np.histogram(signal_filtered[c_i, start : end], range=(minimum, maximum), bins=100)[0]
                bars_ev[c_i] += np.histogram(signal_filtered[c_i, start : end] - ref_ev, range=(minimum, maximum), bins=100)[0]
                bars_w[c_i] += np.histogram(signal_filtered[c_i, start : end] - ref_w, range=(minimum, maximum), bins=100)[0]

        else:
            print("Matrix is not full rank")

    ev = np.concatenate((ev, np.expand_dims(ev_file, axis=0)), axis=0)
    w = np.concatenate((w, np.expand_dims(w_file, axis=0)), axis=0)
    ratio = np.concatenate((ratio, np.expand_dims(ratio_file, axis=0)), axis=0)

    residual_ev = np.concatenate((residual_ev, np.expand_dims(residual_ev_file, axis=0)), axis=0)
    residual_w = np.concatenate((residual_w, np.expand_dims(residual_w_file, axis=0)), axis=0)
    residual_ratio = np.concatenate((residual_ratio, np.expand_dims(residual_ratio_file, axis=0)), axis=0)
    residual_mean = np.concatenate((residual_mean, np.expand_dims(residual_mean_file, axis=0)), axis=0)

    residual_ev_ratio = np.concatenate((residual_ev_ratio, np.expand_dims(residual_ev_ratio_file, axis=0)), axis=0)
    residual_w_ratio = np.concatenate((residual_w_ratio, np.expand_dims(residual_w_ratio_file, axis=0)), axis=0)
    residual_ratio_ratio = np.concatenate((residual_ratio_ratio, np.expand_dims(residual_ratio_ratio_file, axis=0)), axis=0)
    residual_mean_ratio = np.concatenate((residual_mean_ratio, np.expand_dims(residual_mean_ratio_file, axis=0)), axis=0)

# Mean over recordings
mean_tf_ev /= residual_mean.shape[0]
mean_tf_w /= residual_mean.shape[0]
mean_tf_mean /= residual_mean.shape[0]

mean_tf_ev_unfiltered /= residual_mean.shape[0]
mean_tf_w_unfiltered /= residual_mean.shape[0]
mean_tf_mean_unfiltered /= residual_mean.shape[0]

vmin = min(min(np.min(np.abs(mean_tf_ev)), np.min(np.abs(mean_tf_w)), np.min(np.abs(mean_tf_mean))), np.min(np.abs(mean_tf_ratio)))
vmax = max(max(np.max(np.abs(mean_tf_ev)), np.max(np.abs(mean_tf_w)), np.max(np.abs(mean_tf_mean))), np.max(np.abs(mean_tf_ratio)))
vmin_unfiltered = min(min(np.min(np.abs(mean_tf_ev_unfiltered)), np.min(np.abs(mean_tf_w_unfiltered)), np.min(np.abs(mean_tf_mean_unfiltered))), np.min(np.abs(mean_tf_ratio_unfiltered)))
vmax_unfiltered = max(max(np.max(np.abs(mean_tf_ev_unfiltered)), np.max(np.abs(mean_tf_w_unfiltered)), np.max(np.abs(mean_tf_mean_unfiltered))), np.max(np.abs(mean_tf_ratio_unfiltered)))

bar_min = min(np.min(bars_ev), np.min(bars_w), np.min(bars_old_reference)) # Most likely 0...
bar_max = max(np.max(bars_ev), np.max(bars_w), np.max(bars_old_reference))

ev_f = [ef for ef in np.reshape(ev, (ev.shape[0], ev.shape[1] * ev.shape[2]))]
ev_c = [ec for ec in np.reshape(ev.T, (ev.shape[2], ev.shape[1] * ev.shape[0]))]
ratio_f = [ef for ef in np.reshape(ratio, (ratio.shape[0], ratio.shape[1] * ratio.shape[2]))]
ratio_c = [ec for ec in np.reshape(ratio.T, (ratio.shape[2], ratio.shape[1] * ratio.shape[0]))]
w_f = [wf for wf in np.reshape(w, (w.shape[0], w.shape[1] * w.shape[2]))]
w_c = [wc for wc in np.reshape(w.T, (w.shape[2], w.shape[1] * w.shape[0]))]

plt.clf()
fg, ax = plt.subplots(1, 1)
positions = np.arange(0, ev.shape[0], 1)

# ax[0, 0].set_title("sum{w_i}=1")
# ax[0, 0].boxplot(w_f, showfliers=False, showmeans=True)
ax.set_title("$\sum_w{w_i}=1$")
ax.boxplot(w_c, showfliers=False, showmeans=True, labels=channels)

# ax[0, 1].set_title("$\sum_w{w_i^2}=1$")
# ax[0, 1].boxplot(ev_f, showfliers=False, showmeans=True)
#x.set_title("$\sum_w{w_i^2}=1$")
#x.boxplot(ev_c, labels=channels)

# ax[0, 3].set_title("$\min_{w}(||wS*S*^Tw^T|| / ||wSS^Tw^T||)y$)
# ax[0, 3].boxplot(ratio_f, showfliers=False, showmeans=True)
#x.set_title("$\min_{w}(||wS*S*^Tw^T|| / ||wSS^Tw^T||)y$"
#x.boxplot(ratio_c, labels=channels)

# for b_i, b in enumerate(bars_w) :
#     ax.plot(bins[:-1], b, label=channels[b_i])
# ax.set_ylim([bar_min, bar_max])
# ax.legend(ncol=3)
# ax.set_title("sum{w_i}=1")
# for b_i, b in enumerate(bars_ev) :
#     ax.plot(bins[:-1], b, label=channels[b_i])
# ax.set_ylim([bar_min, bar_max])
# ax.legend(ncol=3)
# ax.set_title("sum{w_i^2}=1")
# for b_i, b in enumerate(bars_ratio) :
#     ax.plot(bins[:-1], b, label=channels[b_i])
# ax.set_ylim([bar_min, bar_max])
# ax.legend(ncol=3)
# ax.set_title("min(||wS*S*^Tw^T|| / ||wSS^Tw^T||)")
# for b_i, b in enumerate(bars_old_reference) :
#     ax.plot(bins[:-1], b, label=channels[b_i])
# ax.set_ylim([bar_min, bar_max])
# ax.legend(ncol=3)
# ax.set_title("Filtered signal, old reference (CPz)")

# ztrplotting.plotTopography(ax, np.mean(w, axis=(0, 1)), headset)
# ztrplotting.plotTopography(ax, np.mean(ev, axis=(0, 1)), headset)
# ztrplotting.plotTopography(ax, np.mean(ratio, axis=(0, 1)), headset)

# ax.scatter(positions, np.mean(residual_w, axis=1), label="$\sum_j{w_j}=1$")
# ax.scatter(positions, np.mean(residual_ev, axis=1), label="$\sum_j{w_j^2}=1$")
# ax.scatter(positions, np.mean(residual_mean, axis=1), label="$\sum_j{w_j}=1, w_i = 1/n$")
# ax.scatter(positions, np.mean(residual_ratio, axis=1), label="$\min_{w}(||wS*S*^Tw^T|| / ||wSS^Tw^T||)$")
# ax.set_yscale('log')
# ax.legend()
# ax.set_title("Residuals")

# ax.scatter(positions, np.mean(residual_w_ratio, axis=1), label="$\sum_j{w_j}=1$")
# ax.scatter(positions, np.mean(residual_ev_ratio, axis=1), label="$\sum_j{w_j^2}=1$")
# ax.scatter(positions, np.mean(residual_mean_ratio, axis=1), label="$\sum_j{w_j}=1, w_i = 1/n$")
# ax.scatter(positions, np.mean(residual_ratio_ratio, axis=1), label="$\min_{w}(||wS*S*^Tw^T|| / ||wSS^Tw^T||)$")
# ax.set_yscale('log')
# ax.legend()
# ax.set_title("Residuals ratio")

# ztrplotting.plotTFAmplitudes(fg, ax[5, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_w), ratio = 2. / 10., title="sum{w_i}=1 TF mean over experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[5, 1], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_ev), ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[5, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_mean), ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[5, 3], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_ratio), ratio = 2. / 10., title="min(||wS*S*^Tw^T|| / ||wSS^Tw^T||)\n TF mean over experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[6, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_w), ratio = 2. / 10., vmin=vmin, vmax=vmax, title="sum{w_i}=1 TF mean over experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[6, 1], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_ev), ratio = 2. / 10., vmin=vmin, vmax=vmax, title="sum{w_i^2}=1 TF mean over experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[6, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_mean), ratio = 2. / 10., vmin=vmin, vmax=vmax, title="sum{w_i}=1, w_i = 1/n TF mean over experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[6, 3], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_ratio), ratio = 2. / 10., vmin=vmin, vmax=vmax, title="min(||wS*S*^Tw^T|| / ||wSS^Tw^T||)\n TF mean over experiments\n Alpha band filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[7, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_w_unfiltered), ratio = 2. / 10., title="sum{w_i}=1 TF mean over experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[7, 1], np.linspace(crop, duration - crop , int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_ev_unfiltered), ratio = 2. / 10., title="sum{w_i^2}=1 TF mean over experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[7, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_mean_unfiltered), ratio = 2. / 10., title="sum{w_i}=1, w_i = 1/n TF mean over experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[7, 3], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_ratio_unfiltered), ratio = 2. / 10., title="min(||wS*S*^Tw^T|| / ||wSS^Tw^T||)\n TF mean over experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[8, 0], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_w_unfiltered), ratio = 2. / 10., vmin=vmin_unfiltered, vmax=vmax_unfiltered, title="sum{w_i}=1 TF mean over experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[8, 1], np.linspace(crop, duration - crop , int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_ev_unfiltered), ratio = 2. / 10., vmin=vmin_unfiltered, vmax=vmax_unfiltered, title="sum{w_i^2}=1 TF mean over experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[8, 2], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_mean_unfiltered), ratio = 2. / 10., vmin=vmin_unfiltered, vmax=vmax_unfiltered, title="sum{w_i}=1, w_i = 1/n TF mean over experiments\n No filtering")
# ztrplotting.plotTFAmplitudes(fg, ax[8, 3], np.linspace(crop, duration - crop, int(min_nb_values - 2 * crop * sampling_rate)), frequencies, np.abs(mean_tf_ratio_unfiltered), ratio = 2. / 10., vmin=vmin_unfiltered, vmax=vmax_unfiltered, title="min(||wS*S*^Tw^T|| / ||wSS^Tw^T||)\n TF mean over experiments\n No filtering")

name = "just_beta_channels_sum_equals_one_minimization_min_square_all_files_" + subject_id + "_nb_windows_" + str(nb_windows)
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((30, 10), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
