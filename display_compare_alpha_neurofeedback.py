import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

folder = "/user/clebreto/home/Data/Neurofeedback/2021_04_21/"
files_path = {'volume_unknown':   folder + "2021_04_22_13_59_31_neurofeedback_clean_bandit_0001.edf",
              'volume_50percent': folder + "2021_04_22_14_39_46_0001_neurofeedback_clean_bandit_volume_50pc.edf",
              'volume_25percent': folder + "2021_04_22_14_48_02_clean_bandit_volume_25pc.edf",
              'volume_10percent': folder + "2021_04_22_14_53_19_clean_bandit_volume_10pc_0001_2.edf",
              'voume_0percent':   folder + "2021_04_22_14_59_18_0001_no_music.edf"}

marker_path = {"2021_04_22_13_59_31__ztrMarkerOneChunkPower_type_Legendre_order_2_center_10_bandwidth_6_1.edf",
               "2021_04_22_14_39_46__ztrMarkerOneChunkPower_type_Legendre_order_2_center_10_bandwidth_6_1.edf",
               "2021_04_22_14_48_02__ztrMarkerOneChunkPower_type_Legendre_order_2_center_10_bandwidth_6_3.edf",
               "2021_04_22_14_53_20__ztrMarkerOneChunkPower_type_Legendre_order_2_center_10_bandwidth_6_5.edf",
               "2021_04_22_14_59_18__ztrMarkerOneChunkPower_type_Legendre_order_2_center_10_bandwidth_6_7.edf"}

# files_path = { 'alpha_neurofeedback': "/user/clebreto/home/Data/Neurofeedback/2021_04_16/2021_04_16_14_10_05_0001_alpha_neurofeedback_chunk_length_1_in_my_blood_O1_O2.edf",
#                'alpha_neurofeedback_1' : "/user/clebreto/home/Data/Neurofeedback/2021_04_16/2021_04_16_14_39_25_0001_alpha_neurofeedback_chunk_length_1_imagine_dragons_O1_O2.edf",
#                'alpha_neurofeedback_2' : "/user/clebreto/home/Data/Neurofeedback/2021_04_16/2021_04_16_14_20_56_0001_alpha_neurofeedback_chunk_length_1_clean_bandit_O1_O2.edf",
#                'alpha_no_music' : "/user/clebreto/home/Data/Neurofeedback/2021_04_16/2021_04_16_14_02_19_0001_alpha_neurofeedback_no_music.edf",
#                'alpha_control' : "/user/clebreto/home/Data/Neurofeedback/2021_04_16/2021_04_16_14_45_14_0001_alpha_neurofeedback_no_music.edf",
#                'alpha_lower' : "/user/clebreto/home/Data/Neurofeedback/2021_04_16/2021_04_16_14_51_18_0001_alpha_neurofeedback_in_my_blood_try_to_lower_sound.edf"
#               }

files = {}
sampling_rate = None
channels = None
for f in files_path.items() :
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f[1]))
    reader_edf_file.run()
    files[f[0]] = reader_edf_file.output()[:32]
    sampling_rate = reader_edf_file.samplingRate()
    channels  = np.array(reader_edf_file.channels()[:32])

min_nb_values = np.float64("inf")
for f in files.values() :
    if f.shape[1] < min_nb_values :
        min_nb_values = f.shape[1]

duration  = min_nb_values / sampling_rate

O1_pos = np.where(channels == "O1")[0]

signals = []
for f in files.values() :
    f = f[:, :min_nb_values]

    filter_band_pass_offline = ztrcore.ztrProcessFilterBandPassOffline()
    filter_band_pass_offline.setBandwidth(6.)
    filter_band_pass_offline.setFrequency(10.)
    filter_band_pass_offline.setOrder(4)
    filter_band_pass_offline.setSamplingRate(sampling_rate)
    filter_band_pass_offline.setSignal(f)
    filter_band_pass_offline.run()
    f = np.abs(np.copy(filter_band_pass_offline.output()))

    signals.append(f[O1_pos, :])

time = np.linspace(0., duration, min_nb_values)
signals = np.concatenate(signals, axis=0)

print(files)
for c, k in zip(signals, files.keys()) :
    plt.hist(c, label=k, bins=100, range=(0, 100), alpha=0.4)
    # plt.plot(c, label=k)
plt.legend()
plt.show()

name = "display_compare_alpha_neurofeedback"
shutil.copy("impact.py", name + ".py")

ztrplotting.plot(time, signals, list(files.keys()), window_size=duration, span=200, save=name)
