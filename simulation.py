import ztrcore
import ztrplotting
import numpy as np
from scipy.linalg import eigh
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits import mplot3d
import scipy as sc
from tqdm import tqdm

import matplotlib
matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 30,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 20

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def computeWeights(sensors_activities) :
    sensors_activities += 0.1 * np.random.rand(sensors_activities.shape[0], sensors_activities.shape[1])
    sensors_activities_ref = sensors_activities - sensors_activities[50]
    sensors_activities_ref = np.delete(sensors_activities_ref, 50, 0)

    crop = 2
    bp = ztrcore.ztrProcessFilterBandPassOffline()
    bp.setFrequency(10)
    bp.setBandwidth(3)
    bp.setOrder(4)
    bp.setSamplingRate(sampling_rate)
    bp.setSignal(sensors_activities_ref)
    bp.run()
    M = bp.output()[:, int(crop*sampling_rate): -int(crop*sampling_rate)]
    H = M.dot(M.T)
    if np.linalg.matrix_rank(H) != sensors_activities_ref.shape[0]:
        print("MATRIX NOT FULL RANK : ", np.linalg.matrix_rank(H))
        return []
    else :
        K = H[0:-1,0:-1]
        h = H[-1,0:-1]
        a = H[-1,-1]
        o = np.ones((M.shape[0]-1))
        Q = K-np.outer(h,o)-np.outer(o,h)+a*np.outer(o,o)
        B = a*o-h
        x = np.linalg.solve(Q,B)
        weights = np.append(x, 1 - np.dot(x,o))
        return weights

sampling_rate = 512.
duration = 10.

# Sources
nb_sources = 900
phi = np.linspace(-np.pi/2., np.pi/2., int(np.sqrt(nb_sources))) + np.pi / 100. * np.random.rand(int(np.sqrt(nb_sources)))
theta = np.linspace(0, np.pi, int(np.sqrt(nb_sources))) + np.pi / 100. * np.random.rand(int(np.sqrt(nb_sources)))
x = []
y = []
z = []
for p in phi:
    for t in theta:
        x.append(np.sin(p) * np.cos(t))
        y.append(np.sin(p) * np.sin(t))
        z.append(np.cos(p))

x = np.array(x)
y = np.array(y)
z = np.array(z)
sources_positions = np.vstack((x, y, z))
sources_positions = np.transpose(sources_positions)
sources_orientations = sources_positions / 10 # because centered on 0
sources_activities = np.random.rand(nb_sources, int(duration * sampling_rate))

f0 = 10.
f1 = 15.
# f2 = 10.
phi0 = np.pi / 4.
phi1 = 0.
# phi2 = 0.
time = np.arange(0, duration, 1./sampling_rate)
# s0 = np.cos(2. * np.pi * f0 * time + phi0)
# s1 = np.cos(2. * np.pi * f1 * time + phi1)
# s2 = np.cos(2. * np.pi * f2 * time + phi2)
# sources_activities = np.vstack((s0, s1, s2))
sources_activities[465] += 10 * np.cos(2. * np.pi * f0 * time + phi0)
sources_orientations[465] *= 5
sources_activities[300] += 10 * np.cos(2. * np.pi * f1 * time + phi1)
sources_orientations[300] *= 5

# Sensors
nb_sensors = 100
phi = np.linspace(-np.pi/2, np.pi/2, int(np.sqrt(nb_sensors))) + np.pi / 100. * np.random.rand(int(np.sqrt(nb_sensors)))
theta = np.linspace(0, np.pi, int(np.sqrt(nb_sensors))) + np.pi / 100. * np.random.rand(int(np.sqrt(nb_sensors)))
x = []
y = []
z = []
for p in phi:
    for t in theta:
        x.append(np.sin(p) * np.cos(t))
        y.append(np.sin(p) * np.sin(t))
        z.append(np.cos(p))

x = np.array(x)
y = np.array(y)
z = np.array(z)
sensors_positions = 1.1 * np.vstack((x, y, z))
sensors_positions = np.transpose(sensors_positions)

s2s_proj = ztrcore.ztrProcessSourcesToSensorsProjection()
s2s_proj.setSourcesActivities(sources_activities)
s2s_proj.setSourcesPositions(sources_positions)
s2s_proj.setSourcesOrientations(sources_orientations)
s2s_proj.setSensorsPositions(sensors_positions)
s2s_proj.run()
sensors_activities = s2s_proj.sensorsActivities()

weights = []
for i in range(1000) :
    w = computeWeights(sensors_activities)
    if w != [] :
        weights.append(w)
        print(len(weights))
weights = np.array(weights)
mean_weights = np.mean(weights, axis=0)
#mean_weights = np.insert(mean_weights, 50, 0)

fg, ax = plt.subplots(5, 1)

kk = plt.axes(projection="3d")
xlist=np.linspace(-1.0,1.0,100)
ylist=np.linspace(-1.0,1.0,100)
X,Y= np.meshgrid(xlist,ylist)
Z=np.sqrt(1**2-X**2-Y**2)
kk.plot_wireframe(X,Y,Z,color="r", rstride=10, cstride=10)

xlist=np.linspace(-1.1,1.1,100)
ylist=np.linspace(-1.1,1.1,100)
X,Y= np.meshgrid(xlist,ylist)
Z=np.sqrt(1.1**2-X**2-Y**2)
kk.plot_wireframe(X,Y,Z,color="b", rstride=10, cstride=10)

kk.quiver(sources_positions[:, 0], sources_positions[:, 1], sources_positions[:, 2], sources_orientations[:, 0], sources_orientations[:, 1], sources_orientations[:, 2], color="black")
#`kk.scatter(sensors_positions[:, 0], sensors_positions[:, 1], sensors_positions[:, 2], s=200*np.abs(mean_weights), c=np.abs(mean_weights))
#kk.scatter(sensors_positions[:, 0], sensors_positions[:, 1], sensors_positions[:, 2], s=40)
kk.patch.set_alpha(0.)
kk.view_init(elev=90, azim=45)

sensors_distance = np.sqrt((sensors_positions[:, 0] - sensors_positions[50, 0])*(sensors_positions[:, 0] - sensors_positions[50, 0]) + (sensors_positions[:, 1] - sensors_positions[50, 1])*(sensors_positions[:, 1] - sensors_positions[50, 1]) + (sensors_positions[:, 2] - sensors_positions[50, 2])*(sensors_positions[:, 2] - sensors_positions[50, 2]))

ax[0].plot(sensors_distance)

ax[1].boxplot(weights)

# ax[2].plot(time, sources_activities[465])

# ax[3].plot(time, sensors_activities[50], label="initial")
# sensors_activities_ref = sensors_activities - sensors_activities[50]
# sensors_activities_ref = np.delete(sensors_activities_ref, 50, 0)
# mean_weights = np.delete(mean_weights, 50, 0)
#ax[3].plot(np.dot(mean_weights, sensors_activities_ref), label="artificial")
# ax[3].legend()

# ax[4].plot(time, sensors_activities_ref[50])
#ax[4].plot(time, sensors_activities_ref[]])

fg.show()
name = "simulation"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((60, 20), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()}, pad_inches=10)
