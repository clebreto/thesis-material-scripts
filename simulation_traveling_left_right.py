import ztrcore
import ztrplotting
import numpy as np
from scipy.linalg import eigh
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d.proj3d import proj_transform
from matplotlib.text import Annotation
import scipy as sc
from tqdm import tqdm

import matplotlib

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 30,
    'ytick.labelsize': 40
})
matplotlib.rcParams['legend.fontsize'] = 20

def multiple_formatter(denominator=2, number=np.pi, latex='\pi'):
    def gcd(a, b):
        while b:
            a, b = b, a%b
        return a
    def _multiple_formatter(x, pos):
        den = denominator
        num = int(np.rint(den*x/number))
        com = gcd(num,den)
        (num,den) = (int(num/com),int(den/com))
        if den==1:
            if num==0:
                return r'$0$'
            if num==1:
                return r'$%s$'%latex
            elif num==-1:
                return r'$-%s$'%latex
            else:
                return r'$%s%s$'%(num,latex)
        else:
            if num==1:
                return r'$\frac{%s}{%s}$'%(latex,den)
            elif num==-1:
                return r'$\frac{-%s}{%s}$'%(latex,den)
            else:
                return r'$\frac{%s%s}{%s}$'%(num,latex,den)
    return _multiple_formatter
​
class Multiple:
    def __init__(self, denominator=2, number=np.pi, latex='\pi'):
        self.denominator = denominator
        self.number = number
        self.latex = latex
​
    def locator(self):
        return plt.MultipleLocator(self.number / self.denominator)
​
    def formatter(self):
        return plt.FuncFormatter(multiple_formatter(self.denominator, self.number, self.latex))

class Annotation3D(Annotation):
    '''Annotate the point xyz with text s'''
    def __init__(self, s, xyz, *args, **kwargs):
        Annotation.__init__(self,s, xy=(0,0), *args, **kwargs)
        self._verts3d = xyz

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.xy=(xs,ys)
        Annotation.draw(self, renderer)

def annotate3D(ax, s, *args, **kwargs):
    '''add anotation text s to to Axes3d ax'''
    tag = Annotation3D(s, *args, **kwargs)
    ax.add_artist(tag)

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

colors = np.array(["white", "black", "red", "blue", "cyan"])

channels = ['T7', 'C3', 'Cz', 'C4', 'T8']
pairs_indices = np.array([[1, 2, 3, 4], [0, 0, 0, 0]])

sampling_rate = 512.
duration = 10.

# Sources
nb_sources = 2
sources_positions = [[-1., 0., 0.], [1., 0., 0.]]
sources_positions = np.array(sources_positions)
sources_orientations = sources_positions / 10 # because centered on 0
sources_activities = 1.5*np.random.rand(nb_sources, int(duration * sampling_rate))

f = 10.
phi = 0.93 * np.pi
time = np.arange(0, duration, 1./sampling_rate)
sources_activities[0] += np.cos(2. * np.pi * f * time)
sources_activities[1] += np.cos(2. * np.pi * f * time + phi)

# Sensors
nb_sensors = 5
phi = np.linspace(-np.pi/4, np.pi/4, nb_sensors) + np.pi / 100. * np.random.rand(int(nb_sensors))
x = []
y = []
z = []
for p in phi:
    x.append(0)
    y.append(np.sin(p))
    z.append(np.cos(p))

x = np.array(x)
y = np.array(y)
z = np.array(z)
sensors_positions = 1.1 * np.vstack((x, y, z))
sensors_positions = np.transpose(sensors_positions)

s2s_proj = ztrcore.ztrProcessSourcesToSensorsProjection()
s2s_proj.setSourcesActivities(sources_activities)
s2s_proj.setSourcesPositions(sources_positions)
s2s_proj.setSourcesOrientations(sources_orientations)
s2s_proj.setSensorsPositions(sensors_positions)
s2s_proj.run()
sensors_activities = s2s_proj.sensorsActivities()


wavelet_width = 12
frequencies = np.linspace(8, 12, 4)

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(sensors_activities)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(wavelet_fourier.output())

amplitude = np.abs(wt)
phase = np.angle(wt)

w_phase_diff = np.empty(shape=(4, phase.shape[1], phase.shape[2]))
for p_i in range(1, 5) :
    w_phase_diff[p_i-1] = phase[p_i] - phase[0]

w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)

histograms = []
for p_i in range(1, 5) :
    histograms.append(np.histogram(w_phase_diff[p_i-1], bins=100, range=[-np.pi, np.pi])[0])
bins = np.histogram([], bins=100, range=[-np.pi, np.pi], weights=[])[1]
center = (bins[:-1] + bins[1:]) / 2

fg, ax = plt.subplots(1, 1, subplot_kw=dict(projection="3d"))

xlist=np.linspace(-1.0,1.0,100)
ylist=np.linspace(-1.0,1.0,100)
X,Y= np.meshgrid(xlist,ylist)
Z=np.sqrt(1**2-X**2-Y**2)
ax.plot_wireframe(X,Y,Z,color="r", rstride=10, cstride=10, alpha=0.5)

xlist=np.linspace(-1.1,1.1,100)
ylist=np.linspace(-1.1,1.1,100)
X,Y= np.meshgrid(xlist,ylist)
Z=np.sqrt(1.1**2-X**2-Y**2)
ax.plot_wireframe(X,Y,Z,color="b", rstride=10, cstride=10, alpha=0.5)

ax.quiver(sources_positions[:, 0], sources_positions[:, 1], sources_positions[:, 2], sources_orientations[:, 0], sources_orientations[:, 1], sources_orientations[:, 2], color="black")
ax.scatter(sensors_positions[:, 0], sensors_positions[:, 1], sensors_positions[:, 2], c=colors)
#ax.scatter(sensors_positions[:, 0], sensors_positions[:, 1], sensors_positions[:, 2], s=40)
ax.patch.set_alpha(0.)
ax.view_init(elev=45, azim=45)
# plt.axis('off')
# ax.get_xaxis().set_visible(False)
# ax.get_yaxis().set_visible(False)

for j, xyz_ in enumerate(sensors_positions):
    annotate3D(ax, s=channels[j], xyz=xyz_, fontsize=30, xytext=(-3,3), textcoords='offset points', ha='right',va='bottom')

sources_text = ['S0', 'SF']
for j, xyz_ in enumerate(sources_positions):
    annotate3D(ax, s=sources_text[j], xyz=xyz_, fontsize=30, xytext=(-3,3), textcoords='offset points', ha='right',va='bottom')

# ax[1].plot(time, sensors_activities[0])
# ax[2].plot(time, sensors_activities[1])
# ax[3].plot(time, sensors_activities[2])
# ax[4].plot(time, sensors_activities[3])
# ax[5].plot(time, sensors_activities[4])
# for p_i in range(1, 5) :
#     ax.plot(center, histograms[p_i-1] / np.sum(histograms[p_i-1]), color=colors[p_i-1], label=channels[pairs_indices[1][p_i-1]] + "-" + channels[pairs_indices[0][p_i-1]])
# ax.xaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
# ax.xaxis.set_minor_locator(plt.MultipleLocator(np.pi / 12))
# ax.xaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))
# ax.axvline(x=0.)
# ax.legend()

name = "simulation_traveling_left_right"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((20.5, 20.5), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
