import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import FuncFormatter, MultipleLocator, FormatStrFormatter
import scipy as sc
from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

def multiple_formatter(denominator=2, number=np.pi, latex='\pi'):
    def gcd(a, b):
        while b:
            a, b = b, a%b
        return a
    def _multiple_formatter(x, pos):
        den = denominator
        num = np.int(np.rint(den*x/number))
        com = gcd(num,den)
        (num,den) = (int(num/com),int(den/com))
        if den==1:
            if num==0:
                return r'$0$'
            if num==1:
                return r'$%s$'%latex
            elif num==-1:
                return r'$-%s$'%latex
            else:
                return r'$%s%s$'%(num,latex)
        else:
            if num==1:
                return r'$\frac{%s}{%s}$'%(latex,den)
            elif num==-1:
                return r'$\frac{-%s}{%s}$'%(latex,den)
            else:
                return r'$\frac{%s%s}{%s}$'%(num,latex,den)
    return _multiple_formatter
​
class Multiple:
    def __init__(self, denominator=2, number=np.pi, latex='\pi'):
        self.denominator = denominator
        self.number = number
        self.latex = latex
​
    def locator(self):
        return plt.MultipleLocator(self.number / self.denominator)
​
    def formatter(self):
        return plt.FuncFormatter(multiple_formatter(self.denominator, self.number, self.latex))

def yoyf(t) :
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if t > events_reader_output[e_i].timestamp / 1000. and t < events_reader_output[e_i + 1].timestamp / 1000. :
            return events_reader_output[e_i].uid
        skip = True


recordings = pl.Path("/user/clebreto/home/Data/Neurofeedback/")


## Channel selection
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])

## Wavelet Transform
fmin = 8.
fmax = 12.
fstep = 0.1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 12.

paradigm = 'alpha' # possible values : 'alpha', 'calculus'
subject_id = "0001"
pairing = 'chain' # possible values : 'with_O1', 'chain'
weighting = 'no_weighting' # possible values : 'amp_prod_weighting', 'no_weighting'
reference_electrode = "CPz"

nb_files = 0
files = sorted(recordings.glob("./*/*" + str(subject_id) + "_" + paradigm + ".edf"))
files = [f for f in files]
min_nb_values = float('inf')
nb_channels = None
sampling_rate = 0
for f in tqdm(files) :
    nb_files += 1
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32] # 33 is GSR
    nb_values = signal.shape[1]
    nb_channels = signal.shape[0]
    if nb_values < min_nb_values :
        min_nb_values = nb_values

    sampling_rate = reader_edf_file.samplingRate()

    ## Select only certain electrodes, and filter the signal
    channels = np.array(reader_edf_file.channels())
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

## To compute on pairs
if pairing == 'chain' :
    Y_mask = np.eye(nb_channels, nb_channels, 1)
elif pairing == 'with_O1' :
    Y_mask = np.zeros(shape=(nb_channels, nb_channels))
    Y_mask[0, :] = np.ones(nb_channels)
else :
    print("pairing not recognized")
    exit

pairs_indices = np.nonzero(Y_mask)
nb_pairs = len(Y_mask[pairs_indices])

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events_reader_output = events_reader.output()


window_length = .5 # seconds
window_size = int(window_length * sampling_rate)
nb_windows = int(min_nb_values / window_size)

## Creates the histograms to fill with data :
##  - as many histograms as pairs of channels x 2 (for the two conditions)
##  - with as many bins as 100 (100 bins between all the possible phase lags (-pi -> pi)
bins = np.histogram([], bins=100, range=[-np.pi, np.pi], weights=[])[1]
bars_diff_yo = []
bars_diff_yf = []
for p_i in range(nb_pairs) :
    bars_diff_yo.append(np.histogram([], bins=100, range=[-np.pi, np.pi], weights=[])[0])
    bars_diff_yf.append(np.histogram([], bins=100, range=[-np.pi, np.pi], weights=[])[0])

for f in tqdm(files) :
    print(str(f))
    reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
    reader_edf_file.setFilePath(str(f))
    reader_edf_file.run()
    signal        = reader_edf_file.output()[:32, :min_nb_values] # 33 is GSR
    sampling_rate = reader_edf_file.samplingRate()
    channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
    nb_values = signal.shape[1]
    duration = nb_values / sampling_rate

    nb_channels = len(channels)

    ## Select only certain electrodes, and filter the signal
    indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]

    if reference_electrode in channels :
        reference = signal[[np.where(channels == c)[0][0] for c in [reference_electrode]]]
        signal = signal - reference

    signal = signal[indices_selection]
    channels = channels[indices_selection]
    nb_channels = len(channels)

    ## Computes the wavelet coeficients with the wavelet trasnform first
    wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
    wavelet_fourier.setFrequencies(frequencies)
    wavelet_fourier.setSignal(signal)
    wavelet_fourier.setWidth(wavelet_width)
    wavelet_fourier.setSamplingRate(sampling_rate)
    wavelet_fourier.run()
    wt = np.copy(wavelet_fourier.output())

    ## Exttract the amplitude and the phase
    amplitude = np.abs(wt)
    phase = np.angle(wt)

    ## Then analyze window by window (it's not possible to allocate enough memory to compute all the diff)
    count_yo = 0
    count_yf = 0
    for w_i in range(nb_windows) :
        w_start = int(w_i * window_size)
        w_end = int((w_i + 1) * window_size)
        w_phase = phase[:, :,  w_start : w_end]
        w_amplitude = amplitude[:, :, w_start : w_end]
        w_phase_diff = np.empty(shape=(nb_pairs, wt.shape[1], window_size))
        w_amplitude_prod = np.empty(shape=(nb_pairs, wt.shape[1], window_size))
        for p_i in range(nb_pairs) :
            w_phase_diff[p_i] = w_phase[pairs_indices[1][p_i]] - w_phase[pairs_indices[0][p_i]]
            w_amplitude_prod[p_i] = w_amplitude[pairs_indices[1][p_i]] * w_amplitude[pairs_indices[0][p_i]]
        ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
        w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
        w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)
        ## Check if the center of the window is in yo or yf
        if yoyf((w_end + w_start) / 2 / sampling_rate) == "alpha" :
            for p_i in range(nb_pairs) :
                if weighting == 'amp_prod_weighting' :
                    bars_diff_yf[p_i] += np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi], weights=w_amplitude_prod[p_i])[0]
                elif weighting == 'no_weighting' :
                    bars_diff_yf[p_i] += np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi])[0]
                else :
                    print("weighting not recognized")
            count_yf += w_phase_diff.size
        else :
            for p_i in range(nb_pairs) :
                if weighting == 'amp_prod_weighting' :
                    bars_diff_yo[p_i] += np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi], weights=w_amplitude_prod[p_i])[0]
                elif weighting == 'no_weighting' :
                    bars_diff_yo[p_i] += np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi])[0]
                else :
                    print("weighting not recognized")
            count_yo += w_phase_diff.size
        # if weighting == 'amp_prod_weighting' :
        #     for p_i in range(nb_pairs) :
        #         # Since the phase differences in a window are not normaly distributed, we take the most representative value as the bin value of the highest bar
        #         bars, bins = np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi], weights=w_amplitude_prod[p_i])
        #         diff[p_i][w_i] = bins[np.argmax(bars)]
        # elif weighting == 'no_weighting' :
        #     for p_i in range(nb_pairs) :
        #         # Most simple approximation
        #         diff[p_i][w_i] = np.mean(w_phase_diff[p_i], axis=(0, 1))
        # else :
        #     print("weighting not recognized")
        #     exit

width = 0.7 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
np.random.seed(0)
plt.clf()
fig, ax = plt.subplots(1, 1)
for p_i in range(nb_pairs) :
    color = np.random.rand(3,)
    ax.plot(center, bars_diff_yo[p_i] / count_yo, label="Eyes Open " + channels[pairs_indices[1][p_i]] + "-" + channels[pairs_indices[0][p_i]], color=color)
    ax.plot(center, bars_diff_yf[p_i] / count_yf, label="Eyes Closed " + channels[pairs_indices[1][p_i]] + "-" + channels[pairs_indices[0][p_i]], color=color, marker="+")
ax.xaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
ax.xaxis.set_minor_locator(plt.MultipleLocator(np.pi / 12))
ax.xaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))
ax.axvline(x=0.)
ax.legend()


name = "histo_delay_simple_" + weighting + "_pairing_" + pairing + "_window_" + str(window_length) + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width) + "_paradigm_" + paradigm + "_subject_" + subject_id
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((20.5, 20.5), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
