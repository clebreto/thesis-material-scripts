import ztrcore

import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba
import matplotlib

import scipy as sc
import scipy.ndimage.filters as filters
import scipy.signal
from scipy.spatial import distance_matrix

import copy

from tqdm import tqdm

ztrcore.initialize()

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/programming/come/zither/resources/events/neurofeedback_synchrony.evt")
events_reader.run()
events= events_reader.output()

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")

reader_edf_file.setFilePath("/user/clebreto/home/Data/Neurofeedback/2022_02_04/2022_02_04_15_26_45_0001_neurofeedback_synchrony_marker.edf")

reader_edf_file.run()
marker        = reader_edf_file.output()
marker_sampling_rate = reader_edf_file.samplingRate()
marker_channels      = np.array(reader_edf_file.channels())
marker_nb_values = marker.shape[1]
marker_duration = marker_nb_values / marker_sampling_rate

print("marker duration", marker_duration, "marker sampling rate", marker_sampling_rate)

reader_edf_file.setFilePath("/user/clebreto/home/Data/Neurofeedback/2022_02_04/2022_02_04_15_26_42_0001_neurofeedback_synchrony.edf")

reader_edf_file.run()
eeg        = reader_edf_file.output()
eeg_sampling_rate = reader_edf_file.samplingRate()
eeg_channels      = np.array(reader_edf_file.channels())
eeg_nb_values = eeg.shape[1]
eeg_duration = eeg_nb_values / eeg_sampling_rate

print("eeg duration", eeg_duration, "eeg sampling rate", eeg_sampling_rate)
highpass = ztrcore.ztrProcessFilterHighPassOffline()
highpass.setSignal(eeg);
highpass.setFrequency(2.);
highpass.setSamplingRate(eeg_sampling_rate);
highpass.setOrder(4);
highpass.run();
highpass_output = np.copy(highpass.output());

lowpass = ztrcore.ztrProcessFilterLowPassOffline()
lowpass.setSignal(highpass_output);
lowpass.setFrequency(40.);
lowpass.setSamplingRate(eeg_sampling_rate);
lowpass.setOrder(4);
lowpass.run();
lowpass_output = np.copy(lowpass.output());

plt.clf()
fg, ax = plt.subplots(2, 1)
marker = np.concatenate((np.zeros(shape=(int(2. * marker_sampling_rate))), marker[0]))
marker_time = np.linspace(0, eeg_duration, marker.shape[0])
ax[0].plot(marker_time, marker, color='black')
ax[0].set_ylim(0, 1)
ax[0].margins(x=0)
eeg_time = np.linspace(0, eeg_duration, eeg.shape[1])
ax[1].plot(eeg_time, lowpass_output[0], color='black')
ax[1].set_ylim(-20, 20)
ax[1].margins(x=0)

handles = []

color="red"
event="upregulation"
skip = False
for e_i in range(len(events)) :
    if events[e_i].uid in event :
        if skip :
            skip = False
            continue
        handles.append(ptc.Patch(edgecolor=color, facecolor=(0,0,0,0.0), fill=True, label=events[e_i].uid))
        ax[0].add_patch(ptc.Rectangle( ((events[e_i].timestamp / 1000. - marker_time[0]), 0), (events[e_i + 1].timestamp - events[e_i].timestamp) / 1000., marker.shape[0], edgecolor = color, facecolor = (0,0,0,0.0), fill=True))
        skip = True
skip = False
ax[0].legend(handles=handles)

color="yellow"
event="passive_fixation"
skip = False
for e_i in range(len(events)) :
    if events[e_i].uid in event :
        if skip :
            skip = False
            continue
        handles.append(ptc.Patch(edgecolor=color, facecolor=(0,0,0,0.0), fill=True, label=events[e_i].uid))
        ax[0].add_patch(ptc.Rectangle( ((events[e_i].timestamp / 1000. - marker_time[0]), 0), (events[e_i + 1].timestamp - events[e_i].timestamp) / 1000., marker.shape[0], edgecolor = color, facecolor = (0,0,0,0.0), fill=True))
        skip = True
skip = False
ax[0].legend(handles=handles)

color="blue"
event="relaxation"
skip = False
for e_i in range(len(events)) :
    if events[e_i].uid in event :
        if skip :
            skip = False
            continue
        handles.append(ptc.Patch(edgecolor=color, facecolor=(0,0,0,0.0), fill=True, label=events[e_i].uid))
        ax[0].add_patch(ptc.Rectangle( ((events[e_i].timestamp / 1000. - marker_time[0]), 0), (events[e_i + 1].timestamp - events[e_i].timestamp) / 1000., marker.shape[0], edgecolor = color, facecolor = (0,0,0,0.0), fill=True))
        skip = True
skip = False
ax[0].legend(handles=handles)

name = "neurofeedback_synchrony"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((300, 10), forward=False)
fg.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
