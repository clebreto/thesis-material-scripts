import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import matplotlib.patches as ptc
import mne

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

channels_selection = ['P3', 'F7', 'F5', 'F3', 'F1', 'F2', 'F4', 'F6', 'AF3', 'AFz', 'AF4', 'Fp1', 'Fp2']

channel = 'F7'
channel_index = channels_selection.index(channel)

subject = "04"
session = "1"
file_path_root = "/user/clebreto/home/Data/Hackathlon/P" + str(subject) + "/S1/eeg/alldata_sbj" + str(subject) + "_sess" + session + "_"
conditions = ['RS', 'MATBeasy', 'MATBmed', 'MATBdiff']
conditions_colors = ['blue', 'blue', 'pink', 'pink', 'orange', 'orange', 'red', 'red']

durations = []
signals = []
for c in conditions :
    file_path = file_path_root + c + ".set"
    epochs = mne.io.read_epochs_eeglab(file_path, verbose=False)  # FIR 1-40 Hz
    epochs = epochs.pick_channels(channels_selection)
    data = epochs.get_data()
    signal = np.zeros(shape=(data.shape[1], data.shape[2]*data.shape[0]))
    sampling_rate = 250.
    durations.append(signal.shape[1] / sampling_rate)

    for e in range(data.shape[0]) :
        signal[:, e * data.shape[2] : (e +1) * data.shape[2]] = data[e]

    signals.append(signal)

signal = np.hstack(signals)

sampling_rate = 250.

nb_values = signal.shape[1]
duration = nb_values / sampling_rate

measure_name = "Plv"
wavelet_width = 14.
window_length = 5.
window_overlapping = 3.5

frequencies = np.arange(7, 30, 1.)

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signal)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wavelet_output = wavelet_fourier.output()

tfwt = ztrcore.ztrProcessTFWindowT(measure_name, wavelet_output, sampling_rate, window_length, window_overlapping)
tfwt.run()

tfwt_output = tfwt.output() # C x C x F x T (real)
tfwt_output = tfwt_output[np.triu_indices(tfwt_output.shape[0], 1)] # [(C x C - C) / 2] x F x T

plt.clf()
fig, ax = plt.subplots(1, 1)
ax.plot(np.mean(tfwt_output, axis=(0, 1)))

name = measure_name + "_mean_" + file_path_root.split("/")[-1]
shutil.copy("impact.py", name + ".py")
fig.set_size_inches((90., 20.), forward=False)
fig.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
