import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2020_11_24/2020_11_24_16_27_27_0001_alpha.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()
sampling_rate = reader_edf_file.samplingRate()
channels      = reader_edf_file.channels()
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

## Computes the wavelet coeficients with the wavelet trasnform
fmin = 7.
fmax = 17.
fstep = .1
width = 7.
wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(np.arange(fmin, fmax, fstep))
wavelet_fourier.setSignal(signal)
wavelet_fourier.setWidth(width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wavelet_fourier_output = np.copy(wavelet_fourier.output())

plt.clf()
freq_mean = np.mean(np.abs(wavelet_fourier_output), axis=(0, 2))
freq_std = np.std(np.abs(wavelet_fourier_output), axis=(0, 2))
x = np.linspace(fmin, fmax, int((fmax-fmin) / fstep))
plt.plot(x, freq_mean, label="alpha and baseline")
plt.fill_between(x, freq_mean - freq_std, freq_mean + freq_std, alpha=0.1)
plt.legend()

name = "freq_average" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(width)
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
