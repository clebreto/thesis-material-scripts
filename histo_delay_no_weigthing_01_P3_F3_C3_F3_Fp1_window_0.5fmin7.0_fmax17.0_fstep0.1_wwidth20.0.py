import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2020_11_24/2020_11_24_16_27_27_0001_alpha.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()[:32] # 33 is GSR
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events_reader_output = events_reader.output()

## Select only certain electrodes, and filter the signal
channels_selection = np.array(['O1', 'P3', 'C3', 'F3', 'Fp1'])
indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
signal = signal[indices_selection]
channels = channels[indices_selection]
nb_channels = len(channels)

## To compute only pairs with O1
Y_mask = np.zeros(shape=(nb_channels, nb_channels))
Y_mask[0, 1:] = 1
pairs_indices = np.nonzero(Y_mask)
nb_pairs = len(Y_mask[pairs_indices])

## Computes the wavelet coeficients with the wavelet trasnform first
fmin = 7.
fmax = 17.
fstep = .1
frequencies = np.arange(fmin, fmax, fstep)
wavelet_width = 20.

wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(frequencies)
wavelet_fourier.setSignal(signal)
wavelet_fourier.setWidth(wavelet_width)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wt = np.copy(wavelet_fourier.output())

## Exttract the phase
phase = np.angle(wt)

window_length = .5 # seconds
window_size = int(window_length * sampling_rate)
nb_windows = int(nb_values / window_size)

def yoyf(t) :
    skip = False
    for e_i in range(len(events_reader_output)) :
        if skip :
            skip = False
            continue
        if t > events_reader_output[e_i].timestamp / 1000. and t < events_reader_output[e_i + 1].timestamp / 1000. :
            return events_reader_output[e_i].uid
        skip = True

## Then analyze window by window (it's not possible to allocate enough memory to compute all the diff)
bins = np.histogram([], bins=100, range=[-np.pi, np.pi], weights=[])[1]
bars_diff_yo = []
bars_diff_yf = []
count_yo = []
count_yf = []
for p_i in range(nb_pairs) :
    bars_diff_yo.append(np.histogram([], bins=100, range=[-np.pi, np.pi])[0])
    bars_diff_yf.append(np.histogram([], bins=100, range=[-np.pi, np.pi])[0])
    count_yo.append(0)
    count_yf.append(0)

for w_i in range(nb_windows) :
    w_start = int(w_i * window_size)
    w_end = int((w_i + 1) * window_size)
    w_phase = phase[:, :,  w_start : w_end]
    w_phase_diff = np.empty(shape=(nb_pairs, wt.shape[1], window_size))
    for p_i in range(nb_pairs) :
        w_phase_diff[p_i] = w_phase[pairs_indices[1][p_i]] - w_phase[pairs_indices[0][p_i]]
    ## Correction supposing all delays are shorter than half a period (hence between -pi and +pi)
    w_phase_diff = np.where(w_phase_diff > np.pi, w_phase_diff - 2 * np.pi, w_phase_diff)
    w_phase_diff = np.where(w_phase_diff < -np.pi, w_phase_diff + 2 * np.pi, w_phase_diff)
    ## Check if the center of the window is in yo or yf
    if yoyf((w_end + w_start) / 2 / sampling_rate) == "alpha" :
        for p_i in range(nb_pairs) :
            bars_diff_yf[p_i] += np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi])[0]
            count_yf[p_i] += w_phase_diff[p_i].size
    else :
        for p_i in range(nb_pairs) :
            bars_diff_yo[p_i] += np.histogram(w_phase_diff[p_i], bins=100, range=[-np.pi, np.pi])[0]
            count_yo[p_i] += w_phase_diff[p_i].size

center = (bins[:-1] + bins[1:]) / 2
plt.clf()
for p_i in range(nb_pairs) :
    color = np.random.rand(3,)
    plt.plot(center, bars_diff_yo[p_i] / count_yo[p_i], label="Eyes Open " + channels[pairs_indices[1][p_i]] + "-" + channels[pairs_indices[0][p_i]], color=color)
    plt.plot(center, bars_diff_yf[p_i] / count_yf[p_i], label="Eyes Closed " + channels[pairs_indices[1][p_i]] + "-" + channels[pairs_indices[0][p_i]], color=color, marker="+")
    plt.axvline(x=0.)
plt.legend()

name = "histo_delay_no_weigthing_01_P3_F3_C3_F3_Fp1" + "_window_" + str(window_length) + "fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(wavelet_width)
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
