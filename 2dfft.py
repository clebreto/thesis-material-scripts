import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt
import scipy as sc

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2020_11_24/2020_11_24_16_27_27_0001_alpha.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()[:32] # 33 is GSR
sampling_rate = reader_edf_file.samplingRate()
channels      = np.array(reader_edf_file.channels()[:32]) # 33 is GSR
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events_reader_output = events_reader.output()

## Select only certain electrodes, and filter the signal
channels_selection = np.array(['Oz', 'POz', 'Pz', 'Cz', 'Fz', 'Fpz'])
indices_selection = [np.where(channels == c)[0][0] for c in channels_selection]
signal = signal[indices_selection]
channels = channels[indices_selection]
nb_channels = len(channels)

# signal = np.ndarray(shape=(6, nb_values))
# print(nb_values / duration)
# t = np.linspace(0, duration, nb_values)
# f0 = 10
# f1 = 20
# signal[0] = np.sin(f0 * 2. * np.pi * t + 0. * np.pi / 5.) + 0.5 * np.sin(f1 * 2. * np.pi * t + 0. * np.pi / 5.)
# signal[1] = np.sin(f0 * 2. * np.pi * t + 1. * np.pi / 5.) + 0.5 * np.sin(f1 * 2. * np.pi * t + 1. * np.pi / 5.)
# signal[2] = np.sin(f0 * 2. * np.pi * t + 2. * np.pi / 5.) + 0.5 * np.sin(f1 * 2. * np.pi * t + 2. * np.pi / 5.)
# signal[3] = np.sin(f0 * 2. * np.pi * t + 3. * np.pi / 5.) + 0.5 * np.sin(f1 * 2. * np.pi * t + 3. * np.pi / 5.)
# signal[4] = np.sin(f0 * 2. * np.pi * t + 4. * np.pi / 5.) + 0.5 * np.sin(f1 * 2. * np.pi * t + 4. * np.pi / 5.)
# signal[5] = np.sin(f0 * 2. * np.pi * t + 5. * np.pi / 5.) + 0.5 * np.sin(f1 * 2. * np.pi * t + 5. * np.pi / 5.)

start = 25.
window_duration = 2.
decimation = 1
window_signal = signal[:, int(start * sampling_rate) : int((start + window_duration) * sampling_rate) : decimation]
fft2 = np.fft.fft2(window_signal)
window_sampling_rate = sampling_rate / decimation
space_freqs = np.fft.fftfreq(fft2.shape[0], 1 / 3.) #30mms between each pair of electrodes
time_freqs = np.fft.fftfreq(fft2.shape[1],  1. / window_sampling_rate)

plt.clf()
plt.imshow(np.abs(fft2[: int(space_freqs.size / 2.), 20 : int(time_freqs.size / 2.)]), aspect="auto", origin="lower")
x_positions = np.arange(20, int(fft2.shape[1] / 2.), 10)
y_positions = np.arange(0, int(fft2.shape[0] / 2.))
x_labels = time_freqs[20 : int(time_freqs.size / 2.)][::10]
y_labels = space_freqs[: int(space_freqs.size / 2.)]
plt.yticks(y_positions, np.around(y_labels, decimals=1))
plt.xticks(x_positions, np.around(x_labels, decimals=1))

name = "2dfft"
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
