import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil

import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import LineCollection
from matplotlib.colors import to_rgba
import matplotlib

import scipy as sc
from scipy import ndimage
from scipy import signal
from tqdm import tqdm

matplotlib.rcParams.update({
    'text.usetex': True,
    'ps.usedistiller': 'xpdf',
    'legend.fontsize': 'x-large',
    'figure.figsize': (15, 5),
    'figure.titlesize': 40,
    'axes.labelsize': 40,
    'axes.titlesize': 40,
    'xtick.labelsize': 40,
    'ytick.labelsize': 40
})

f = np.zeros(shape=(1000))

gaussian_a = sc.signal.gaussian(1000, 10)
gaussian_a /= np.sum(gaussian_a)

gaussian_b = sc.signal.gaussian(1000, 5)
gaussian_b /= np.sum(gaussian_b)

ricker_a = sc.signal.ricker(1000, 10)
ricker_b = sc.signal.ricker(1000, 5)

f[:500] = gaussian[250:750]
f[500:] = gaussian_b[250:750]

plt.clf()
fg, ax = plt.subplots(3, 1)
ax[0].plot(f, color='black')
ax[0].set_xlabel("$Time (s.)$")
ax[0].set_ylabel(r'$Voltage (\mu V)$')

ax[1].plot(sc.signal.convolve(f, gaussian_a, mode='same'), color='red')
ax[1].plot(sc.signal.convolve(f, gaussian_b, mode='same'), color='blue')
ax[1].set_xlabel("$Time (s.)$")
ax[1].set_ylabel(r'$Voltage (\mu V)$')

ax[2].plot(sc.signal.convolve(f, ricker_a, mode='same'), color='red')
ax[2].plot(sc.signal.convolve(f, ricker_b, mode='same'), color='blue')
ax[2].set_xlabel("$Time (s.)$")
ax[2].set_ylabel(r'$Voltage (\mu V)$')

name = "gaussians"
shutil.copy("impact.py", name + ".py")
fg.set_size_inches((30, 25), forward=False)
fg.savefig(name + ".png", dpi=100, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
