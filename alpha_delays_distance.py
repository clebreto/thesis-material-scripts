import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl

import matplotlib.patches as ptc
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events_reader_output = events_reader.output()

file_path = pl.Path("/user/clebreto/home/Data/Neurofeedback/2020_11_24/2020_11_24_16_27_27_0001_alpha.edf")

reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
reader_edf_file_output        = np.copy(reader_edf_file.output())
reader_edf_file_sampling_rate = reader_edf_file.samplingRate()
reader_edf_file_channels      = reader_edf_file.channels()

sampling_rate = reader_edf_file_sampling_rate
nb_values = reader_edf_file_output.shape[1]
duration = nb_values / sampling_rate

selection = ["P3", "C3", "F3", "O1", "P7", "F7", "Fp1", "Fpz"]
channels = [x for x in selection if x in reader_edf_file_channels]
indices = np.array([reader_edf_file_channels.index(x) for x in selection if x in reader_edf_file_channels])
data = reader_edf_file_output[indices, :]

fmin = 9.
fmax = 11.
fstep = 0.1
wavelet_fourier = ztrcore.ztrProcessWaveletFourier()
wavelet_fourier.setFrequencies(np.arange(fmin, fmax, fstep))
wavelet_fourier.setSignal(data)
wavelet_fourier.setWidth(14.)
wavelet_fourier.setSamplingRate(sampling_rate)
wavelet_fourier.run()
wavelet_fourier_output = np.copy(wavelet_fourier.output())
complex_coefficients = wavelet_fourier_output

nb_channels = len(channels)
delays = np.zeros(shape=(nb_channels, nb_channels, complex_coefficients.shape[1], complex_coefficients.shape[2]))

for c_a in range(nb_channels) :
    for c_b in range(c_a + 1, nb_channels) :
        delays[c_a, c_b] = np.angle(complex_coefficients[c_a] * np.conj(complex_coefficients[c_b]))

delays_time = np.linspace(0, complex_coefficients.shape[2] / sampling_rate, complex_coefficients.shape[2])
delays_event = np.ndarray(shape=(nb_channels, nb_channels, complex_coefficients.shape[1], 0))
skip = False
for e_i in range(len(events_reader_output)) :
    if events_reader_output[e_i].uid == 'baseline' :
        if skip :
            skip = False
            continue
        delays_indices = (delays_time > events_reader_output[e_i].timestamp / 1000.) & (delays_time < events_reader_output[e_i + 1].timestamp / 1000.)
        temp_delays = delays[:, :, :, delays_indices]
        delays_event = np.concatenate((delays_event, temp_delays), axis=(3))
        skip = True

headset = ztrcore.readHeadset("/home/clebreto/programming/come/zither/src/ztrCore/resources/32_eego.json")
positions = headset.positions()[indices]
labels    = np.array(headset.labels())[indices]
electrodes_positions = dict(zip(labels, positions))

distance = []
mean     = []
std      = []
label    = []
values   = []
print(channels)
for c_i  in range(nb_channels) :
    for c_j in range(c_i + 1, nb_channels) :
        distance.append(np.linalg.norm(electrodes_positions[channels[c_i]] - electrodes_positions[channels[c_j]]))
        values.append(delays_event[c_i, c_j].flatten())
        mean.append(np.mean(delays_event[c_i, c_j]))
        std.append(np.std(delays_event[c_i, c_j]))
        label.append(channels[c_i] + "-" + channels[c_j])

dv = list(zip(distance, values))
dv.sort(key=lambda x: x[0])
values = [x for _,x in dv]
dv = list(zip(distance, mean))
dv.sort(key=lambda x: x[0])
mean = [x for _,x in dv]
plt.clf()
plt.boxplot(values, positions = distance, widths=0.1, showfliers=False)
plt.scatter(distance, mean, c=std, cmap='winter_r')
plt.ylim(-np.pi, np.pi)
for i, txt in enumerate(label):
    plt.annotate(txt, (distance[i], mean[i]))
plt.xticks([])
plt.axhline()
plt.show()

name = "alpha_delays_distance"
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
