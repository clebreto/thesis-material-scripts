import ztrcore
import ztrplotting
import numpy as np
import pathlib  as pl
import sys, os, shutil
import matplotlib.pyplot as plt

ztrcore.setVerboseLoading(True)
ztrcore.setAutoLoading(True)
ztrcore.initialize()

file_path = pl.Path("/user/clebreto/home/Data/P300/2021_09_25/2021_09_25_23_24_20__0.edf")
reader_edf_file = ztrcore.processReader_pluginFactory().create("ztrProcessReaderEdfFile")
reader_edf_file.setFilePath(str(file_path))
reader_edf_file.run()
signal        = reader_edf_file.output()
sampling_rate = reader_edf_file.samplingRate()
channels      = reader_edf_file.channels()
nb_values = signal.shape[1]
duration = nb_values / sampling_rate

nb_channels = len(channels)

## Reads the event as they were presented during the recording
events_reader = ztrcore.ztrProcessEventsReader()
events_reader.setFilePath("/user/clebreto/home/Data/Neurofeedback/2020_11_24/alpha.evt")
events_reader.run()
events = events_reader.output()

fmin = 4.
fmax = 17.
fstep = .1
frequency = np.arange(fmin, fmax, fstep)

w_width = 30.

time = np.linspace(0, duration, nb_values)
frequencies = np.arange(2., 40, 0.1)

fig, ax = plt.subplots(1, 1)
ztrplotting.plotTFAmplitudes(ax, time, frequencies, signal[6], w_width=10, events=events, event=['alpha'], color='red', channel_name="FCz")

name = "TF_8_dry_alpha_baseline_nowindow_fmin" + str(fmin) + "_fmax" + str(fmax) + "_fstep" + str(fstep) + "_wwidth" + str(width)
shutil.copy("impact.py", name + ".py")
plt.savefig(name + ".png", dpi=None, orientation='landscape', format="png", metadata={"code":open(os.path.abspath(name + ".py"), "r").read()})
