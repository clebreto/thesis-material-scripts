import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import itertools as itl

def genRanks(na, nb) :
    ranks_A_B = np.arange(1, na+nb+1)
    ranks_A = np.random.choice(ranks_A_B, na, replace=False)
    ranks_B = np.delete(ranks_A_B, ranks_A - 1)
    return (ranks_A, ranks_B, ranks_A_B)

na = 100
nb = 100
f = 3
sum_ranks_As = []
sum_ranks_Bs = []
min_sum_ranks = []
for i in range(100000) :
    (ranks_A, ranks_B, ranks_A_B) = genRanks(na, nb)
    ranks_A = f * (np.repeat(ranks_A, f) - 1) + 1 + np.tile(np.arange(0, f), na)
    ranks_B = f * (np.repeat(ranks_B, f) - 1) + 1 + np.tile(np.arange(0, f), nb)
    sum_ranks_A = np.sum(ranks_A)
    sum_ranks_B = np.sum(ranks_B)
    sum_ranks_As.append(sum_ranks_A)
    sum_ranks_Bs.append(sum_ranks_B)
    min_sum_ranks.append(min(sum_ranks_A, sum_ranks_B))

sum_ranks_As = np.array(sum_ranks_As)
sum_ranks_Bs = np.array(sum_ranks_Bs)
min_sum_ranks = np.array(min_sum_ranks)

# ranks = itl.permutations(np.arange(1, na+nb+1))
# sum_ranks_As = []
# sum_ranks_Bs = []
# min_sum_ranks = []
# for r in ranks :
#     sum_ranks_A = np.sum(np.array(r)[:na])
#     sum_ranks_B = np.sum(np.array(r)[na:])
#     sum_ranks_As.append(sum_ranks_A)
#     sum_ranks_Bs.append(sum_ranks_B)
#     min_sum_ranks.append(min(sum_ranks_A, sum_ranks_B))
# sum_ranks_As = np.array(sum_ranks_As)
# sum_ranks_Bs = np.array(sum_ranks_Bs)
# min_sum_ranks = np.array(min_sum_ranks)

print(stats.norm.fit(sum_ranks_As))
print("mu: " + str(na * (na + nb + 1) / 2), "std: " + str(np.sqrt(na * nb * (na + nb + 1) / 12)))
print("mu: " + str(f * na * (f *na + f * nb + 1) / 2), "std: " + str(np.sqrt(f * na * f * nb * (f * na + f * nb + 1) / 12)))
plt.hist(sum_ranks_As, bins=100)
plt.show()

print("Over")
